import ast
import functools
import math
import os
from collections import OrderedDict
from typing import Tuple, List, Union, Dict, Set, Callable

import PIL.Image
import numpy as np
import pandas as pd
import tensorflow as tf
from PIL.Image import Image
from imgaug import augmenters as iaa, SegmentationMapsOnImage
from sklearn.preprocessing import MultiLabelBinarizer
from tensorflow.keras.preprocessing.image import Iterator, load_img
from torch.utils.data.dataset import Dataset

from datalife.mask_utils import rle_to_mask
from datalife.preprocessing import IMAGE_PREPROCESSING_FUNCTIONS
from datalife.task.data_preparation.base import PIL_INTERPOLATION_METHODS
from datalife.task.meta_config import ProjectConfig, InputType, TaskType, Task
from datalife.utils import img_to_array, SoftmaxLabelBinarizer


class ImageDataset(Dataset):
    def __init__(self, project_config: ProjectConfig, tasks: List[Task], data_frame: pd.DataFrame,
                 images_path: str, image_size: Tuple[int, int], interpolation: str, image_preprocessing: str,
                 image_preprocessing_extra_params: dict, image_color_mode: str, image_data_format: str,
                 number_of_slices: int = None,
                 augmenter: Union[iaa.Augmenter, Callable[[tf.Tensor, tf.Tensor], tf.Tensor]] = None,
                 cache_images: bool = False, dtype: str = "float32", workers: int = None):
        self._project_config = project_config
        self._tasks = tasks
        self._input_type = project_config.input_type
        self._number_of_slices = number_of_slices
        self._image_size = image_size
        self._image_preprocessing_function = IMAGE_PREPROCESSING_FUNCTIONS[image_preprocessing]
        self._image_color_mode = image_color_mode
        self._image_data_format = image_data_format
        self._image_preprocessing_params = {"image_data_format": image_data_format, **image_preprocessing_extra_params}
        self._interpolation = interpolation
        self._augmenter = augmenter
        self._cache_images = cache_images
        self._dtype = dtype
        self._workers = workers

        if self._input_type == InputType.IMAGE_2D:
            self.im_list = np.array([os.path.join(images_path, image) for image in data_frame["fileName"]])
        elif self._input_type == InputType.IMAGE_3D or self._input_type == InputType.MULTI_VIEW_IMAGE_2D:
            assert "fileNames" in data_frame
            if type(data_frame.iloc[0].fileNames) is str:
                data_frame["fileNames"] = data_frame["fileNames"].apply(lambda file_names: ast.literal_eval(file_names))
            self.im_list = np.array(
                [[os.path.join(images_path, image) for image in images] for images in data_frame["fileNames"]])
        else:
            raise ValueError("input_type must be either IMAGE_2D or IMAGE_3D")

        self.targets: Dict[str, np.ndarray] = OrderedDict()
        for task in tasks:
            if task.type == TaskType.SOFTMAX_CLASSIFICATION:
                transformer = SoftmaxLabelBinarizer(classes=[class_ for class_ in task.mapping.keys()])
                task_targets = transformer.transform(data_frame[task.target_column].values)
            elif task.type == TaskType.MULTILABEL_CLASSIFICATION:
                transformer = MultiLabelBinarizer(classes=np.array([class_ for class_ in task.mapping.keys()]))
                if type(data_frame.iloc[0][task.target_column]) is str:
                    data_frame[task.target_column] = data_frame[task.target_column].apply(ast.literal_eval)
                task_targets = transformer.fit_transform(data_frame[task.target_column].values)
            else:
                task_targets = data_frame[task.target_column].values
            self.targets[task.output_layer_name] = task_targets.astype(np.float32)

        sample_image = np.array(PIL.Image.open(self.im_list[0]
                                               if self._input_type == InputType.IMAGE_2D else self.im_list[0][0]))
        self._image_dtype = str(sample_image.dtype) if sample_image.dtype == np.uint8 else "uint16"

        # if cache_images:
        #     self._cache: Dict[str, Image] = {}
        #     print("Loading images in cache...")
        #     im_list: List[str] = self.im_list \
        #         if self._input_type == InputType.IMAGE_2D \
        #         else [image for images in self.im_list for image in images]
        #     for im in tqdm(im_list, total=len(im_list)):
        #         self._cache[im] = self._load_raw_image(im)

    def set_augmenter(self, augmenter: iaa.Augmenter) -> None:
        self._augmenter = augmenter

    def _load_raw_image(self, im: str) -> Image:
        if hasattr(self, "_cache") and im in self._cache:
            return self._cache[im]
        else:
            return load_img(im, color_mode=self._image_color_mode, target_size=self._image_size,
                            interpolation=self._interpolation)

    def preprocess_image(self, image: np.ndarray) -> np.ndarray:
        return self._image_preprocessing_function(image, self._image_preprocessing_params)

    def _join_slices(self, images: List[np.ndarray]) -> np.ndarray:
        if self._number_of_slices > len(images):
            padding_images = [np.zeros(images[0].shape) for _ in range(self._number_of_slices - len(images))]
            left_padding, right_padding = np.array_split(padding_images, 2)
            images = left_padding.tolist() + images + right_padding.tolist()
        else:
            images = [np.mean(group, axis=0) for group in (np.array_split(images, self._number_of_slices))]
        return np.stack(images, -1 if self._image_data_format == "channels_first" else 2)

    def _load_image(self, im: Union[str, List[str]]) -> np.ndarray:
        if self._input_type == InputType.IMAGE_2D:
            raw_image = self._load_raw_image(im)
            image = img_to_array(raw_image, self._image_data_format)

            if not hasattr(self, "_cache"):
                raw_image.close()
        else:
            raw_images = [self._load_raw_image(image) for image in im]
            image = self._join_slices([img_to_array(raw_image, self._image_data_format) for raw_image in raw_images])

            if not hasattr(self, "_cache"):
                for raw_image in raw_images:
                    raw_image.close()

        return image

    def _augment_images(self, images: np.ndarray,
                        targets: List[Dict[str, np.ndarray]]) -> Tuple[np.ndarray, List[Dict[str, np.ndarray]]]:
        if any(task.type == TaskType.SEGMENTATION for task in self._tasks):
            segmentation_maps = [SegmentationMapsOnImage(target[task.output_layer_name],
                                                         target[task.output_layer_name].shape)
                                 for target in targets
                                 for task in self._tasks if task.type == TaskType.SEGMENTATION]
            aug_images, aug_segmentation_maps = self._augmenter.augment(images=images,
                                                                        segmentation_maps=segmentation_maps)  # type: np.ndarray, List[SegmentationMapsOnImage]
            i = 0
            for target in targets:
                for task in self._tasks:
                    if task.type == TaskType.SEGMENTATION:
                        target[task.output_layer_name] = aug_segmentation_maps[i].arr
                        i += 1
        else:
            aug_images = self._augmenter.augment_images(images)
        return aug_images, targets

    def get_aspect_ratio(self, index: int) -> float:
        image = PIL.Image.open(self.im_list[index])
        return float(image.width) / float(image.height)

    def get_target(self, index: int) -> Dict[str, np.ndarray]:
        target = OrderedDict()
        for task, (k, t) in zip(self._tasks, self.targets.items()):
            if task.type == TaskType.SEGMENTATION:
                rle: str = t[index]
                if rle and isinstance(rle, str) and rle != "nan":
                    mask = rle_to_mask(rle, self._image_size[0], self._image_size[1])
                    mask = PIL.Image.fromarray(mask)
                    mask = mask.resize(self._image_size, resample=PIL_INTERPOLATION_METHODS[self._interpolation])
                    target[k] = np.expand_dims(np.asarray(mask), axis=2)
                else:
                    target[k] = np.zeros((self._image_size[0], self._image_size[1], 1))
            else:
                target[k] = t[index]
        return target

    def load_image(self, index: int) -> np.ndarray:
        return self._load_image(self.im_list[index])

    def load_raw_image(self, index: int) -> Image:
        assert self._input_type == InputType.IMAGE_2D
        return self._load_raw_image(self.im_list[index])

    def load_raw_images(self, index: int) -> List[Image]:
        assert self._input_type == InputType.IMAGE_3D
        return [self._load_raw_image(im) for im in self.im_list[index]]

    def __getitem__(self, index: int) -> Tuple[np.ndarray, Dict[str, np.ndarray]]:
        image = self.load_image(index)
        target = self.get_target(index)

        if self._augmenter:
            images, targets = self._augment_images(np.array([image]), [target])
            image = images[0]
            target = targets[0]

        image = self.preprocess_image(image.astype(self._dtype))

        return image, target

    def load_batch(self, index_array: List[int]) -> Tuple[np.ndarray, Dict[str, np.ndarray]]:
        batch: List[Tuple[np.ndarray, Dict[str, np.ndarray]]] = [(self.load_image(index), self.get_target(index))
                                                                 for index in index_array]

        images = np.array([item[0] for item in batch])
        targets = [item[1] for item in batch]

        return self._augment_and_preprocess(images, targets)

    def _augment_and_preprocess(self, images: np.ndarray, targets: List[Dict[str, np.ndarray]]):
        if self._augmenter:
            images, targets = self._augment_images(images, targets)
        images = images.astype(self._dtype)
        images = np.array([self.preprocess_image(image) for image in images])
        agg_targets = OrderedDict()
        for key in targets[0].keys():
            agg_targets[key] = np.array([target[key] for target in targets])
        return images, agg_targets

    def __len__(self) -> int:
        return len(self.im_list)

    def _tf_load_image(self, im_path: tf.Tensor,
                       target: Dict[str, tf.Tensor]) -> Tuple[tf.Tensor, Dict[str, tf.Tensor]]:
        img = tf.io.read_file(im_path)
        img = tf.image.decode_png(img, channels=3 if self._image_color_mode == "rgb" else 1,
                                  dtype=tf.as_dtype(self._image_dtype))

        return img, target

    def _tf_load_multi_view_images(self, im_path: List[tf.Tensor],
                                   target: Dict[str, tf.Tensor]) -> Tuple[Tuple[tf.Tensor, ...], Dict[str, tf.Tensor]]:
        imgs = tuple(self._tf_load_image(im_path[i], target)[0] for i in range(self._number_of_slices))

        return imgs, target

    def _tf_augment_multi_view_images(self, images: Tuple[tf.Tensor, ...],
                                      target: Dict[str, tf.Tensor]) -> Tuple[
        Tuple[tf.Tensor, ...], Dict[str, tf.Tensor]]:
        imgs = tuple(self._augmenter(images[i], target)[0] for i in range(self._number_of_slices))

        return imgs, target

    def _tf_augment_and_preprocess(self, images: tf.Tensor,
                                   targets: Dict[str, tf.Tensor]) -> Tuple[tf.Tensor, Dict[str, tf.Tensor]]:
        if self._augmenter and isinstance(self._augmenter, iaa.Augmenter):
            img_dtype = images.dtype
            img_shape = tf.shape(images)
            images = tf.numpy_function(self._augmenter.augment_images,
                                       [images],
                                       img_dtype)
            images = tf.reshape(images, shape=img_shape)

        images = tf.cast(images, tf.as_dtype(self._dtype))
        images = self.preprocess_image(images)

        return images, targets

    def _tf_augment_and_preprocess_multi_view_images(self, images: Tuple[tf.Tensor, ...],
                                                     targets: Dict[str, tf.Tensor]) -> Tuple[Tuple[tf.Tensor, ...],
                                                                                             Dict[str, tf.Tensor]]:
        imgs = tuple(self._tf_augment_and_preprocess(images[i], targets)[0] for i in range(self._number_of_slices))

        return imgs, targets

    def _cshift(self, values: tf.Tensor) -> tf.Tensor:  # Circular shift in batch dimension
        return tf.concat([values[-1:, ...], values[:-1, ...]], 0)

    def _mixup(self, images: tf.Tensor, targets: Dict[str, tf.Tensor], mixup: float,
               batch_size: int) -> Tuple[tf.Tensor, Dict[str, tf.Tensor]]:
        beta = tf.distributions.Beta(mixup, mixup)
        lam = beta.sample(batch_size)
        ll = tf.expand_dims(tf.expand_dims(tf.expand_dims(lam, -1), -1), -1)
        lt = tf.expand_dims(lam, -1)
        images = ll * images + (1 - ll) * self._cshift(images)
        targets = {key: (lt * tensor + (1 - lt) * self._cshift(tensor)) for key, tensor in targets.items()}

        return images, targets

    def to_tf_dataset(self, batch_size: int, shuffle: bool, mixup: float = 0.0, seed: int = 42) -> tf.data.Dataset:
        is_multi_view = self._input_type == InputType.MULTI_VIEW_IMAGE_2D

        dataset = tf.data.Dataset.from_tensor_slices((self.im_list, self.targets))
        if shuffle:
            dataset = dataset.shuffle(buffer_size=len(self), seed=seed)
        dataset = dataset.repeat()
        if is_multi_view:
            dataset = dataset.map(self._tf_load_multi_view_images,
                                  num_parallel_calls=self._workers or tf.data.experimental.AUTOTUNE)
        else:
            dataset = dataset.map(self._tf_load_image,
                                  num_parallel_calls=self._workers or tf.data.experimental.AUTOTUNE)
        if self._cache_images:
            dataset = dataset.cache()
        if self._augmenter and not isinstance(self._augmenter, iaa.Augmenter):
            if is_multi_view:
                dataset = dataset.map(self._tf_augment_multi_view_images,
                                      num_parallel_calls=self._workers or tf.data.experimental.AUTOTUNE)
            else:
                dataset = dataset.map(self._augmenter,
                                      num_parallel_calls=self._workers or tf.data.experimental.AUTOTUNE)
        dataset = dataset.batch(batch_size=batch_size)

        if is_multi_view:
            dataset = dataset.map(self._tf_augment_and_preprocess_multi_view_images,
                                  num_parallel_calls=self._workers or tf.data.experimental.AUTOTUNE)
        else:
            dataset = dataset.map(self._tf_augment_and_preprocess,
                                  num_parallel_calls=self._workers or tf.data.experimental.AUTOTUNE)
        if mixup:
            if is_multi_view:
                raise NotImplementedError("Mixup isn't supported for multi view")
            dataset = dataset.map(functools.partial(self._mixup, mixup=mixup, batch_size=batch_size),
                                  num_parallel_calls=self._workers or tf.data.experimental.AUTOTUNE)
        dataset = dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
        return dataset


class TfAugmenter(object):
    def __init__(self, tensors: Dict[str, tf.Tensor], labels: Set[str] = None):
        if labels is None:
            labels = {}
        self.tensors = tensors
        self.types = {k: "label" if k in labels else "image" for k, tensor in tensors.items()}

        self.operations: List[Tuple[Dict[str, Callable[[tf.Tensor], tf.Tensor], bool], float]] = []

        self.tensor_shape = tf.shape(list(self.tensors.values())[0])

    def transform_tensors(self, augmentations_done_range: Tuple[int, int]) -> Dict[str, tf.Tensor]:
        num_sometimes_augmentations = sum(1 for _, _, always in self.operations if not always)
        augmentations_done_range = (augmentations_done_range[0],
                                    min(augmentations_done_range[1], num_sometimes_augmentations))

        augmentations_done = tf.random.uniform([], augmentations_done_range[0], augmentations_done_range[1],
                                               dtype=tf.int32)

        sometimes_augmentations_to_run = tf.random.shuffle(tf.range(num_sometimes_augmentations))[:augmentations_done]

        out = self.tensors.copy()
        for i, (funcs, probability, always) in enumerate(self.operations):
            choice: tf.Tensor = tf.random.uniform([], 0, 1)
            for k, tensor in out.items():
                if k in funcs:
                    if always:
                        out[k] = self._cond_operation(tensor, funcs[k], choice, probability)
                    else:
                        should_run = tf.math.reduce_any(tf.scan(lambda init, el: el == i or init,
                                                                sometimes_augmentations_to_run, False))
                        out[k] = tf.cond(should_run,
                                         lambda: self._cond_operation(tensor, funcs[k], choice, probability),
                                         lambda: tensor)

        return out

    def _cond_operation(self, tensor: tf.Tensor, func: Callable[[tf.Tensor], tf.Tensor], choice: tf.Tensor,
                        probability: float = None) -> tf.Tensor:
        if probability:
            return tf.cond(choice < probability, lambda: func(tensor), lambda: tensor)
        else:
            return tensor

    def _add_operation(self, funcs: Dict[str, Callable[[tf.Tensor], tf.Tensor]],
                       probability: float, always: bool):
        self.operations.append((funcs, probability, always))

    def _get_interpolation(self, img_type: str):
        return "NEAREST" if img_type == "label" else "BILINEAR"

    def multiply(self, multiply_range: Tuple[float, float], probability: float = None, always: bool = True):
        factor = tf.random.uniform([], multiply_range[0], multiply_range[1])

        funcs = {}
        for k, img_type in self.types.items():
            if img_type == "image":
                funcs[k] = lambda input: tf.image.adjust_gamma(input, gain=factor)
        self._add_operation(funcs, probability, always)

    def add(self, add_range: Tuple[float, float], probability: float = None, always: bool = True):
        delta = tf.random.uniform([], add_range[0], add_range[1])

        funcs = {}
        for k, img_type in self.types.items():
            if img_type == "image":
                funcs[k] = lambda input: tf.image.adjust_brightness(input, delta)
        self._add_operation(funcs, probability, always)

    def contrast(self, contrast_range: Tuple[float, float], probability: float = None, always: bool = True):
        factor = tf.random.uniform([], contrast_range[0], contrast_range[1])

        funcs = {}
        for k, img_type in self.types.items():
            if img_type == "image":
                funcs[k] = lambda input: tf.image.adjust_contrast(input, factor)
        self._add_operation(funcs, probability, always)

    def flip_x(self, probability: float, always: bool = True):
        funcs = {}
        for k, _ in self.types.items():
            funcs[k] = tf.image.flip_left_right
        self._add_operation(funcs, probability, always)

    def flip_y(self, probability: float, always: bool = True):
        funcs = {}
        for k, _ in self.types.items():
            funcs[k] = tf.image.flip_up_down
        self._add_operation(funcs, probability, always)

    def _scale(self, input: tf.Tensor, scale: tf.Tensor, interpolation: str) -> tf.Tensor:
        x1 = y1 = 0.5 - (0.5 * 1 / scale)  # type: tf.Tensor
        x2 = y2 = 0.5 + (0.5 * 1 / scale)  # type: tf.Tensor

        boxes = tf.convert_to_tensor([[x1, y1, x2, y2]])

        return tf.cast(tf.image.crop_and_resize(tf.expand_dims(input, 0), boxes, tf.convert_to_tensor([0]),
                                                [self.tensor_shape[0], self.tensor_shape[1]],
                                                method=interpolation)[0], input.dtype)

    def scale(self, scale_range: Tuple[float, float], probability: float = None, always: bool = True):
        scale = tf.random.uniform([], scale_range[0], scale_range[1])

        funcs = {}
        for k, img_type in self.types.items():
            funcs[k] = lambda input: self._scale(input, scale, self._get_interpolation(img_type).lower())
        self._add_operation(funcs, probability, always)

    def translate(self, translate_range: Tuple[float, float], probability: float = None, always: bool = True):
        dx = tf.cast(self.tensor_shape[0], tf.float32) * tf.random.uniform([], translate_range[0], translate_range[1])
        dy = tf.cast(self.tensor_shape[1], tf.float32) * tf.random.uniform([], translate_range[0], translate_range[1])

        funcs = {}
        for k, img_type in self.types.items():
            funcs[k] = lambda input: tf.contrib.image.translate(input, [dx, dy],
                                                                interpolation=self._get_interpolation(img_type))
        self._add_operation(funcs, probability, always)

    def rotate(self, angle_dregrees_range: Tuple[float, float], probability: float = None, always: bool = True):
        angle_rads_range = tuple(degree * math.pi / 180. for degree in angle_dregrees_range)
        angle = tf.random.uniform([], angle_rads_range[0], angle_rads_range[1])

        funcs = {}
        for k, img_type in self.types.items():
            funcs[k] = lambda input: tf.contrib.image.rotate(input, angle,
                                                             interpolation=self._get_interpolation(img_type))
        self._add_operation(funcs, probability, always)

    def _transform_image(self, image: tf.Tensor, forward_transform: tf.Tensor, interpolation: str) -> tf.Tensor:
        t = tf.contrib.image.matrices_to_flat_transforms(tf.linalg.inv(forward_transform))
        return tf.contrib.image.transform(image, t, interpolation=interpolation)

    def _shear(self, intput: tf.Tensor, shear_lambda: tf.Tensor, interpolation: str) -> tf.Tensor:
        return self._transform_image(intput, tf.convert_to_tensor([[1.0, shear_lambda, 0], [0, 1.0, 0], [0, 0, 1.0]]),
                                     interpolation)

    def shear(self, shear_range: Tuple[float, float], probability: float = None, always: bool = True):
        shear_lambda = tf.random.uniform([], shear_range[0], shear_range[1])

        funcs = {}
        for k, img_type in self.types.items():
            funcs[k] = lambda input: self._shear(input, shear_lambda, self._get_interpolation(img_type))
        self._add_operation(funcs, probability, always)

    def _invert(self, intput: tf.Tensor) -> tf.Tensor:
        return tf.image.convert_image_dtype((1 - tf.image.convert_image_dtype(intput, tf.float32)), intput.dtype)

    def invert(self, probability: float, always: bool = True):
        funcs = {}
        for k, img_type in self.types.items():
            if img_type == "image":
                funcs[k] = self._invert
        self._add_operation(funcs, probability, always)


class ImageIterator(Iterator):
    """
    Keras Sequence object to train a model on larger-than-memory data.
    """

    def __init__(self, dataset: ImageDataset, batch_size: int, shuffle: bool = True, seed: int = None):
        super().__init__(len(dataset), batch_size, shuffle, seed)
        self.dataset = dataset

    def _get_batches_of_transformed_samples(self, index_array: List[int]):
        return self.dataset.load_batch(index_array)


def calculate_steps_per_epoch(dataset: ImageDataset, batch_size: int) -> int:
    return math.ceil(len(dataset) / batch_size)
