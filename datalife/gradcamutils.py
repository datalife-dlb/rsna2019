from typing import Tuple, List, Union

from scipy.ndimage.interpolation import zoom
import numpy as np

import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras import Model


def grad_cam(input_model, image, layer_name, width=224, height=224) -> np.ndarray:
    cls = np.argmax(input_model.predict(image))
    def normalize(x):
        """Utility function to normalize a tensor by its L2 norm"""
        return (x + 1e-10) / (K.sqrt(K.mean(K.square(x))) + 1e-10)
    """GradCAM method for visualizing input saliency."""
    y_c = input_model.output[0, cls]
    conv_output = input_model.get_layer(layer_name).output
    grads = K.gradients(y_c, conv_output)[0]
    #grads = normalize(grads)
    gradient_function = K.function([input_model.input], [conv_output, grads])

    output, grads_val = gradient_function([image])
    output, grads_val = output[0, :], grads_val[0, :, :, :]

    weights = np.mean(grads_val, axis=(0, 1))
    cam = np.dot(output, weights)
    #print (cam)

    cam = np.maximum(cam, 0)
    #cam = resize(cam, (height, width))
    cam = zoom(cam, height / cam.shape[0])
    #cam = np.maximum(cam, 0)
    cam = cam / cam.max()
    return cam

def grad_cam_plus(input_model: Model, img: np.ndarray, layer_name, width=224, height=224) -> Tuple[np.ndarray, np.ndarray]:
    model_prediction: Union[List[np.ndarray], np.ndarray] = input_model.predict(img)
    prediction: np.ndarray = model_prediction[0] if isinstance(model_prediction, list) else model_prediction
    cls: int = np.argmax(prediction)
    model_output: Union[List[tf.Tensor], tf.Tensor] = input_model.output
    output = model_output[0] if isinstance(model_output, list) else model_output
    y_c: tf.Tensor = output[0, cls]
    conv_output: tf.Tensor = input_model.get_layer(layer_name).output
    grads = K.gradients(y_c, conv_output)[0]
    #grads = normalize(grads)

    first = K.exp(y_c)*grads
    second = K.exp(y_c)*grads*grads
    third = K.exp(y_c)*grads*grads

    gradient_function = K.function([input_model.input], [y_c,first,second,third, conv_output, grads])
    y_c, conv_first_grad, conv_second_grad,conv_third_grad, conv_output, grads_val = gradient_function([img])
    global_sum = np.sum(conv_output[0].reshape((-1,conv_first_grad[0].shape[2])), axis=0)

    alpha_num = conv_second_grad[0]
    alpha_denom = conv_second_grad[0]*2.0 + conv_third_grad[0]*global_sum.reshape((1,1,conv_first_grad[0].shape[2]))
    alpha_denom = np.where(alpha_denom != 0.0, alpha_denom, np.ones(alpha_denom.shape))
    alphas = alpha_num/alpha_denom

    weights = np.maximum(conv_first_grad[0], 0.0)

    alpha_normalization_constant = np.sum(np.sum(alphas, axis=0),axis=0)

    alphas /= alpha_normalization_constant.reshape((1,1,conv_first_grad[0].shape[2]))

    deep_linearization_weights = np.sum((weights*alphas).reshape((-1,conv_first_grad[0].shape[2])),axis=0)
    #print deep_linearization_weights
    grad_CAM_map = np.sum(deep_linearization_weights*conv_output[0], axis=2)

    # Passing through ReLU
    cam = np.maximum(grad_CAM_map, 0)
    cam = zoom(cam, height / cam.shape[0])
    cam = cam / np.max(cam) # scale 0 to 1.0
    #cam = resize(cam, (224,224))

    return cam, prediction[0]