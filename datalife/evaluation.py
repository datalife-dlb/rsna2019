from typing import Type, Dict


from datalife.task.base import BaseTorchModelTraining, load_torch_model_training_from_task_dir, \
    load_torch_model_training_from_task_id


def evaluate_torch_model_from_task_id(model_cls: Type[BaseTorchModelTraining], task_id: str) -> Dict[str, float]:
    model = load_torch_model_training_from_task_id(model_cls, task_id)
    return evaluate_torch_model(model)


def evaluate_torch_model_from_task_dir(model_cls: Type[BaseTorchModelTraining], task_dir: str) -> Dict[str, float]:
    model = load_torch_model_training_from_task_dir(model_cls, task_dir)
    return evaluate_torch_model(model)


def evaluate_torch_model(model: BaseTorchModelTraining) -> Dict[str, float]:
    trial = model.create_trial(model.get_trained_module())

    return trial.with_val_generator(model.get_test_generator()).evaluate()
