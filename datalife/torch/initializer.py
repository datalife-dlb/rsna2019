from math import sqrt

import torch
import torch.nn as nn
from torch.nn.init import _calculate_fan_in_and_fan_out

def lecun_normal(tensor: torch.Tensor):
    fan_in, _ = _calculate_fan_in_and_fan_out(tensor)
    nn.init.normal_(tensor, 0, sqrt(1. / fan_in))


def lecun_uniform(tensor: torch.Tensor):
    fan_in, _ = _calculate_fan_in_and_fan_out(tensor)
    nn.init.uniform_(tensor, 0, sqrt(1. / fan_in))


def he_normal(tensor: torch.Tensor):
    nn.init.kaiming_normal_(tensor, mode='fan_in')


def he_uniform(tensor: torch.Tensor):
    nn.init.kaiming_uniform(tensor, mode='fan_in')