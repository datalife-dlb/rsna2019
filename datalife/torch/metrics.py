import torch
from torchbearer import metrics


@metrics.default_for_key("binary_accuracy")
@metrics.to_dict
@metrics.lambda_metric("binary_accuracy", on_epoch=False)
def binary_accuracy(y_pred: torch.Tensor, y_true: torch.Tensor, threshold: float = 0.5):
    y_pred = _convert_pred(y_pred, threshold)
    y_true = y_true.float()

    correct = torch.eq(y_pred, y_true).view(-1)
    num_correct = torch.sum(correct).item()
    num_examples = correct.shape[0]
    return num_correct / num_examples

@metrics.default_for_key("precision")
@metrics.to_dict
@metrics.lambda_metric("precision", on_epoch=False)
def precision(y_pred: torch.Tensor, y_true: torch.Tensor, threshold: float = 0.5, eps=1e-9):
    y_pred = _convert_pred(y_pred, threshold)
    y_true = y_true.float()

    true_positive = (y_pred * y_true).sum(dim=1)
    precision = true_positive.div(y_pred.sum(dim=1).add(eps))
    return float(torch.mean(precision))


@metrics.default_for_key("recall")
@metrics.to_dict
@metrics.lambda_metric("recall", on_epoch=False)
def recall(y_pred: torch.Tensor, y_true: torch.Tensor, threshold: float = 0.5, eps=1e-9):
    y_pred = _convert_pred(y_pred, threshold)
    y_true = y_true.float()

    true_positive = (y_pred * y_true).sum(dim=1)
    recall = true_positive.div(y_true.sum(dim=1).add(eps))
    return float(torch.mean(recall))


def _convert_pred(y_pred: torch.Tensor, threshold: float=0.5):
    return torch.ge(y_pred.float(), threshold).float()