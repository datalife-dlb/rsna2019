from typing import Tuple

import numpy as np
import PIL
import PIL.Image


def rle_to_mask(rle: str, width: int, height: int) -> np.ndarray:
    mask = np.zeros(width * height, dtype=np.int32)
    array = np.asarray([int(x) for x in rle.split()])
    starts = array[0::2]
    lengths = array[1::2]

    current_position = 0
    for index, start in enumerate(starts):
        current_position += start
        mask[current_position:current_position + lengths[index]] = 1
        current_position += lengths[index]

    return mask.reshape(height, width).T


def mask_to_rle(img: np.ndarray):
    if len(img.shape) == 3:
        img = np.squeeze(img, -1)

    rle = []
    last_color = 0
    current_pixel = 0
    run_start = -1
    run_length = 0

    for y in range(img.shape[1]):
        for x in range(img.shape[0]):
            current_color = img[x][y]
            if current_color != last_color:
                if current_color == 1:
                    run_start = current_pixel
                    run_length = 1
                else:
                    rle.append(str(run_start))
                    rle.append(str(run_length))
                    run_start = -1
                    run_length = 0
                    current_pixel = 0
            elif run_start > -1:
                run_length += 1
            last_color = current_color
            current_pixel += 1
    return " ".join(rle)


def resize_mask(mask: np.ndarray, target_shape: Tuple[int, int]) -> np.ndarray:
    mask = PIL.Image.fromarray(mask)
    mask = mask.resize(target_shape, resample=PIL.Image.BILINEAR)
    return np.array(mask)
