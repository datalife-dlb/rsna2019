import ast
import os
import json
import warnings
from typing import Type, List, Tuple, Dict

import numpy as np


def get_params_path(task_dir: str) -> str:
    return os.path.join(task_dir, "params.json")


def get_params(task_dir: str) -> dict:
    with open(get_params_path(task_dir), "r") as params_file:
        return json.load(params_file)


def get_classes_path(task_dir: str) -> str:
    return os.path.join(task_dir, "classes.json")


def get_torch_weights_path(task_dir: str) -> str:
    return os.path.join(task_dir, "weights.pt")


def get_keras_weights_path(task_dir: str) -> str:
    return os.path.join(task_dir, "weights.h5")

def get_keras_swa_weights_path(task_dir: str) -> str:
    return os.path.join(task_dir, "weights_swa.h5")

def get_history_path(task_dir: str) -> str:
    return os.path.join(task_dir, "history.csv")


def get_history_plot_path(task_dir: str) -> str:
    return os.path.join(task_dir, "history.jpg")


def get_tensorboard_logdir(task_id: str) -> str:
    return os.path.join("output", "tensorboard_logs", task_id)


def get_task_dir(base_dir: str, model_cls: Type, task_id: str):
    return os.path.join(base_dir, "models", model_cls.__name__, "results", task_id)


def get_operadoras_path():
    return os.path.join("input", "operadoras.csv")


def symlink_files(filepaths: List[str], dirpath: str, new_filenames: List[str] = None):
    if new_filenames is None:
        new_filenames = [os.path.split(filepath)[1] for filepath in filepaths]
    for filepath, filename in zip(filepaths, new_filenames):
        try:
            os.symlink(os.path.abspath(filepath), os.path.join(dirpath, filename))
        except FileExistsError:
            warnings.warn("Failed to sym link %s -> %s. File exists!" % (os.path.abspath(filepath),
                                                                         os.path.join(dirpath, filename)))


def symlink_misclassified_inputs(misclassified_inputs: Dict[str, np.ndarray], dirpath: str):
    filepaths_and_probas: List[Tuple[str, np.ndarray]] = list(misclassified_inputs.items())
    if filepaths_and_probas[0][0].startswith("["):
        filepaths_and_probas = [(filepath, probas) for filepaths, probas in filepaths_and_probas
                                for filepath in ast.literal_eval(filepaths)]
    symlink_files([filepath for filepath, _ in filepaths_and_probas], dirpath,
                  ["%s-%s" % (str(probas), os.path.split(filepath)[1]) for filepath, probas in filepaths_and_probas])
