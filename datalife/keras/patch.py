# Patch K.is_keras_tensor for Tensorflow 1.15.0
import functools

import tensorflow.keras.backend as K
from tensorflow.python.framework import ops as tf_ops
from tensorflow.python.util.tf_export import tf_export
from tensorflow.keras import Model


@tf_export('keras.backend.is_tensor')
def is_tensor(x):
    return isinstance(x, tf_ops._TensorLike) or tf_ops.is_dense_tensor_like(x)


@tf_export('keras.backend.is_keras_tensor')
def is_keras_tensor(x):
    if not is_tensor(x):
        raise ValueError('Unexpectedly found an instance of type `' +
                         str(type(x)) + '`. '
                                        'Expected a symbolic tensor instance.')
    return hasattr(x, '_keras_history')


K.is_tensor = is_tensor
K.is_keras_tensor = is_keras_tensor


_original_load_weights = functools.partial(Model.load_weights, by_name=True)


def load_weights(self, *args, **kwargs):
    return _original_load_weights(self, *args, **kwargs)

Model.load_weights = load_weights
