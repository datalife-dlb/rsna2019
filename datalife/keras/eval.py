import contextlib
import os
from collections import OrderedDict
from typing import Dict, List, Tuple, Union

import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn import metrics
from tensorflow.keras import Model
from tqdm import tqdm

from datalife.data import ImageIterator
from datalife.files import symlink_misclassified_inputs
from datalife.plot import plot_confusion_matrix, plot_roc_curve


class EvaluationReport(object):
    def __init__(self, acc: float, precision: float, recall: float, f1_score: float,
                 confusion_matrix: List[List[float]], misclassified_inputs: Dict[str, np.ndarray] = None):
        self.acc = acc
        self.precision = precision
        self.recall = recall
        self.f1_score = f1_score
        self.confusion_matrix = confusion_matrix
        self.misclassified_inputs = misclassified_inputs


def pred_probas_for_classifier(model: Model, dataset: tf.data.Dataset, steps: int) -> Union[np.ndarray, List[np.ndarray]]:
    return model.predict(dataset, steps=steps, verbose=1)


def evaluate_classifier(probas: np.ndarray, targets: np.ndarray, filenames: List[str],
                        threshold: Union[float, None] = None) -> EvaluationReport:
    if threshold is not None:
        probas_minus_negative = probas[:, 1:]
        highest_probas = np.array([np.argmax(proba) + 1 for proba in probas_minus_negative])
        preds = np.array([i if proba[i] > threshold else 0 for proba, i in zip(probas, highest_probas)])
    else:
        preds = np.array([np.argmax(proba) for proba in probas])

    targets = np.array([np.argmax(_pred) for _pred in targets])

    acc, precision, recall, f1_score, confusion_matrix = calculate_scores(targets, preds)

    misclassified_filenames = np.array(filenames)[targets != preds]
    misclassified_proba_preds = probas[targets != preds]
    misclassified_inputs = {str(filename.tolist() if isinstance(filename, np.ndarray) else filename): proba_pred
                            for filename, proba_pred in zip(misclassified_filenames, misclassified_proba_preds)}

    return EvaluationReport(acc, precision, recall, f1_score, confusion_matrix, misclassified_inputs)


def evaluate_roc_curve(target_index: int, probas: np.ndarray,
                       targets: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray, float]:
    probas = probas[:, target_index]
    targets = targets[:, target_index]

    fpr, tpr, threshold = metrics.roc_curve(targets, probas)
    roc_auc = metrics.auc(fpr, tpr)
    return fpr, tpr, threshold, roc_auc


def calculate_scores(trues: np.ndarray, preds: np.ndarray) -> Tuple[float, float, float, float, List[List[float]]]:
    acc = metrics.accuracy_score(trues, preds)
    precision, recall, f1_score, _ = metrics.precision_recall_fscore_support(trues, preds, average=None)
    confusion_matrix = metrics.confusion_matrix(trues, preds).tolist()

    return acc, precision, recall, f1_score, confusion_matrix


def evaluate_and_report_classifier(train_probas: np.ndarray, train_targets: np.ndarray, train_image_paths: List[str],
                                   val_probas: np.ndarray, val_targets: np.ndarray, val_image_paths: List[str],
                                   thresholds: List[float], target_labels: List[str], output_path: str) -> pd.DataFrame:
    results = []
    print("Evaluating...")
    for threshold in tqdm(thresholds, total=len(thresholds)):
        print("Evaluating training dataset...")
        train_report = evaluate_classifier(train_probas, train_targets, train_image_paths)
        print("Finished evaluating training dataset")
        print("Evaluating validation dataset...")
        val_report = evaluate_classifier(val_probas, val_targets, val_image_paths)
        print("Finished evaluating validation dataset")

        train_misclassified_images_for_threshold_path = os.path.join(output_path,
                                                                     "train_misclassified_images_%.2f" % threshold
                                                                     if threshold else "train_misclassified_images")
        val_misclassified_images_for_threshold_path = os.path.join(output_path,
                                                                   "val_misclassified_images_%.2f" % threshold
                                                                   if threshold else "val_misclassified_images")

        os.makedirs(train_misclassified_images_for_threshold_path, exist_ok=True)
        os.makedirs(val_misclassified_images_for_threshold_path, exist_ok=True)

        symlink_misclassified_inputs(train_report.misclassified_inputs,
                                     train_misclassified_images_for_threshold_path)
        symlink_misclassified_inputs(val_report.misclassified_inputs, val_misclassified_images_for_threshold_path)

        results.append(
            OrderedDict(threshold=threshold, acc=train_report.acc, precision=train_report.precision,
                        recall=train_report.recall, f1_score=train_report.f1_score, val_acc=val_report.acc,
                        val_precision=val_report.precision, val_recall=val_report.recall,
                        val_f1_score=val_report.f1_score, ))

        train_confusion_matrix_fig = plot_confusion_matrix(train_report.confusion_matrix, target_labels)
        train_confusion_matrix_fig.savefig(os.path.join(output_path,
                                                        "confusion_matrix_%.2f.png" % threshold
                                                        if threshold else "confusion_matrix.png"))
        val_confusion_matrix_fig = plot_confusion_matrix(val_report.confusion_matrix, target_labels)
        val_confusion_matrix_fig.savefig(
            os.path.join(output_path,
                         "val_confusion_matrix_%.2f.png" % threshold if threshold else "val_confusion_matrix.png"))
        plt.close(train_confusion_matrix_fig)
        plt.close(val_confusion_matrix_fig)

    df = pd.DataFrame(results)
    df.to_csv(os.path.join(output_path, "report.csv"), index=False)
    return df


def plot_roc_curves(train_probas: np.ndarray, train_targets: np.ndarray, val_probas: np.ndarray,
                    val_targets: np.ndarray, target_labels: List[str], output_path: str):
    print("Plotting ROC curves...")
    for i, label in enumerate(tqdm(target_labels, total=len(target_labels))):
        fpr, tpr, _, roc_auc = evaluate_roc_curve(i, train_probas, train_targets)
        train_roc_curve_fig = plot_roc_curve(fpr, tpr, roc_auc)
        train_roc_curve_fig.savefig(os.path.join(output_path, "train_roc_curve_[%s].png" % label))
        fpr, tpr, _, roc_auc = evaluate_roc_curve(i, val_probas, val_targets)
        val_roc_curve_fig = plot_roc_curve(fpr, tpr, roc_auc)
        val_roc_curve_fig.savefig(os.path.join(output_path, "val_roc_curve_[%s].png" % label))
        plt.close(train_roc_curve_fig)
        plt.close(val_roc_curve_fig)
