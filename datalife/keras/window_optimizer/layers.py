from typing import Dict, Callable

from tensorflow.keras.layers import Conv2D, Activation
from tensorflow.keras.activations import sigmoid

from datalife.keras.window_optimizer.activations import upbound_relu, upbound_sigmoid
from datalife.utils import no_params_wrapper

_ACTIVATIONS: Dict[str, Callable[[float], Callable]] = dict(relu=upbound_relu, sigmoid=upbound_sigmoid,
                                                            regular_sigmoid=no_params_wrapper(sigmoid))


def WindowOptimizerConv2D(filters: int, **kwargs) -> Conv2D:
    return Conv2D(filters=filters, kernel_size=(1, 1), strides=(1, 1), padding="same", name="window_conv", **kwargs)


def WindowOptimizerActivation(activation: str, upbound_window: float) -> Activation:
    return Activation(_ACTIVATIONS[activation](upbound_window), name="window_activation")
