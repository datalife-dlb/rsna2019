import tensorflow.keras.backend as K


def upbound_relu(upbound_value: float):
    def inner_upbound_relu(x):
        return K.minimum(K.maximum(x, 0), upbound_value)
    return inner_upbound_relu


def upbound_sigmoid(upbound_value: float):
    def inner_upbound_sigmoid(x):
        return upbound_value * K.sigmoid(x)
    return inner_upbound_sigmoid
