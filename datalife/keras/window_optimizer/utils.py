import math
from typing import Tuple, List

from tensorflow.keras import Model


def get_init_conv_params_relu(wl: float, ww: float, upbound_value: float = 255.) -> Tuple[float, float]:
    w = upbound_value / ww
    b = -1. * upbound_value * (wl - ww / 2.) / ww
    return w, b


def get_init_conv_params_sigmoid(wl: float, ww: float, upbound_value: float = 255.,
                                 smooth: float = 1., ) -> Tuple[float, float]:
    w = 2. / ww * math.log(upbound_value / smooth - 1.)
    b = -2. * wl / ww * math.log(upbound_value / smooth - 1.)
    return w, b


def get_window_settings_relu(w: float, b: float, upbound_value: float = 255.) -> Tuple[float, float]:
    wl = upbound_value / (2. * w) - b / w
    ww = upbound_value / w
    return wl, ww


def get_window_settings_sigmoid(w: float, b: float, upbound_value: float = 255.,
                                smooth: float = 1.) -> Tuple[float, float]:
    wl = - b / w
    ww = 2. / w * math.log(upbound_value / smooth - 1.)
    return wl, ww


def initialize_windows(model: Model, act_window: str, init_windows: List[Tuple[float, float]],
                       upbound_window: float, conv_layer_name="window_conv"):
    '''
    :param model:
    :param act_window: str. 'sigmoid', 'regular_sigmoid' or 'relu'
    :param init_windows: list of tuples
    :param conv_layer_name: str. a name of window
    :return: model. with loaded weight.
    '''

    # get all layer names
    layer_names = [layer.name for layer in model.layers]

    w_conv, b_conv = model.layers[layer_names.index(conv_layer_name)].get_weights()
    n_windows = w_conv.shape[-1]
    num_init_windows = len(init_windows)
    assert num_init_windows <= n_windows

    for idx, (wl, ww) in enumerate(init_windows):
        w_new, b_new = get_init_conv_params_relu(wl, ww, upbound_window) if act_window == "relu" \
            else get_init_conv_params_sigmoid(wl, ww, upbound_window)
        w_conv[0, 0, 0, idx] = w_new
        b_conv[idx] = b_new

    model.layers[layer_names.index(conv_layer_name)].set_weights([w_conv, b_conv])

    return model


def get_windows(model: Model, act_window: str,  upbound_window: float,
                conv_layer_name="window_conv") -> List[Tuple[float, float]]:
    # get all layer names
    layer_names = [layer.name for layer in model.layers]

    w_conv, b_conv = model.layers[layer_names.index(conv_layer_name)].get_weights()
    n_windows = w_conv.shape[-1]

    windows: List[Tuple[float, float]] = []

    for idx in range(n_windows):
        w = w_conv[0, 0, 0, idx]
        b = b_conv[idx]

        wl, ww = get_window_settings_relu(w, b, upbound_window) if act_window == "relu"\
            else get_window_settings_sigmoid(w, b, upbound_window)

        windows.append((wl, ww))

    return windows