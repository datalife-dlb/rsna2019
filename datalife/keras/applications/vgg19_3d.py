"""VGG16_3D model for Keras.

# Reference

- [Very Deep Convolutional Networks for Large-Scale Image Recognition](
    https://arxiv.org/abs/1409.1556)

"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

from tensorflow.keras import Model
from tensorflow.keras.layers import Input, Activation, Flatten, Dense, Conv3D, MaxPooling3D, GlobalAveragePooling3D,\
    GlobalMaxPooling3D, BatchNormalization
import tensorflow.keras.backend as K
from tensorflow.keras.utils import get_source_inputs


def conv_block(units, activation='relu', block=1, layer=1, batch_normalization=False):
    def layer_wrapper(inp):
        x = Conv3D(units, (3, 3, 3), padding='same', name='block{}_conv{}'.format(block, layer))(inp)
        if batch_normalization:
            x = BatchNormalization(name='block{}_bn{}'.format(block, layer))(x)
        x = Activation(activation, name='block{}_activation{}'.format(block, layer))(x)
        return x

    return layer_wrapper


def dense_block(units, activation='relu', name='fc1', batch_normalization=False):
    def layer_wrapper(inp):
        x = Dense(units, name=name)(inp)
        if batch_normalization:
            x = BatchNormalization(name='{}_bn'.format(name))(x)
        x = Activation(activation, name='{}_activation'.format(name))(x)
        return x

    return layer_wrapper


def VGG19_3D_BN(include_top=True,
                weights='imagenet',
                input_tensor=None,
                input_shape=None,
                pooling=None,
                classes=1000):
    return VGG19_3D(include_top, weights, input_tensor, input_shape, pooling, classes, batch_normalization=True)


def VGG19_3D(include_top=True,
             weights=None,
             input_tensor=None,
             input_shape=None,
             pooling=None,
             classes=1000,
             batch_normalization=False):
    """Instantiates the VGG16_3D architecture.

    # Arguments
        include_top: whether to include the 3 fully-connected
            layers at the top of the network.
        weights: one of `None` (random initialization),
              'imagenet' (pre-training on ImageNet),
              or the path to the weights file to be loaded.
        input_tensor: optional Keras tensor
            (i.e. output of `layers.Input()`)
            to use as image input for the model.
        input_shape: optional shape tuple, only to be specified
            if `include_top` is False (otherwise the input shape
            has to be `(224, 224, 224, 3)`
            (with `channels_last` data format)
            or `(3, 224, 224, 224)` (with `channels_first` data format).
            It should have exactly 3 input channels,
            and width and height should be no smaller than 48.
            E.g. `(200, 200, 200, 3)` would be one valid value.
        pooling: Optional pooling mode for feature extraction
            when `include_top` is `False`.
            - `None` means that the output of the model will be
                the 5D tensor output of the
                last convolutional layer.
            - `avg` means that global average pooling
                will be applied to the output of the
                last convolutional layer, and thus
                the output of the model will be a 3D tensor.
            - `max` means that global max pooling will
                be applied.
        classes: optional number of classes to classify images
            into, only to be specified if `include_top` is True, and
            if no `weights` argument is specified.

    # Returns
        A Keras model instance.

    # Raises
        ValueError: in case of invalid argument for `weights`,
            or invalid input shape.
    """
    if not(weights is None or os.path.exists(weights)):
        raise ValueError('The `weights` argument should be either '
                         '`None` (random initialization) '
                         'or the path to the weights file to be loaded.')

    if input_tensor is None:
        img_input = Input(shape=input_shape)
    else:
        if not K.is_keras_tensor(input_tensor):
            img_input = Input(tensor=input_tensor, shape=input_shape)
        else:
            img_input = input_tensor
    # Block 1
    x = conv_block(64, activation='relu', block=1, layer=1, batch_normalization=batch_normalization)(img_input)
    x = conv_block(64, activation='relu', block=1, layer=2, batch_normalization=batch_normalization)(x)
    x = MaxPooling3D((2, 2, 2), strides=(2, 2, 2), name='block1_pool')(x)

    # Block 2
    x = conv_block(128, activation='relu', block=2, layer=1, batch_normalization=batch_normalization)(x)
    x = conv_block(128, activation='relu', block=2, layer=2, batch_normalization=batch_normalization)(x)
    x = MaxPooling3D((2, 2, 2), strides=(2, 2, 2), name='block2_pool')(x)

    # Block 3
    x = conv_block(256, activation='relu', block=3, layer=1, batch_normalization=batch_normalization)(x)
    x = conv_block(256, activation='relu', block=3, layer=2, batch_normalization=batch_normalization)(x)
    x = conv_block(256, activation='relu', block=3, layer=3, batch_normalization=batch_normalization)(x)
    x = conv_block(256, activation='relu', block=3, layer=4, batch_normalization=batch_normalization)(x)
    x = MaxPooling3D((2, 2, 2), strides=(2, 2, 2), name='block3_pool')(x)

    # Block 4
    x = conv_block(512, activation='relu', block=4, layer=1, batch_normalization=batch_normalization)(x)
    x = conv_block(512, activation='relu', block=4, layer=2, batch_normalization=batch_normalization)(x)
    x = conv_block(512, activation='relu', block=4, layer=3, batch_normalization=batch_normalization)(x)
    x = conv_block(512, activation='relu', block=4, layer=4, batch_normalization=batch_normalization)(x)
    x = MaxPooling3D((2, 2, 2), strides=(2, 2, 2), name='block4_pool')(x)

    # Block 5
    x = conv_block(512, activation='relu', block=5, layer=1, batch_normalization=batch_normalization)(x)
    x = conv_block(512, activation='relu', block=5, layer=2, batch_normalization=batch_normalization)(x)
    x = conv_block(512, activation='relu', block=5, layer=3, batch_normalization=batch_normalization)(x)
    x = conv_block(512, activation='relu', block=5, layer=4, batch_normalization=batch_normalization)(x)
    x = MaxPooling3D((2, 2, 2), strides=(2, 2, 2), name='block5_pool')(x)

    if include_top:
        # Classification block
        x = Flatten(name='flatten')(x)
        x = dense_block(4096, activation='relu', name='fc1', batch_normalization=batch_normalization)(x)
        x = dense_block(4096, activation='relu', name='fc2', batch_normalization=batch_normalization)(x)
        x = Dense(classes, activation='softmax', name='predictions')(x)
    else:
        if pooling == 'avg':
            x = GlobalAveragePooling3D()(x)
        elif pooling == 'max':
            x = GlobalMaxPooling3D()(x)

    # Ensure that the model takes into account
    # any potential predecessors of `input_tensor`.
    if input_tensor is not None:
        inputs = get_source_inputs(input_tensor)
    else:
        inputs = img_input
    # Create model.
    model = Model(inputs, x, name='vgg19_3d')

    # Load weights.
    if weights is not None:
        model.load_weights(weights)

    return model
