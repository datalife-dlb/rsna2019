import math
from typing import List, Tuple, Union

import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.callbacks import Callback, LearningRateScheduler


class StepLR(LearningRateScheduler):
    def __init__(self, step_size, gamma=0.1, verbose=0):
        super().__init__(self._get_lr, verbose)
        self._step_size = step_size
        self._gamma = gamma

    def _get_lr(self, epoch: int, lr: float):
        if not hasattr(self, "_lr0"):
            self._lr0 = lr

        return self._lr0 * self._gamma ** (epoch // self._step_size)


class ExponentialLR(LearningRateScheduler):
    def __init__(self, gamma, verbose=0):
        super().__init__(self._get_lr, verbose)
        self._gamma = gamma

    def _get_lr(self, epoch: int, lr: float):
        if not hasattr(self, "_lr0"):
            self._lr0 = lr

        return self._lr0 * self._gamma ** epoch


class CosineAnnealingLR(LearningRateScheduler):
    def __init__(self, T_max, eta_min=0, verbose=0):
        super().__init__(self._get_lr, verbose)
        self._T_max = T_max
        self._eta_min = eta_min

    def _get_lr(self, epoch: int, lr: float):
        if not hasattr(self, "_lr0"):
            self._lr0 = lr

        return self._eta_min + (self._lr0 - self._eta_min) * (1 + math.cos(math.pi * epoch / self._T_max)) / 2


class CosineAnnealingWithRestartsLR(CosineAnnealingLR):
    def __init__(self, T_max, eta_min=0, T_mult=1, verbose=0):
        super().__init__(T_max, eta_min, verbose)

        self._T_mult = T_mult

        self._restart_every = T_max
        self._restarts = 0
        self._restarted_at = 0

    def _restart(self, epoch: int):
        self._restart_every *= self._T_mult
        self._restarted_at = epoch

    def _get_lr(self, epoch: int, lr: float):
        if (epoch - self._restarted_at) >= self._restart_every:
            self._restart(epoch)

        return super()._get_lr(epoch, lr)


class WarmUpLearningRateScheduler(Callback):
    """Warmup learning rate scheduler
    """

    def __init__(self, warmup_batches: int, init_lr: float, final_lr: float, lr_scheduler: LearningRateScheduler = None,
                 verbose=0):
        """Constructor for warmup learning rate scheduler

        Arguments:
            warmup_batches {int} -- Number of batch for warmup.
            init_lr {float} -- Learning rate after warmup.

        Keyword Arguments:
            verbose {int} -- 0: quiet, 1: update messages. (default: {0})
        """

        super().__init__()
        self.warmup_batches = warmup_batches
        self.init_lr = init_lr
        self.slope = (final_lr - init_lr) / warmup_batches
        self.verbose = verbose
        self.batch_count = 0
        self.learning_rates = []

        self.lr_scheduler = lr_scheduler

    def set_params(self, params):
        super().set_params(params)
        if self.lr_scheduler:
            self.lr_scheduler.set_params(params)

    def set_model(self, model):
        super().set_model(model)
        if self.lr_scheduler:
            self.lr_scheduler.set_model(model)

    def on_batch_begin(self, batch, logs=None):
        if self.batch_count <= self.warmup_batches:
            lr = self.slope * self.batch_count + self.init_lr
            K.set_value(self.model.optimizer.lr, lr)
            if self.verbose > 0:
                print('\nBatch %05d: WarmUpLearningRateScheduler setting learning '
                      'rate to %s.' % (self.batch_count + 1, lr))

    def on_batch_end(self, batch, logs=None):
        self.batch_count = self.batch_count + 1
        lr = K.get_value(self.model.optimizer.lr)
        self.learning_rates.append(lr)

    def on_epoch_begin(self, epoch, logs=None):
        if self.lr_scheduler and self.batch_count > self.warmup_batches:
            self.lr_scheduler.on_epoch_begin(epoch, logs)

    def on_epoch_end(self, epoch, logs=None):
        if self.lr_scheduler and self.batch_count > self.warmup_batches:
            self.lr_scheduler.on_epoch_end(epoch, logs)


class LearningRateFinder(Callback):
    def __init__(self, iterations: int, initial_lr: float, end_lr: float = 10.0, linear=False, stop_dv=True) -> None:
        super().__init__()

        ratio = end_lr / initial_lr
        self._gamma = (ratio / iterations) if linear else ratio ** (1 / iterations)

        self._iterations = iterations
        self._current_iteration = 0
        self._stop_dv = stop_dv
        self._best_loss = 1e9
        self.learning_rates: List[float] = []
        self.loss_values: List[float] = []

    def on_batch_begin(self, batch: int, logs=None):
        lr = float(K.get_value(self.model.optimizer.lr))
        if not hasattr(self, "_lr0"):
            self._lr0 = lr

        lr = self._lr0 * self._gamma ** batch

        K.set_value(self.model.optimizer.lr, lr)

    def on_batch_end(self, batch: int, logs=None):
        self._current_iteration += 1

        self.learning_rates.append(float(K.get_value(self.model.optimizer.lr)))
        loss = logs['loss']
        self.loss_values.append(loss)

        if loss < self._best_loss:
            self._best_loss = loss

        if self._current_iteration >= self._iterations or (self._stop_dv and loss > 10 * self._best_loss):
            self.model.stop_training = True

    def get_loss_derivatives(self, sma: int = 1):
        derivatives = [0] * (sma + 1)
        for i in range(1 + sma, len(self.loss_values)):
            derivative = (self.loss_values[i] - self.loss_values[i - sma]) / sma
            derivatives.append(derivative)
        return derivatives


class PlotTraining(Callback):
    def __init__(self, output_path):
        super(PlotTraining, self).__init__()
        self.output_path = output_path

    def on_train_begin(self, logs={}):
        self.x = []
        self.values = {}

    def on_epoch_end(self, epoch, logs={}):
        self.x.append(epoch)

        for k in logs.keys():
            if k in self.values:
                self.values[k].append(logs.get(k))
            else:
                self.values[k] = [logs.get(k)]

            if not "val_" in k:
                try:
                    plt.figure()
                    plt.plot(self.x, self.values[k], label=k)
                    plt.plot(self.x, self.values["val_{}".format(k)], label="val_{}".format(k))
                    plt.legend()
                    plt.savefig("{}/plot_train_{}.png".format(self.output_path, k))
                    plt.close()
                except Exception as e:
                    # print('error:', e.args)
                    pass


class StochasticWeightAveraging(Callback):

    def __init__(self, filepath, swa_epoch):
        super(StochasticWeightAveraging, self).__init__()
        self.filepath = filepath
        self.swa_epoch = swa_epoch
        self.swa_weights = None

    def on_train_begin(self, logs=None):
        self.nb_epoch = self.params['epochs']
        print('Stochastic weight averaging selected for last {} epochs.'
              .format(self.nb_epoch - self.swa_epoch))

    def on_epoch_end(self, epoch, logs=None):

        if epoch == self.swa_epoch:
            self.swa_weights = self.model.get_weights()

        elif epoch > self.swa_epoch:
            print('Applying Stochastic weight averaging for epoch {}'.format(epoch + 1))
            for i in range(len(self.swa_weights)):
                self.swa_weights[i] = (self.swa_weights[i] *
                                       (epoch - self.swa_epoch) + self.model.get_weights()[i]) / (
                                              (epoch - self.swa_epoch) + 1)

        else:
            pass

    def on_train_end(self, logs=None):
        self.model.set_weights(self.swa_weights)
        self.model.save_weights(self.filepath)


class FineTuningCallback(Callback):
    def __init__(self, frozen_layers_at_epochs: List[Tuple[Union[int, float], int]],
                 strategy = None,  # type: tf.contrib.distribute.Strategy
                 verbose: int = 1):
        super().__init__()
        self.frozen_layers_at_epochs = frozen_layers_at_epochs
        self.strategy = strategy
        self.verbose = verbose

    def _recompile(self):
        self.model.compile(optimizer=self.model.optimizer, loss=self.model.loss,
                           loss_weights=self.model.loss_weights, metrics=self.model.metrics)

    def on_epoch_begin(self, epoch: int, logs=None):
        for frozen_layers, at_epoch in self.frozen_layers_at_epochs:
            if isinstance(frozen_layers, float):
                assert frozen_layers <= 1.0
                frozen_layers = int(frozen_layers * len(self.model.layers))
            if epoch == at_epoch:
                if self.verbose > 0:
                    print('\nEpoch %05d: Freezing the first %d layers and unfreezing the rest.' % (
                    epoch + 1, frozen_layers))
                for layer in self.model.layers[frozen_layers:]:
                    layer.trainable = False
                for layer in self.model.layers[:frozen_layers]:
                    layer.trainable = True

                if self.strategy:
                    with self.strategy.scope():
                        self._recompile()
                else:
                    self._recompile()
