from typing import Dict, Callable

import numpy as np

_EPSILON = 1e-7


def rescale(image: np.ndarray, _: dict = None) -> np.ndarray:
    return image / 255.


def rescale_zero_centered(image: np.ndarray, _: dict = None) -> np.ndarray:
    return image / 127.5 - 1.


def zero_center(image: np.ndarray, _: dict = None) -> np.ndarray:
    return image - 127.5


def samplewise_zero_center(image: np.ndarray, _: dict = None) -> np.ndarray:
    return image - np.mean(image, keepdims=True)


def samplewise_zero_center_with_std_normalization(image: np.ndarray, _: dict = None) -> np.ndarray:
    image = samplewise_zero_center(image)
    return image / (np.std(image, keepdims=True) + _EPSILON)


def zero_center_custom_mean(image: np.ndarray, params: dict) -> np.ndarray:
    image = np.copy(image)

    red_mean = params["red_mean"]
    green_mean = params["green_mean"]
    blue_mean = params["blue_mean"]

    if params["image_data_format"] == 'channels_first':
        if image.ndim == 3:
            image[0, :, :] -= red_mean
            image[1, :, :] -= green_mean
            image[2, :, :] -= blue_mean
        else:
            image[:, 0, :, :] -= red_mean
            image[:, 1, :, :] -= green_mean
            image[:, 2, :, :] -= blue_mean
    else:
        image[..., 0] -= red_mean
        image[..., 1] -= green_mean
        image[..., 2] -= blue_mean

    return image


def zero_center_imagenet_mean(image: np.ndarray, params: dict) -> np.ndarray:
    return zero_center_custom_mean(image, {"red_mean": 103.939, "green_mean": 116.779, "blue_mean": 123.68, **params})


def none(image: np.ndarray, _: dict = None) -> np.ndarray:
    return image

IMAGE_PREPROCESSING_FUNCTIONS: Dict[str, Callable[[np.ndarray, dict], np.ndarray]] = dict(rescale=rescale,
                                                                                          rescale_zero_centered=rescale_zero_centered,
                                                                                          zero_center=zero_center,
                                                                                          zero_center_custom_mean=zero_center_custom_mean,
                                                                                          zero_center_imagenet_mean=zero_center_imagenet_mean,
                                                                                          samplewise_zero_center=samplewise_zero_center,
                                                                                          samplewise_zero_center_with_std_normalization=samplewise_zero_center_with_std_normalization,
                                                                                          none=none)