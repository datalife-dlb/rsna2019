import functools
import os
from glob import glob
from multiprocessing.pool import Pool
from typing import List, Tuple, Callable

import PIL.Image
import PIL.ImageOps
import luigi
import numpy as np
import pandas as pd
import pydicom
import pydicom.multival
from tqdm import tqdm

import pydicom_PIL
from datalife.task.data_preparation.base import BasePrepareDataFrames, BaseResizeImages
from datalife.utils import row_with_one_hot_to_list, get_first_of_dicom_field_as_int

BASE_DIR: str = os.path.join("output", "intracranial_hemorrhage_detection")
DATASET_DIR: str = os.path.join(BASE_DIR, "dataset")


class CheckStage1Dataset(luigi.Task):
    def output(self):
        return luigi.LocalTarget(
            os.path.join(BASE_DIR, "rsna-intracranial-hemorrhage-detection", "stage_1_train_images")), \
               luigi.LocalTarget(os.path.join(BASE_DIR, "rsna-intracranial-hemorrhage-detection", "stage_1_train.csv")), \
               luigi.LocalTarget(
                   os.path.join(BASE_DIR, "rsna-intracranial-hemorrhage-detection", "stage_1_test_images"))

    def run(self):
        raise AssertionError(
            f"Not found: {[output.path for output in self.output()]}")


class CheckStage2Dataset(CheckStage1Dataset):
    def output(self):
        return luigi.LocalTarget(
            os.path.join(BASE_DIR, "rsna-intracranial-hemorrhage-detection", "stage_2_train_images")), \
               luigi.LocalTarget(os.path.join(BASE_DIR, "rsna-intracranial-hemorrhage-detection", "stage_2_train.csv")), \
               luigi.LocalTarget(
                   os.path.join(BASE_DIR, "rsna-intracranial-hemorrhage-detection", "stage_2_test_images"))


def _to_hounsfield_units(img: np.ndarray, intercept: int, slope: int) -> np.ndarray:
    return img * slope + intercept


def _window_image(img: np.ndarray, window_center: int, window_width: int, intercept: int, slope: int) -> np.ndarray:
    return pydicom_PIL.get_linear_lut_value(_to_hounsfield_units(img, intercept, slope), window_width, window_center)


def _convert_dicom_image_with_single_window(dicom_image_path: str, output_path: str,
                                            window_width: int = None, window_center: int = None):
    try:
        dicom = pydicom.read_file(dicom_image_path)

        window_width = window_width or get_first_of_dicom_field_as_int(dicom.WindowWidth)
        window_center = window_center or get_first_of_dicom_field_as_int(dicom.WindowCenter)
        intercept = get_first_of_dicom_field_as_int(dicom.RescaleIntercept)
        slope = get_first_of_dicom_field_as_int(dicom.RescaleSlope)

        image = PIL.Image.fromarray(_window_image(dicom.pixel_array, window_center, window_width,
                                                  intercept, slope)).convert('L')

        name: str = os.path.split(dicom_image_path)[1].split(".")[0]
        image.save(os.path.join(output_path, name + ".png"))
    except Exception as e:
        print("Erro no arquivo:", dicom_image_path)
        # raise e


def _convert_dicom_image_three_windows(dicom_image_path: str, output_path: str, window_widths: List[int],
                                       window_centers: List[int], grayscale: bool = False):
    assert len(window_widths) == 3
    assert len(window_centers) == 3
    try:
        dicom = pydicom.read_file(dicom_image_path)

        intercept = get_first_of_dicom_field_as_int(dicom.RescaleIntercept)
        slope = get_first_of_dicom_field_as_int(dicom.RescaleSlope)

        channels: List[np.ndarray] = []
        for window_width, window_center in zip(window_widths, window_centers):
            channels.append(_window_image(dicom.pixel_array, window_center, window_width,
                                          intercept, slope))
        image = PIL.Image.fromarray(np.dstack(channels).astype(np.uint8))
        if grayscale:
            image = image.convert("L")

        name: str = os.path.split(dicom_image_path)[1].split(".")[0]
        image.save(os.path.join(output_path, name + ".png"))
    except Exception as e:
        print("Erro no arquivo:", dicom_image_path)
        # raise e


def _convert_dicom_image_with_no_window(dicom_image_path: str, output_path: str):
    try:
        dicom = pydicom.read_file(dicom_image_path)

        intercept = get_first_of_dicom_field_as_int(dicom.RescaleIntercept)
        slope = get_first_of_dicom_field_as_int(dicom.RescaleSlope)

        image = PIL.Image.fromarray(_to_hounsfield_units(dicom.pixel_array, intercept, slope))

        name: str = os.path.split(dicom_image_path)[1].split(".")[0]
        image.save(os.path.join(output_path, name + ".png"))
    except Exception as e:
        print("Erro no arquivo:", dicom_image_path)
        # raise e


class ConvertDicomImagesFromRSNA2019Stage1Dataset(luigi.Task):
    window_mode: str = luigi.ChoiceParameter(choices=[
        "from_dicom", "three_windows", "three_window_grayscale", "single_window", "raw",
    ], default="from_dicom")
    window_widths: List[int] = luigi.ListParameter(default=[200, 200, 200])
    window_centers: List[int] = luigi.ListParameter(default=[50, 200, 350])

    def requires(self):
        return CheckStage1Dataset()

    @staticmethod
    def _format_window(window_width: int, window_center: int) -> str:
        return "%d..%d" % (window_center - window_width // 2, window_center + window_width // 2)

    def output(self):
        if self.window_mode == "from_dicom":
            return luigi.LocalTarget(os.path.join(DATASET_DIR, "stage_1_images"))
        elif self.window_mode == "three_windows" or self.window_mode == "three_window_grayscale":
            assert len(self.window_widths) == 3
            assert len(self.window_centers) == 3

            path = os.path.join(DATASET_DIR, "stage_1_images_three_windows__%s,%s,%s" % (
                self._format_window(self.window_widths[0], self.window_centers[0]),
                self._format_window(self.window_widths[1], self.window_centers[1]),
                self._format_window(self.window_widths[2], self.window_centers[2]),))
            if self.window_mode == "three_window_grayscale":
                path += "_grayscale"
            return luigi.LocalTarget(path)
        elif self.window_mode == "single_window":
            assert len(self.window_widths) == 1
            assert len(self.window_centers) == 1

            return luigi.LocalTarget(os.path.join(DATASET_DIR, "stage_1_images_single_window__%s" %
                                                  self._format_window(self.window_widths[0], self.window_centers[0])))
        else:
            return luigi.LocalTarget(os.path.join(DATASET_DIR, "stage_1_images_raw"))

    def run(self):
        output_path = self.output().path
        os.makedirs(output_path, exist_ok=True)

        dicom_image_paths: List[str] = glob(os.path.join(self.input()[0].path, "*.dcm")) + \
                                       glob(os.path.join(self.input()[2].path, "*.dcm"))

        if self.window_mode == "from_dicom":
            convert_fn = functools.partial(_convert_dicom_image_with_single_window, output_path=output_path)
        elif self.window_mode == "three_windows" or self.window_mode == "three_window_grayscale":
            convert_fn = functools.partial(_convert_dicom_image_three_windows, window_widths=self.window_widths,
                                           window_centers=self.window_centers, output_path=output_path,
                                           grayscale=self.window_mode == "three_window_grayscale")
        elif self.window_mode == "single_window":
            convert_fn = functools.partial(_convert_dicom_image_with_single_window, output_path=output_path,
                                           window_width=self.window_widths[0], window_center=self.window_centers[0])
        else:
            convert_fn = functools.partial(_convert_dicom_image_with_no_window, output_path=output_path)

        with Pool(os.cpu_count()) as p:
            list(tqdm(p.imap(convert_fn, dicom_image_paths), total=len(dicom_image_paths)))


class ConvertDicomImagesFromRSNA2019Stage2Dataset(ConvertDicomImagesFromRSNA2019Stage1Dataset):
    def requires(self):
        return CheckStage2Dataset()

    def output(self):
        return luigi.LocalTarget(super().output().path.replace("stage_1", "stage_2"))


class TransformRSNA2019Stage1DataFrames(luigi.Task):
    def requires(self):
        return CheckStage1Dataset()

    def output(self):
        return luigi.LocalTarget(os.path.join(DATASET_DIR, "stage_1_train.csv")), \
               luigi.LocalTarget(os.path.join(DATASET_DIR, "stage_1_test.csv"))

    def run(self):
        os.makedirs(DATASET_DIR, exist_ok=True)

        print("Reading stage_1_train.csv...")
        train_df = pd.read_csv(self.input()[1].path)
        print("Running transformations...")
        split_df = train_df["ID"].str.rsplit("_", n=1, expand=True)
        train_df["imageId"] = split_df[0]
        train_df["hemorrhageType"] = split_df[1]

        train_df = train_df.pivot_table(values="Label", index="imageId", columns=["hemorrhageType"]).reset_index()
        train_df["hemorrhages"] = train_df.apply(functools.partial(
            row_with_one_hot_to_list,
            columns=["any", "epidural", "intraparenchymal", "intraventricular", "subarachnoid", "subdural"]), axis=1)
        train_df["fileName"] = train_df["imageId"].apply(lambda image_id: image_id + ".png")

        print("Extracting extra information from train DICOMs...")
        extra_infos: List[dict] = []
        for image_id in tqdm(train_df["imageId"], total=len(train_df)):
            dicom = pydicom.read_file(os.path.join(self.input()[0].path, image_id + ".dcm"), stop_before_pixels=True)
            extra_infos.append({
                "patient": dicom.PatientID,
                "study": dicom.StudyInstanceUID,
                "series": dicom.SeriesInstanceUID,
                "zPosition": dicom.ImagePositionPatient[2],
            })
        train_df = train_df.join(pd.DataFrame(extra_infos, columns=["patient", "study", "series", "zPosition"]))
        train_df = self._add_instance_number(train_df)
        print("Saving stage_1_train.csv...")
        train_df.to_csv(self.output()[0].path, index=False)

        print("Extracting extra information from test DICOMs...")
        dicom_test_image_paths: List[str] = glob(os.path.join(self.input()[2].path, "*.dcm"))
        test_rows: List[dict] = []
        for dicom_image_path in tqdm(dicom_test_image_paths, total=len(dicom_test_image_paths)):
            dicom = pydicom.read_file(dicom_image_path, stop_before_pixels=True)
            image_id: str = os.path.split(dicom_image_path)[1].split(".")[0]
            test_rows.append({
                "imageId": image_id,
                "fileName": image_id + ".png",
                "patient": dicom.PatientID,
                "study": dicom.StudyInstanceUID,
                "series": dicom.SeriesInstanceUID,
                "zPosition": dicom.ImagePositionPatient[2],
                "any": -1,
                "epidural": -1,
                "intraparenchymal": -1,
                "intraventricular": -1,
                "subarachnoid": -1,
                "subdural": -1,
                "hemorrhages": "[]",
            })
        print("Saving stage_1_test.csv...")
        test_df = pd.DataFrame(test_rows)
        test_df = self._add_instance_number(test_df)
        test_df.to_csv(self.output()[1].path, index=False)

    @staticmethod
    def _add_instance_number(df: pd.DataFrame) -> pd.DataFrame:
        df["zPosition"] = df["zPosition"].astype(float)
        df = df.sort_values(by=["series", "zPosition"])
        df["instanceNumber"] = df.groupby("series").cumcount() + 1
        max_instance_numbers = df.groupby(["series"])["instanceNumber"].max()
        df["maxInstanceNumber"] = df.series.apply(lambda series: max_instance_numbers[series])
        df["relativeInstanceNumber"] = df["instanceNumber"] / df["maxInstanceNumber"]
        for n_bins in range(3, 10 + 1):
            bins = np.arange(0, n_bins)
            df["instanceNumberBinnedAt%d" % n_bins] = pd.cut(df["relativeInstanceNumber"],
                                                             bins=np.linspace(0.0, 1.0, n_bins + 1),
                                                             labels=bins)
        return df


class TransformRSNA2019Stage2DataFrames(TransformRSNA2019Stage1DataFrames):
    def requires(self):
        return CheckStage2Dataset()

    def output(self):
        return luigi.LocalTarget(os.path.join(DATASET_DIR, "stage_2_train.csv")), \
               luigi.LocalTarget(os.path.join(DATASET_DIR, "stage_2_test.csv"))


class PrepareRSNA2019Stage1DataFrames(BasePrepareDataFrames):
    def requires(self):
        return TransformRSNA2019Stage1DataFrames()

    @property
    def dataset_dir(self) -> str:
        return DATASET_DIR

    @property
    def stratification_property(self) -> str:
        return "any"

    def read_data_frame(self) -> pd.DataFrame:
        df = pd.read_csv(self.input()[0].path)
        df = df[df["imageId"] != "ID_6431af929"]
        return df

    def _study_split(self, df: pd.DataFrame,
                     split_function: Callable[[pd.DataFrame], Tuple[pd.DataFrame,
                                                                    pd.DataFrame]]) -> Tuple[pd.DataFrame,
                                                                                             pd.DataFrame]:
        study_df = df.groupby(["study"])["any"].sum().reset_index()
        study_df["any"] = study_df["any"].apply(lambda e: 1 if e > 0 else 0)

        train_study_df, test_study_df = split_function(study_df)

        train_df = df[df.study.isin(train_study_df.study)]
        test_df = df[df.study.isin(test_study_df.study)]
        return train_df, test_df

    def train_test_split(self, df: pd.DataFrame, test_size: float) -> Tuple[pd.DataFrame, pd.DataFrame]:
        train_test_split = super().train_test_split
        return self._study_split(df, lambda df: train_test_split(df, test_size))

    def kfold_split(self, df) -> Tuple[pd.DataFrame, pd.DataFrame]:
        return self._study_split(df, super().kfold_split)


class PrepareRSNA2019Stage2DataFrames(PrepareRSNA2019Stage1DataFrames):
    def requires(self):
        return TransformRSNA2019Stage2DataFrames()


class ResizeRSNA2019Stage1Images(BaseResizeImages):
    window_mode: str = luigi.ChoiceParameter(choices=[
        "from_dicom", "three_windows", "three_window_grayscale", "single_window", "raw",
    ], default="from_dicom")
    window_widths: List[int] = luigi.ListParameter(default=[200, 200, 200])
    window_centers: List[int] = luigi.ListParameter(default=[50, 200, 350])

    def requires(self):
        return ConvertDicomImagesFromRSNA2019Stage1Dataset(window_mode=self.window_mode,
                                                           window_widths=self.window_widths,
                                                           window_centers=self.window_centers)

    @property
    def dataset_dir(self) -> str:
        return DATASET_DIR

    @property
    def image_dir(self) -> str:
        return self.input().path


class ResizeRSNA2019Stage2Images(ResizeRSNA2019Stage1Images):
    def requires(self):
        return ConvertDicomImagesFromRSNA2019Stage2Dataset(window_mode=self.window_mode,
                                                           window_widths=self.window_widths,
                                                           window_centers=self.window_centers)
