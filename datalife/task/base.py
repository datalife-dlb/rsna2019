import abc
import inspect
import json
import logging
import multiprocessing
import os
import shutil
import warnings
from contextlib import redirect_stdout
from typing import Type, Tuple, List, Callable, Dict, Union

import luigi
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow.keras.backend as K
import torch
import torch.nn as nn
import torch.nn.functional as F
from adabound import AdaBound as TorchAdaBound
from classification_models.tfkeras import Classifiers
from efficientnet.tfkeras import EfficientNetB7, EfficientNetB6, EfficientNetB5, EfficientNetB4, \
    EfficientNetB3, EfficientNetB2, EfficientNetB1, EfficientNetB0
from imgaug import augmenters as iaa
from segmentation_models.losses import jaccard_loss as keras_jaccard_loss, bce_jaccard_loss as keras_bce_jaccard_loss, \
    cce_jaccard_loss as keras_cce_jaccard_loss, dice_loss as keras_dice_loss, bce_dice_loss as keras_bce_dice_loss, \
    cce_dice_loss as keras_cce_dice_loss
from segmentation_models.metrics import iou_score as keras_iou_score
from tensorflow.keras import Model as KerasModel
from tensorflow.keras.activations import relu as keras_relu, selu as keras_selu, tanh as keras_tanh, \
    sigmoid as keras_sigmoid, \
    linear as keras_linear
from tensorflow.keras.callbacks import ModelCheckpoint as KerasModelCheckpoint, \
    EarlyStopping as KerasEarlyStopping, \
    CSVLogger as KerasCSVLogger, TensorBoard as KerasTensorBoard, Callback as KerasCallback, \
    ReduceLROnPlateau as KerasReduceLROnPlateau
from tensorflow.keras.initializers import lecun_normal as keras_lecun_normal, he_normal as keras_he_normal, \
    glorot_normal as keras_glorot_normal, lecun_uniform as keras_lecun_uniform, he_uniform as keras_he_uniform, \
    glorot_uniform as keras_glorot_uniform
from tensorflow.keras.layers import Input as KerasInput, SpatialDropout2D as KerasSpatialDropout2D, \
    SpatialDropout3D as KerasSpatialDropout3D, Flatten as KerasFlatten, \
    GlobalAveragePooling2D as KerasGlobalAveragePooling2D, \
    GlobalMaxPooling2D as KerasGlobalMaxPooling2D, GlobalAveragePooling3D as KerasGlobalAveragePooling3D, \
    GlobalMaxPooling3D as KerasGlobalMaxPooling3D
import tensorflow.keras.layers as keras_layers
from tensorflow.keras.losses import binary_crossentropy as keras_binary_crossentropy, \
    categorical_crossentropy as keras_categorical_crossentropy, hinge as keras_hinge, \
    squared_hinge as keras_squared_hinge, categorical_hinge as keras_categorical_hinge, \
    mean_absolute_error as keras_mean_absolute_error, mean_squared_error as keras_mean_squared_error
from tensorflow.keras.metrics import Precision as KerasPrecision, Recall as KerasRecall
from tensorflow.keras.optimizers import Adam as KerasAdam, RMSprop as KerasRMSprop, SGD as KerasSGD, \
    Adadelta as KerasAdadelta, \
    Adagrad as KerasAdagrad, Adamax as KerasAdamax
from tensorflow.keras.optimizers import Optimizer as KerasOptimizer
from tensorflow.keras.regularizers import l1 as keras_l1, l2 as keras_l2
from tensorflow.python import connect_to_remote_host
from torch.optim import Adam as TorchAdam, RMSprop as TorchRMSprop, SGD as TorchSGD
from torch.optim.adadelta import Adadelta as TorchAdadelta
from torch.optim.adagrad import Adagrad as TorchAdagrad
from torch.optim.adamax import Adamax as TorchAdamax
from torch.optim.optimizer import Optimizer as TorchOptimizer
from torch.utils.data.dataloader import DataLoader
from torchbearer import Trial
from torchbearer.callbacks.callbacks import Callback as TorchbearerCallback
from torchbearer.callbacks.checkpointers import ModelCheckpoint as TorchbearerModelCheckpoint
from torchbearer.callbacks.csv_logger import CSVLogger as TorchbearerCSVLogger
from torchbearer.callbacks.early_stopping import EarlyStopping as TorchbearerEarlyStopping
from torchbearer.callbacks.tensor_board import TensorBoard as TorchbearerTensorBoard
from torchbearer.callbacks.torch_scheduler import CosineAnnealingLR as TorchbearerCosineAnnealingLR, \
    ExponentialLR as TorchbearerExponentialLR, ReduceLROnPlateau as TorchbearerReduceLROnPlateau, \
    StepLR as TorchbearerStepLR

from datalife.data import ImageDataset, IMAGE_PREPROCESSING_FUNCTIONS, calculate_steps_per_epoch, \
    TfAugmenter
from datalife.files import get_params_path, get_torch_weights_path, get_params, get_history_path, \
    get_tensorboard_logdir, get_classes_path, get_task_dir, get_keras_weights_path, get_history_plot_path, \
    get_keras_swa_weights_path
from datalife.keras.activations import swish as keras_swish
from datalife.keras.applications.inception_v4 import InceptionV4
from datalife.keras.applications.vgg16 import VGG16, VGG16_BN
from datalife.keras.applications.vgg16_3d import VGG16_3D, VGG16_3D_BN
from datalife.keras.applications.vgg19 import VGG19, VGG19_BN
from datalife.keras.applications.vgg19_3d import VGG19_3D, VGG19_3D_BN
from datalife.keras.callbacks import StepLR as KerasStepLR, ExponentialLR as KerasExponentialLR, \
    CosineAnnealingLR as KerasCosineAnnealingLR, CosineAnnealingWithRestartsLR as KerasCosineAnnealingWithRestartsLR, \
    LearningRateFinder as KerasLearningRateFinder, WarmUpLearningRateScheduler, PlotTraining as KerasPlotTraining, \
    StochasticWeightAveraging as KerasSWA, FineTuningCallback as KerasFineTuningCallback
from datalife.keras.eval import pred_probas_for_classifier, evaluate_and_report_classifier, plot_roc_curves
from datalife.keras.losses import focal_loss as keras_focal_loss, \
    binary_focal_loss as keras_binary_focal_loss, categorical_focal_loss as keras_categorical_focal_loss, \
    weighted_categorical_crossentropy as keras_weighted_categorical_crossentropy, label_smoothing_loss_function, \
    sigmoid_focal_crossentropy as keras_sigmoid_focal_crossentropy
from datalife.keras.metrics import FBetaScore as KerasFBetaScore
from datalife.keras.optimizers import RectifiedAdam as KerasRectifiedAdam, Lookahead as KerasLookahead, \
    extend_with_decoupled_weight_decay as keras_extend_with_decoupled_weight_decay
from datalife.keras.utils import insert_layer_nonseq
from datalife.plot import plot_history, plot_loss_per_lr, plot_loss_derivatives_per_lr
from datalife.task.config import PROJECTS, InputType
from datalife.task.cuda import CudaRepository
from datalife.task.data_preparation.base import PIL_INTERPOLATION_METHODS
from datalife.task.meta_config import Task, TaskType
from datalife.torch.callbacks import TorchbearerCosineAnnealingWithRestartsLR, TorchbearerCyclicLR, \
    TorchbearerLearningRateFinder
from datalife.torch.initializer import lecun_normal as torch_lecun_normal, he_normal as torch_he_normal, \
    lecun_uniform as torch_lecun_uniform, he_uniform as torch_he_uniform
from datalife.torch.losses import FocalLoss as TorchFocalLoss
from datalife.torch.summary import summarize_torch_model
from datalife.utils import no_params_wrapper

tf.enable_v2_behavior()

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

TORCH_OPTIMIZERS = dict(adam=TorchAdam, rmsprop=TorchRMSprop, sgd=TorchSGD, adadelta=TorchAdadelta,
                        adagrad=TorchAdagrad, adamax=TorchAdamax, adabound=TorchAdaBound)
TORCH_LOSS_FUNCTIONS = dict(binary_crossentropy=nn.BCELoss, focal=TorchFocalLoss)
TORCH_ACTIVATION_FUNCTIONS = dict(relu=F.relu, selu=F.selu, tanh=F.tanh, sigmoid=F.sigmoid, linear=F.linear)
TORCH_WEIGHT_INIT = dict(lecun_normal=torch_lecun_normal, he_normal=torch_he_normal,
                         glorot_normal=nn.init.xavier_normal, lecun_uniform=torch_lecun_uniform,
                         he_uniform=torch_he_uniform, glorot_uniform=nn.init.xavier_uniform)
TORCH_DROPOUT_MODULES = dict(dropout=nn.Dropout, alpha=nn.AlphaDropout)
TORCH_LR_SCHEDULERS = dict(step=TorchbearerStepLR, exponential=TorchbearerExponentialLR,
                           cosine_annealing=TorchbearerCosineAnnealingLR,
                           cosine_annealing_with_restarts=TorchbearerCosineAnnealingWithRestartsLR,
                           cyclic=TorchbearerCyclicLR, reduce_on_plateau=TorchbearerReduceLROnPlateau,
                           none=None)

KERAS_OPTIMIZERS = dict(adam=KerasAdam, rmsprop=KerasRMSprop, sgd=KerasSGD, adadelta=KerasAdadelta,
                        adagrad=KerasAdagrad, adamax=KerasAdamax, radam=KerasRectifiedAdam)
KERAS_LOSS_FUNCTIONS = dict(
    binary_crossentropy=no_params_wrapper(keras_binary_crossentropy),
    categorical_crossentropy=no_params_wrapper(keras_categorical_crossentropy),
    weighted_categorical_crossentropy=keras_weighted_categorical_crossentropy,
    hinge=no_params_wrapper(keras_hinge),
    squared_hinge=no_params_wrapper(keras_squared_hinge),
    categorical_hinge=no_params_wrapper(keras_categorical_hinge),
    mean_absolute_error=no_params_wrapper(keras_mean_absolute_error),
    mean_squared_error=no_params_wrapper(keras_mean_squared_error),
    jaccard=no_params_wrapper(keras_jaccard_loss), bce_jaccard=no_params_wrapper(keras_bce_jaccard_loss),
    cce_jaccard=no_params_wrapper(keras_cce_jaccard_loss),
    dice=no_params_wrapper(keras_dice_loss), bce_dice=no_params_wrapper(keras_bce_dice_loss),
    cce_dice=no_params_wrapper(keras_cce_dice_loss), focal=keras_focal_loss,
    binary_focal=keras_binary_focal_loss, categorical_focal=keras_categorical_focal_loss,
    sigmoid_focal_crossentropy=keras_sigmoid_focal_crossentropy,
)
KERAS_ACTIVATION_FUNCTIONS = dict(relu=keras_relu, selu=keras_selu, tanh=keras_tanh, sigmoid=keras_sigmoid,
                                  linear=keras_linear, swish=keras_swish)
KERAS_WEIGHT_INIT = dict(lecun_normal=keras_lecun_normal, he_normal=keras_he_normal, glorot_normal=keras_glorot_normal,
                         lecun_uniform=keras_lecun_uniform,
                         he_uniform=keras_he_uniform, glorot_uniform=keras_glorot_uniform)
KERAS_LR_SCHEDULERS = dict(step=KerasStepLR, exponential=KerasExponentialLR, cosine_annealing=KerasCosineAnnealingLR,
                           cosine_annealing_with_restarts=KerasCosineAnnealingWithRestartsLR,
                           reduce_on_plateau=KerasReduceLROnPlateau, none=None)
KERAS_REGULARIZERS = dict(l1=keras_l1, l2=keras_l2, none=None)

KERAS_BASE_MODELS: Dict[str, Callable[..., KerasModel]] = {model_name: Classifiers.get(model_name)[0]
                                                           for model_name in Classifiers.models_names()}
KERAS_BASE_MODELS["efficientnet_b7"] = EfficientNetB7
KERAS_BASE_MODELS["efficientnet_b6"] = EfficientNetB6
KERAS_BASE_MODELS["efficientnet_b5"] = EfficientNetB5
KERAS_BASE_MODELS["efficientnet_b4"] = EfficientNetB4
KERAS_BASE_MODELS["efficientnet_b3"] = EfficientNetB3
KERAS_BASE_MODELS["efficientnet_b2"] = EfficientNetB2
KERAS_BASE_MODELS["efficientnet_b1"] = EfficientNetB1
KERAS_BASE_MODELS["efficientnet_b0"] = EfficientNetB0
KERAS_BASE_MODELS["vgg16"] = VGG16
KERAS_BASE_MODELS["vgg16_bn"] = VGG16_BN
KERAS_BASE_MODELS["vgg19"] = VGG19
KERAS_BASE_MODELS["vgg19_bn"] = VGG19_BN
KERAS_BASE_MODELS["vgg16_3d"] = VGG16_3D
KERAS_BASE_MODELS["vgg16_3d_bn"] = VGG16_3D_BN
KERAS_BASE_MODELS["vgg19_3d"] = VGG19_3D
KERAS_BASE_MODELS["vgg19_3d_bn"] = VGG19_3D_BN
KERAS_BASE_MODELS["inception_v4"] = InceptionV4
KERAS_BASE_MODELS["custom"] = None

SEED = 42

DEFAULT_DEVICE = "cuda" if torch.cuda.is_available() else "cpu"


class TaskWithAugmentation(luigi.Task):
    data_augmented: bool = luigi.BoolParameter(default=False)
    augmentations_done_range: Tuple[int, int] = luigi.TupleParameter(default=(0, 13))
    augmentation_multiply_range: Tuple[float, float] = luigi.TupleParameter(default=(1.0, 1.0))  # Ex: (0.75, 1.25)
    augmentation_multiply_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_add_range: Tuple[float, float] = luigi.TupleParameter(default=(0, 0))  # Ex: (-20, 20)
    augmentation_add_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_contrast_range: Tuple[int, int] = luigi.TupleParameter(default=(1.0, 1.0))  # Ex: (0.6, 1.4)
    augmentation_contrast_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_gaussian_blur_range: Tuple[int, int] = luigi.TupleParameter(default=(0.0, 0.0))  # Ex: (0.0, 1.0)
    augmentation_gaussian_blur_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_gaussian_noise_range: Tuple[float, float] = luigi.TupleParameter(default=(0.0, 0.0))  # Ex: (0.0, 0.01)
    augmentation_gaussian_noise_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_flip_x_chance: float = luigi.FloatParameter(default=0.0)  # Ex: 0.5
    augmentation_flip_x_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_flip_y_chance: float = luigi.FloatParameter(default=0.0)  # Ex: 0.5
    augmentation_flip_y_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_scale_range: Tuple[float, float] = luigi.TupleParameter(default=(1.0, 1.0))  # Ex: (0.55, 1.0)
    augmentation_scale_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_translate_percent_range: Tuple[float, float] = luigi.TupleParameter(
        default=(0.0, 0.0))  # Ex: (-0.05, 0.05)
    augmentation_translate_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_rotate_degrees_range: Tuple[float, float] = luigi.TupleParameter(default=(0.0, 0.0))  # Ex: (-5.0, 5.0)
    augmentation_rotate_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_shear_degrees_range: Tuple[float, float] = luigi.TupleParameter(default=(0.0, 0.0))  # Ex: (-2.0, 2.0)
    augmentation_shear_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_dropout: float = luigi.FloatParameter(default=0.0)  # Ex: 0.5
    augmentation_dropout_always_run: bool = luigi.BoolParameter(default=False)
    augmentation_invert_chance: float = luigi.FloatParameter(default=0.0)  # Ex: 0.5
    augmentation_invert_always_run: bool = luigi.BoolParameter(default=False)

    @property
    def augmenter(self) -> iaa.Augmenter:
        always_augmentations = []
        some_times_augmentations = []

        if self.augmentation_multiply_range and (
                self.augmentation_multiply_range[0] != 1.0 or self.augmentation_multiply_range[1] != 1.0):
            multiply = iaa.Multiply(self.augmentation_multiply_range)
            if self.augmentation_multiply_always_run:
                always_augmentations.append(multiply)
            else:
                some_times_augmentations.append(multiply)
        if self.augmentation_add_range and (
                self.augmentation_add_range[0] != 0 or self.augmentation_add_range[1] != 0):
            add = iaa.Add(self.augmentation_add_range)
            if self.augmentation_add_always_run:
                always_augmentations.append(add)
            else:
                some_times_augmentations.append(add)
        if self.augmentation_contrast_range and (
                self.augmentation_contrast_range[0] != 1.0 or self.augmentation_contrast_range[1] != 1.0):
            contrast_normalization = iaa.ContrastNormalization(self.augmentation_contrast_range)
            if self.augmentation_contrast_always_run:
                always_augmentations.append(contrast_normalization)
            else:
                some_times_augmentations.append(contrast_normalization)
        if self.augmentation_gaussian_blur_range and (
                self.augmentation_gaussian_blur_range[0] != 0.0 or self.augmentation_gaussian_blur_range[1] != 0.0):
            gaussian_blur = iaa.GaussianBlur(self.augmentation_gaussian_blur_range)
            if self.augmentation_gaussian_blur_always_run:
                always_augmentations.append(gaussian_blur)
            else:
                some_times_augmentations.append(gaussian_blur)
        if self.augmentation_gaussian_noise_range and (
                self.augmentation_gaussian_noise_range[0] != 0.0 or self.augmentation_gaussian_noise_range[1] != 0.0):
            gaussian_noise = iaa.AdditiveGaussianNoise(scale=(
                self.augmentation_gaussian_noise_range[0] * 255, self.augmentation_gaussian_noise_range[1] * 255))
            if self.augmentation_gaussian_noise_always_run:
                always_augmentations.append(gaussian_noise)
            else:
                some_times_augmentations.append(gaussian_noise)
        if self.augmentation_flip_x_chance:
            flip_x = iaa.Fliplr(self.augmentation_flip_x_chance)
            if self.augmentation_flip_x_always_run:
                always_augmentations.append(flip_x)
            else:
                some_times_augmentations.append(flip_x)
        if self.augmentation_flip_y_chance:
            flip_y = iaa.Flipud(self.augmentation_flip_y_chance)
            if self.augmentation_flip_y_always_run:
                always_augmentations.append(flip_y)
            else:
                some_times_augmentations.append(flip_y)
        if self.augmentation_scale_range and (
                self.augmentation_scale_range[0] != 1.0 or self.augmentation_scale_range[1] != 1.0):
            scale = iaa.Affine(scale={"x": self.augmentation_scale_range, "y": self.augmentation_scale_range},
                               name="Scale")
            if self.augmentation_scale_always_run:
                always_augmentations.append(scale)
            else:
                some_times_augmentations.append(scale)
        if self.augmentation_translate_percent_range and (
                self.augmentation_translate_percent_range[0] != 0.0 or
                self.augmentation_translate_percent_range[1] != 0.0):
            translate = iaa.Affine(translate_percent={"x": self.augmentation_translate_percent_range,
                                                      "y": self.augmentation_translate_percent_range},
                                   name="Translate")
            if self.augmentation_translate_always_run:
                always_augmentations.append(translate)
            else:
                some_times_augmentations.append(translate)
        if self.augmentation_rotate_degrees_range and (
                self.augmentation_rotate_degrees_range[0] != 0.0 or self.augmentation_rotate_degrees_range[1] != 0.0):
            rotate = iaa.Affine(rotate=self.augmentation_rotate_degrees_range, name="Rotate")
            if self.augmentation_rotate_always_run:
                always_augmentations.append(rotate)
            else:
                some_times_augmentations.append(rotate)
        if self.augmentation_shear_degrees_range and (
                self.augmentation_shear_degrees_range[0] != 0.0 or self.augmentation_shear_degrees_range[1] != 0.0):
            shear = iaa.Affine(shear=self.augmentation_shear_degrees_range, name="Shear")
            if self.augmentation_shear_always_run:
                always_augmentations.append(shear)
            else:
                some_times_augmentations.append(shear)
        if self.augmentation_dropout:
            dropout = iaa.Dropout(self.augmentation_dropout)
            if self.augmentation_dropout_always_run:
                always_augmentations.append(dropout)
            else:
                some_times_augmentations.append(dropout)
        if self.augmentation_invert_chance:
            invert = iaa.Invert(self.augmentation_invert_chance)
            if self.augmentation_invert_always_run:
                always_augmentations.append(invert)
            else:
                some_times_augmentations.append(invert)

        return iaa.Sequential(
            always_augmentations +
            [
                iaa.SomeOf(
                    (self.augmentations_done_range[0],
                     min(self.augmentations_done_range[1], len(some_times_augmentations))),
                    some_times_augmentations, random_order=True, random_state=self.seed,
                )
            ], random_order=True, random_state=self.seed,
        )

    @property
    def tf_augmenter(self) -> Callable[[tf.Tensor, tf.Tensor], Tuple[tf.Tensor, tf.Tensor]]:
        def augment_image(image: tf.Tensor, target: tf.Tensor):
            augmenter = TfAugmenter(tensors={"image": image})

            if self.augmentation_multiply_range and (
                    self.augmentation_multiply_range[0] != 1.0 or self.augmentation_multiply_range[1] != 1.0):
                augmenter.multiply(self.augmentation_multiply_range, always=self.augmentation_multiply_always_run)
            if self.augmentation_add_range and (
                    self.augmentation_add_range[0] != 0 or self.augmentation_add_range[1] != 0):
                augmenter.add(self.augmentation_add_range, always=self.augmentation_add_always_run)
            if self.augmentation_contrast_range and (
                    self.augmentation_contrast_range[0] != 1.0 or self.augmentation_contrast_range[1] != 1.0):
                augmenter.contrast(self.augmentation_contrast_range, always=self.augmentation_contrast_always_run)
            if self.augmentation_gaussian_blur_range and (
                    self.augmentation_gaussian_blur_range[0] != 0.0 or self.augmentation_gaussian_blur_range[1] != 0.0):
                warnings.warn("Gaussian Blur not implemented for TfAugmenter")
            if self.augmentation_gaussian_noise_range and (
                    self.augmentation_gaussian_noise_range[0] != 0.0
                    or self.augmentation_gaussian_noise_range[1] != 0.0):
                warnings.warn("Gaussian Noise not implemented for TfAugmenter")
            if self.augmentation_flip_x_chance:
                augmenter.flip_x(self.augmentation_flip_x_chance)
            if self.augmentation_flip_y_chance:
                augmenter.flip_y(self.augmentation_flip_x_chance)
            if self.augmentation_scale_range and (
                    self.augmentation_scale_range[0] != 1.0 or self.augmentation_scale_range[1] != 1.0):
                augmenter.scale(self.augmentation_scale_range, always=self.augmentation_scale_always_run)
            if self.augmentation_translate_percent_range and (
                    self.augmentation_translate_percent_range[0] != 0.0 or
                    self.augmentation_translate_percent_range[1] != 0.0):
                augmenter.translate(self.augmentation_translate_percent_range,
                                    always=self.augmentation_translate_always_run)
            if self.augmentation_rotate_degrees_range and (
                    self.augmentation_rotate_degrees_range[0] != 0.0
                    or self.augmentation_rotate_degrees_range[1] != 0.0):
                augmenter.rotate(self.augmentation_rotate_degrees_range, always=self.augmentation_rotate_always_run)
            if self.augmentation_shear_degrees_range and (
                    self.augmentation_shear_degrees_range[0] != 0.0 or self.augmentation_shear_degrees_range[1] != 0.0):
                augmenter.shear(self.augmentation_shear_degrees_range, always=self.augmentation_shear_always_run)
            if self.augmentation_dropout:
                warnings.warn("Gaussian Noise not implemented for TfAugmenter")
            if self.augmentation_invert_chance:
                augmenter.invert(self.augmentation_invert_chance)

            return augmenter.transform_tensors(self.augmentations_done_range)["image"], target

        return augment_image


class BaseModelTraining(TaskWithAugmentation, metaclass=abc.ABCMeta):
    project: str = luigi.ChoiceParameter(choices=PROJECTS.keys())
    target_cols: List[str] = luigi.ListParameter(default=[])
    device: str = luigi.ChoiceParameter(choices=["cpu", "cuda"], default=DEFAULT_DEVICE)
    multi_gpu: bool = luigi.BoolParameter(default=False)
    tpu_address: str = luigi.Parameter(default="")
    tpu_bucket: str = luigi.Parameter(default="")
    training_type: str = luigi.ChoiceParameter(choices=["fit", "cross_validation", "lr_find", "debug"], default="fit")
    lr_find_iterations: int = luigi.IntParameter(default=100)

    test_size: float = luigi.FloatParameter(default=0.2)
    dataset_split_method: str = luigi.ChoiceParameter(choices=["holdout", "k_fold"], default="holdout")
    val_size: float = luigi.FloatParameter(default=0.2)
    n_splits: int = luigi.IntParameter(default=5)
    split_index: int = luigi.IntParameter(default=0)
    data_frames_preparation_extra_params: dict = luigi.DictParameter(default={})
    label_smoothing: float = luigi.FloatParameter(default=0.0)

    dtype: str = luigi.ChoiceParameter(choices=["float32", "float16"], default="float32")
    input_shape: Tuple[int, int] = luigi.TupleParameter(default=(224, 224))
    class_weight: Dict[int, float] = luigi.DictParameter(default=None)
    image_3d_number_of_slices: int = luigi.IntParameter(default=0)
    sampling_strategy: str = luigi.ChoiceParameter(choices=["oversample", "undersample", "none"], default="oversample")
    balance_fields: List[str] = luigi.ListParameter(default=[])
    sampling_proportions: Dict[str, Dict[str, float]] = luigi.DictParameter(default={})
    use_sampling_in_validation: bool = luigi.BoolParameter(default=False)
    eq_filters: Dict[str, any] = luigi.DictParameter(default={})
    neq_filters: Dict[str, any] = luigi.DictParameter(default={})
    isin_filters: Dict[str, any] = luigi.DictParameter(default={})
    use_augmentation_in_validation: bool = luigi.BoolParameter(default=False)
    interpolation: str = luigi.ChoiceParameter(choices=PIL_INTERPOLATION_METHODS.keys(),
                                               default="lanczos")
    resize_images_task_extra_params: dict = luigi.DictParameter(default={})
    image_preprocessing: str = luigi.ChoiceParameter(choices=IMAGE_PREPROCESSING_FUNCTIONS.keys(), default="rescale")
    image_preprocessing_extra_params: dict = luigi.DictParameter(default={})
    image_color_mode: str = luigi.ChoiceParameter(choices=["rgb", "grayscale"], default="rgb")
    image_data_format: str = luigi.ChoiceParameter(choices=["channels_first", "channels_last"])
    use_multiprocessing: bool = luigi.BoolParameter(default=False)
    learning_rate: float = luigi.FloatParameter(1e-3)
    batch_size: int = luigi.IntParameter(default=50)
    val_batch_size: int = luigi.IntParameter(default=50)
    epochs: int = luigi.IntParameter(default=100)
    monitor_metric: str = luigi.Parameter(default="val_loss")
    monitor_mode: str = luigi.ChoiceParameter(choices=["min", "max", "auto"], default="min")
    early_stopping_patience: int = luigi.IntParameter(default=10)
    early_stopping_min_delta: float = luigi.FloatParameter(default=1e-6)
    generator_workers: int = luigi.IntParameter(default=min(multiprocessing.cpu_count(), 60))
    generator_max_queue_size: int = luigi.IntParameter(default=50)
    cache_images: bool = luigi.BoolParameter(default=False)
    initial_weights_path = luigi.Parameter(default="")
    thresholds_to_eval: List[float] = luigi.ListParameter(default=np.arange(0.1, 1.0, 0.1).tolist())
    plot_training: bool = luigi.BoolParameter(default=False)
    swa_last_epochs: int = luigi.IntParameter(default=0)

    seed: int = luigi.IntParameter(default=SEED)

    def requires(self):
        if self.training_type == "cross_validation":
            return [self.__class__(**{**self.param_kwargs, "dataset_split_method": "k_fold", "training_type": "fit",
                                      "split_index": i})
                    for i in range(self.n_splits)]
        else:
            return (self.project_config.prepare_data_frames_task(target_shape=self.input_shape,
                                                                 test_size=self.test_size,
                                                                 dataset_split_method=self.dataset_split_method,
                                                                 val_size=self.val_size,
                                                                 n_splits=self.n_splits,
                                                                 split_index=self.split_index,
                                                                 sampling_strategy=self.sampling_strategy,
                                                                 sampling_proportions=self.sampling_proportions,
                                                                 balance_fields=self.balance_fields or self.project_config.default_balance_fields,
                                                                 use_sampling_in_validation=self.use_sampling_in_validation,
                                                                 eq_filters=self.eq_filters,
                                                                 neq_filters=self.neq_filters,
                                                                 isin_filters=self.isin_filters,
                                                                 seed=self.seed,
                                                                 **{
                                                                     **self.project_config.data_frames_preparation_extra_params,
                                                                     **self.data_frames_preparation_extra_params}
                                                                 ),
                    self.project_config.resize_images_task(target_shape=self.input_shape,
                                                           interpolation=self.interpolation,
                                                           **self.resize_images_task_extra_params))

    def input(self) -> Tuple[Tuple[luigi.LocalTarget, luigi.LocalTarget, luigi.LocalTarget], luigi.LocalTarget]:
        return super().input()

    def output(self):
        return luigi.LocalTarget(get_task_dir(self.project_config.base_dir, self.__class__, self.task_id))

    @property
    def project_config(self):
        return PROJECTS[self.project]

    @property
    def target_columns(self) -> List[str]:
        return list(self.target_cols) if self.target_cols else self.project_config.default_target_cols

    @property
    def tasks(self) -> List[Task]:
        if not hasattr(self, "_tasks"):
            self._tasks = [task for target_column in self.target_columns for task in self.project_config.tasks
                           if task.target_column == target_column]
            assert len(self._tasks) == len(self.target_columns)
            if len(self._tasks) != len(set(task.output_layer_name for task in self._tasks)):
                for i, task in enumerate(self._tasks):
                    task.output_layer_name = "%s_%d" % (task.output_layer_name, i)
        return self._tasks

    @property
    def number_of_slices(self) -> int:
        return self.image_3d_number_of_slices or self.project_config.default_number_of_slices

    @property
    def resources(self):
        # TODO: Verificar como resolver o problema de multi-gpu
        # if self.multi_gpu:
        #     return {"cuda": torch.cuda.device_count()} if self.device == "cuda" else {}
        return {"cuda": 1} if self.device == "cuda" else {}

    @property
    def device_id(self):
        if not hasattr(self, "_device_id"):
            if self.device == "cuda":
                self._device_id = CudaRepository.get_avaliable_device()
            else:
                self._device_id = None
        return self._device_id

    def _save_params(self):
        params = self._get_params()
        with open(get_params_path(self.output_path), "w") as params_file:
            json.dump(params, params_file, default=lambda o: dict(o), indent=4)

    def _get_params(self) -> dict:
        return self.param_kwargs

    @property
    def output_path(self) -> str:
        if hasattr(self, "_output_path"):
            return self._output_path
        return self.output().path

    @property
    def train_data_frame_path(self) -> str:
        return self.input()[0][0].path

    @property
    def val_data_frame_path(self) -> str:
        return self.input()[0][1].path

    @property
    def test_data_frame_path(self) -> str:
        return self.input()[0][2].path

    @property
    def images_path(self) -> str:
        path = self.input()[1].path
        if self.tpu_bucket:
            return os.path.join(self.tpu_bucket, path)
        return path

    @property
    def train_dataset(self) -> ImageDataset:
        if not hasattr(self, "_train_dataset"):
            train_df = pd.read_csv(self.train_data_frame_path)
            self._train_dataset = self.project_config.dataset_class(self.project_config, self.tasks, train_df,
                                                                    self.images_path,
                                                                    self.input_shape,
                                                                    self.interpolation,
                                                                    image_preprocessing=self.image_preprocessing,
                                                                    image_preprocessing_extra_params=self.image_preprocessing_extra_params,
                                                                    image_color_mode=self.image_color_mode,
                                                                    image_data_format=self.image_data_format,
                                                                    number_of_slices=self.number_of_slices,
                                                                    augmenter=self.augmenter if self.data_augmented else None,
                                                                    cache_images=self.cache_images,
                                                                    dtype=self.dtype, workers=self.generator_workers)
        return self._train_dataset

    @property
    def val_dataset(self) -> ImageDataset:
        if not hasattr(self, "_val_dataset"):
            val_df = pd.read_csv(self.val_data_frame_path)
            self._val_dataset = self.project_config.dataset_class(self.project_config, self.tasks, val_df,
                                                                  self.images_path,
                                                                  self.input_shape,
                                                                  self.interpolation,
                                                                  image_preprocessing=self.image_preprocessing,
                                                                  image_preprocessing_extra_params=self.image_preprocessing_extra_params,
                                                                  image_color_mode=self.image_color_mode,
                                                                  image_data_format=self.image_data_format,
                                                                  number_of_slices=self.number_of_slices,
                                                                  augmenter=self.augmenter if self.use_augmentation_in_validation else None,
                                                                  cache_images=self.cache_images,
                                                                  dtype=self.dtype, workers=self.generator_workers)
        return self._val_dataset

    def get_test_df(self) -> pd.DataFrame:
        return pd.read_csv(self.test_data_frame_path)

    @property
    def test_dataset(self) -> ImageDataset:
        if not hasattr(self, "_test_dataset"):
            self._test_dataset = self.project_config.dataset_class(self.project_config, self.tasks,
                                                                   self.get_test_df(),
                                                                   self.images_path,
                                                                   self.input_shape, self.interpolation,
                                                                   image_preprocessing=self.image_preprocessing,
                                                                   image_preprocessing_extra_params=self.image_preprocessing_extra_params,
                                                                   number_of_slices=self.number_of_slices,
                                                                   image_color_mode=self.image_color_mode,
                                                                   image_data_format=self.image_data_format,
                                                                   dtype=self.dtype, workers=self.generator_workers)
        return self._test_dataset

    @property
    def train_steps_per_epoch(self) -> int:
        return calculate_steps_per_epoch(self.train_dataset, self.batch_size)

    @property
    def val_steps_per_epoch(self) -> int:
        return calculate_steps_per_epoch(self.val_dataset, self.val_batch_size)

    @property
    def test_steps_per_epoch(self) -> int:
        return calculate_steps_per_epoch(self.test_dataset, self.val_batch_size)

    def train(self):
        dict(fit=self.fit, cross_validation=self.cross_validate,
             lr_find=self.lr_find, debug=self.debug)[self.training_type]()

    @abc.abstractmethod
    def fit(self):
        pass

    def cross_validate(self):
        for input in self.input():
            path = input.path
            os.symlink(os.path.abspath(path), os.path.join(self.output_path, os.path.split(path)[1]))

    @abc.abstractmethod
    def lr_find(self):
        pass

    def debug(self):
        print("Debug not implemented!")

    def after_fit(self, model: KerasModel = None):
        if model is None:
            model = self.get_trained_model()
        if any(task.type == TaskType.SOFTMAX_CLASSIFICATION or
               task.type == TaskType.MULTILABEL_CLASSIFICATION for task in self.tasks):
            print("Running best model on train dataset...")
            model_train_probas = pred_probas_for_classifier(model, self.get_train_generator(shuffle=False),
                                                            self.train_steps_per_epoch)[:len(self.train_dataset)]
            print("Running best model on val dataset...")
            model_val_probas = pred_probas_for_classifier(model, self.val_generator,
                                                          self.val_steps_per_epoch)[:len(self.val_dataset)]

            for i, task in enumerate(self.tasks):
                if task.type == TaskType.SOFTMAX_CLASSIFICATION or task.type == TaskType.MULTILABEL_CLASSIFICATION:
                    train_probas = model_train_probas if len(self.tasks) == 1 else model_train_probas[i]
                    val_probas = model_val_probas if len(self.tasks) == 1 else model_val_probas[i]
                    train_targets = self.train_dataset.targets[task.output_layer_name]
                    val_targets = self.val_dataset.targets[task.output_layer_name]
                    output_path = os.path.join(self.output().path, task.output_layer_name)
                    os.makedirs(output_path, exist_ok=True)
                    if task.type == TaskType.SOFTMAX_CLASSIFICATION:
                        evaluate_and_report_classifier(train_probas, train_targets, self.train_dataset.im_list,
                                                       val_probas, val_targets, self.val_dataset.im_list,
                                                       self.thresholds_to_eval if task.has_negative_target else [None],
                                                       task.labels, output_path)
                    plot_roc_curves(train_probas, train_targets, val_probas, val_targets, task.labels, output_path)

    def before_train(self):
        pass

    def after_train(self):
        pass

    def run(self):
        np.random.seed(self.seed)
        os.makedirs(self.output_path, exist_ok=True)
        self._save_params()
        try:
            self.before_train()
            self.train()
            if self.training_type == "fit":
                self.after_fit()
        except Exception:
            shutil.rmtree(self.output_path)
            raise
        finally:
            try:
                self.after_train()
            finally:
                if self.device == "cuda":
                    CudaRepository.put_available_device(self.device_id)


class BaseTorchModelTraining(BaseModelTraining, metaclass=abc.ABCMeta):
    image_data_format: str = luigi.ChoiceParameter(choices=["channels_first", "channels_last"],
                                                   default="channels_first")
    optimizer: str = luigi.ChoiceParameter(choices=TORCH_OPTIMIZERS.keys(), default="adam")
    optimizer_params: dict = luigi.DictParameter(default={})
    lr_scheduler: str = luigi.ChoiceParameter(choices=TORCH_LR_SCHEDULERS.keys(), default="none")
    lr_scheduler_params: dict = luigi.DictParameter(default={})
    loss_function: str = luigi.ChoiceParameter(choices=TORCH_LOSS_FUNCTIONS.keys(), default="categorical_crossentropy")
    loss_function_params: dict = luigi.DictParameter(default={})

    @abc.abstractmethod
    def create_module(self) -> nn.Module:
        pass

    def _set_torch_device(self):
        torch.manual_seed(self.seed)
        if self.device == "cuda":
            torch.cuda.set_device(self.device_id)

    def fit(self):
        self._set_torch_device()

        classes_path = get_classes_path(self.output_path)
        with open(classes_path, "w") as classes_file:
            json.dump(self.classes.tolist(), classes_file)

        train_loader = self.get_train_generator()
        val_loader = self.get_val_generator()

        module = self.create_module()

        summart_path = os.path.join(self.output_path, "summary.txt")
        with open(summart_path, "w") as summary_file:
            with redirect_stdout(summary_file):
                summarize_torch_model(module, torch.zeros((1, self.input_shape[0], self.input_shape[1], 3)))

        trial = self.create_trial(module)

        try:
            trial.with_generators(train_generator=train_loader, val_generator=val_loader).run(epochs=self.epochs)
        except KeyboardInterrupt:
            print("Finishing the training at the request of the user...")

        history_df = pd.read_csv(get_history_path(self.output_path))

        history_fig = plot_history(history_df)
        history_fig.savefig(get_history_plot_path(self.output_path))
        plt.close(history_fig)

    def lr_find(self):
        self._set_torch_device()

        train_loader = self.get_train_generator()

        self.learning_rate = 1e-6
        lr_finder = TorchbearerLearningRateFinder(min(len(train_loader), self.lr_find_iterations), self.learning_rate)

        module = self.create_module()

        trial = Trial(module, self._get_optimizer(module), self._get_loss_function(), callbacks=[lr_finder]) \
            .with_train_generator(train_loader) \
            .to(self.torch_device)

        trial.run(epochs=1)

        loss_per_lr_path = os.path.join(self.output_path, "loss_per_lr.jpg")
        loss_derivatives_per_lr_path = os.path.join(self.output_path, "loss_derivatives_per_lr.jpg")

        loss_per_lr_fig = plot_loss_per_lr(lr_finder.learning_rates, lr_finder.loss_values)
        loss_per_lr_fig.savefig(loss_per_lr_path)
        loss_derivatives_per_lr_fig = plot_loss_derivatives_per_lr(lr_finder.learning_rates,
                                                                   lr_finder.get_loss_derivatives(5))
        loss_derivatives_per_lr_fig.savefig(loss_derivatives_per_lr_path)
        plt.close(loss_per_lr_fig)
        plt.close(loss_derivatives_per_lr_fig)

    def create_trial(self, module: nn.Module) -> Trial:
        metrics = ["loss", "acc", "precision", "recall"]
        return Trial(module, self._get_optimizer(module), self._get_loss_function(), callbacks=self._get_callbacks(),
                     metrics=metrics).to(self.torch_device)

    def _get_loss_function(self) -> Callable:
        return TORCH_LOSS_FUNCTIONS[self.loss_function](**self.loss_function_params)

    def _get_optimizer(self, module) -> TorchOptimizer:
        return TORCH_OPTIMIZERS[self.optimizer](module.parameters(), lr=self.learning_rate,
                                                **self.optimizer_params)

    def _get_callbacks(self) -> List[TorchbearerCallback]:
        callbacks = [*self._get_extra_callbacks(),
                     TorchbearerModelCheckpoint(get_torch_weights_path(self.output_path),
                                                save_best_only=True,
                                                monitor=self.monitor_metric,
                                                mode=self.monitor_mode),
                     TorchbearerEarlyStopping(patience=self.early_stopping_patience,
                                              min_delta=self.early_stopping_min_delta,
                                              monitor=self.monitor_metric,
                                              mode=self.monitor_mode,
                                              verbose=True),
                     TorchbearerCSVLogger(get_history_path(self.output_path)),
                     TorchbearerTensorBoard(get_tensorboard_logdir(self.task_id))]
        if self.lr_scheduler != "none":
            callbacks.append(TORCH_LR_SCHEDULERS[self.lr_scheduler](**self.lr_scheduler_params))
        return callbacks

    def _get_extra_callbacks(self) -> List[TorchbearerCallback]:
        return []

    def get_train_generator(self) -> DataLoader:
        return DataLoader(self.train_dataset, batch_size=self.batch_size, shuffle=True,
                          num_workers=self.generator_workers, pin_memory=True)

    def get_val_generator(self) -> DataLoader:
        return DataLoader(self.val_dataset, batch_size=self.val_batch_size, shuffle=False,
                          num_workers=self.generator_workers, pin_memory=True)

    def get_test_generator(self) -> DataLoader:
        return DataLoader(self.test_dataset, batch_size=self.val_batch_size, shuffle=False,
                          num_workers=self.generator_workers, pin_memory=True)

    def get_trained_module(self) -> nn.Module:
        module = self.create_module().to(self.torch_device)
        state_dict = torch.load(get_torch_weights_path(self.output_path), map_location=self.torch_device)
        module.load_state_dict(state_dict["model"])
        module.eval()
        return module

    @property
    def torch_device(self) -> torch.device:
        if not hasattr(self, "_torch_device"):
            if self.device == "cuda":
                self._torch_device = torch.device(f"cuda:{self.device_id}")
            else:
                self._torch_device = torch.device("cpu")
        return self._torch_device


class BaseKerasModelTraining(BaseModelTraining, metaclass=abc.ABCMeta):
    image_data_format: str = luigi.ChoiceParameter(choices=["channels_first", "channels_last"],
                                                   default="channels_last")
    optimizer: str = luigi.ChoiceParameter(choices=KERAS_OPTIMIZERS.keys(), default="adam")
    optimizer_params: dict = luigi.DictParameter(default={})
    optimizer_lookahead: bool = luigi.BoolParameter(default=False)
    optimizer_lookahead_params: dict = luigi.DictParameter(default={})
    weight_decay: float = luigi.FloatParameter(default=0.0)
    warmup_batches: int = luigi.IntParameter(default=None)
    warmup_epochs: int = luigi.IntParameter(default=None)
    warmup_init_lr: float = luigi.FloatParameter(default=0.0)
    lr_scheduler: str = luigi.ChoiceParameter(choices=KERAS_LR_SCHEDULERS.keys(), default="none")
    lr_scheduler_params: dict = luigi.DictParameter(default={})
    loss_functions: List[str] = luigi.ListParameter(default=["categorical_crossentropy"])
    loss_function_params: List[dict] = luigi.ListParameter(default=[{}])
    loss_function_weights: List[float] = luigi.ListParameter(default=[1.0])
    activity_regularizer: str = luigi.ChoiceParameter(choices=KERAS_REGULARIZERS.keys(), default="none")
    activity_regularizer_value: float = luigi.FloatParameter(default=0.01)
    kernel_regularizer: str = luigi.ChoiceParameter(choices=KERAS_REGULARIZERS.keys(), default="none")
    kernel_regularizer_value: float = luigi.FloatParameter(default=0.01)

    frozen_layers: int = luigi.IntParameter(default=0)
    fine_tuning_frozen_layers: List[Union[int, float]] = luigi.ListParameter(default=[])
    fine_tuning_epochs: List[int] = luigi.ListParameter(default=[])

    mixup: float = luigi.FloatParameter(default=0.0)

    use_tf_augmenter: bool = luigi.BoolParameter(default=False)
    enable_mixed_precision: bool = luigi.BoolParameter(default=False)

    generator_workers: int = luigi.IntParameter(default=None)

    @property
    def augmenter(self) -> Union[iaa.Augmenter, Callable[[tf.Tensor, tf.Tensor], Tuple[tf.Tensor, tf.Tensor]]]:
        if self.use_tf_augmenter:
            return self.tf_augmenter
        else:
            return super().augmenter

    @property
    def image_input_shape(self) -> Union[Tuple[int, int, int], Tuple[int, int, int, int]]:
        channels = 3 if self.image_color_mode == "rgb" else 1
        if self.project_config.input_type == InputType.IMAGE_2D \
                or self.project_config.input_type == InputType.MULTI_VIEW_IMAGE_2D:
            if self.image_data_format == "channels_last":
                return self.input_shape[0], self.input_shape[1], channels
            else:
                return channels, self.input_shape[0], self.input_shape[1]
        elif self.project_config.input_type == InputType.IMAGE_3D:
            if self.image_data_format == "channels_last":
                return self.input_shape[0], self.input_shape[1], self.number_of_slices, channels
            else:
                return channels, self.input_shape[0], self.input_shape[1], self.number_of_slices
        else:
            raise NotImplementedError(f"Not implemented for {self.project_config.input_type}")

    @property
    def weights_path(self) -> str:
        weights_path = get_keras_weights_path(self.output_path)
        if self.tpu_bucket:
            return os.path.join(self.tpu_bucket, weights_path)
        return weights_path

    @property
    def swa_weights_path(self) -> str:
        weights_path = get_keras_swa_weights_path(self.output_path)
        if self.tpu_bucket:
            return os.path.join(self.tpu_bucket, weights_path)
        return weights_path

    def create_input_tensor(self) -> tf.Tensor:
        return KerasInput(shape=self.image_input_shape)

    @abc.abstractmethod
    def create_model(self) -> KerasModel:
        """Retornar um modelo do Keras"""
        pass

    def get_metrics(self):
        metrics: Dict[str, List[Callable]] = {}
        for task in self.tasks:
            if task.type == TaskType.SOFTMAX_CLASSIFICATION or task.type == TaskType.MULTILABEL_CLASSIFICATION:
                task_metrics: List[Callable] = ["acc"]
                if not self.tpu_address:  # TODO Make it work in TPU
                    for i, _ in enumerate(task.labels):
                        precision = KerasPrecision(class_id=i, name="precision_%d" % i, dtype="float32")
                        recall = KerasRecall(class_id=i, name="recall_%d" % i, dtype="float32")
                        f1_score = KerasFBetaScore(class_id=i, name="f1_score_%d" % i, dtype="float32")
                        task_metrics.extend([precision, recall, f1_score])
                if task.type == TaskType.MULTILABEL_CLASSIFICATION:
                    task_metrics.append(keras_binary_crossentropy)
                metrics[task.output_layer_name] = task_metrics
            if task.type == TaskType.SEGMENTATION:
                metrics[task.output_layer_name] = [keras_iou_score]
        return metrics

    def set_keras_dtype(self):
        K.set_floatx(self.dtype)
        K.set_epsilon(1e-4 if self.dtype == "float16" else 1e-7)

    def before_train(self):
        tf.set_random_seed(self.seed)
        self.set_keras_dtype()

    def after_train(self):
        # K.clear_session()
        pass

    def lr_find(self):
        with self.strategy.scope():
            self.keras_model = self.create_model()
            if self.initial_weights_path:
                self.keras_model.load_weights(self.initial_weights_path, by_name=True)

            self.keras_model.compile(optimizer=self.get_optimizer(), loss=self.get_loss_functions(),
                                     loss_weights=self.get_loss_function_weights())

        train_loader = self.get_train_generator()

        self.learning_rate = 1e-6
        lr_finder = KerasLearningRateFinder(min(len(train_loader), self.lr_find_iterations), self.learning_rate)
        self.keras_model.fit(train_loader, steps_per_epoch=self.lr_find_iterations, epochs=1,
                             verbose=1, callbacks=[lr_finder], shuffle=False)

        loss_per_lr_path = os.path.join(self.output_path, "loss_per_lr.jpg")
        loss_derivatives_per_lr_path = os.path.join(self.output_path, "loss_derivatives_per_lr.jpg")

        loss_per_lr_fig = plot_loss_per_lr(lr_finder.learning_rates, lr_finder.loss_values)
        loss_per_lr_fig.savefig(loss_per_lr_path)
        loss_derivatives_per_lr_fig = plot_loss_derivatives_per_lr(lr_finder.learning_rates,
                                                                   lr_finder.get_loss_derivatives(5))
        loss_derivatives_per_lr_fig.savefig(loss_derivatives_per_lr_path)
        plt.close(loss_per_lr_fig)
        plt.close(loss_derivatives_per_lr_fig)

    def fit(self):
        with self.strategy.scope():
            self.keras_model = self.create_model()

            for layer in self.keras_model.layers[:self.frozen_layers]:
                layer.trainable = False

            with open("%s/summary.txt" % self.output_path, "w") as summary_file:
                with redirect_stdout(summary_file):
                    self.keras_model.summary()

            if self.initial_weights_path:
                self.keras_model.load_weights(self.initial_weights_path, by_name=True)
            if self.kernel_regularizer != "none":
                for layer in self.keras_model.layers:
                    if hasattr(layer, 'kernel_regularizer'):
                        layer.kernel_regularizer = KERAS_REGULARIZERS[self.kernel_regularizer](
                            self.kernel_regularizer_value)
            if self.activity_regularizer != "none":
                for layer in self.keras_model.layers:
                    if hasattr(layer, 'activity_regularizer'):
                        layer.activity_regularizer = KERAS_REGULARIZERS[self.activity_regularizer](
                            self.activity_regularizer_value)
            metrics = self.get_metrics()
            self.keras_model.compile(optimizer=self.get_optimizer(), loss=self.get_loss_functions(),
                                     loss_weights=self.get_loss_function_weights(), metrics=metrics)

        try:
            self.keras_model.fit(self.train_generator, epochs=self.epochs,
                                 steps_per_epoch=self.train_steps_per_epoch,
                                 validation_data=self.val_generator,
                                 validation_steps=self.val_steps_per_epoch,
                                 class_weight={int(k): v for k, v in
                                               self.class_weight.items()} if self.class_weight else None,
                                 verbose=1, callbacks=self._get_callbacks(self.keras_model),
                                 workers=self.generator_workers,
                                 max_queue_size=self.generator_max_queue_size,
                                 use_multiprocessing=self.use_multiprocessing,
                                 shuffle=False)
        except (KeyboardInterrupt, KeyError) as e:
            print("Finishing the training at the request of the user...")

        history_df = pd.read_csv(get_history_path(self.output_path))

        history_fig = plot_history(history_df)
        history_fig.savefig(get_history_plot_path(self.output_path))
        plt.close(history_fig)

    def get_loss_functions(self) -> Dict[str, Callable]:
        assert len(self.tasks) == len(self.loss_functions)
        if len(self.tasks) > 1:
            if len(self.loss_function_params) == 1 and self.loss_function_params[0] == {}:
                self.loss_function_params = [{}] * len(self.tasks)
        loss_functions: Dict[str, Callable] = {}
        for task, loss_function_key, loss_function_params in zip(self.tasks, self.loss_functions,
                                                                 self.loss_function_params):
            loss_function = KERAS_LOSS_FUNCTIONS[loss_function_key](**loss_function_params)
            if self.label_smoothing:
                loss_function = label_smoothing_loss_function(loss_function, self.label_smoothing,
                                                              task.type == TaskType.MULTILABEL_CLASSIFICATION)
            loss_functions[task.output_layer_name] = loss_function

        return loss_functions

    def get_loss_function_weights(self):
        if len(self.tasks) > 1:
            if len(self.loss_function_weights) == 1 and self.loss_function_weights[0] == 1.0:
                self.loss_function_weights = [1.0] * len(self.tasks)
        weights: Dict[str, float] = {}
        for task, weight in zip(self.tasks, self.loss_function_weights):
            weights[task.output_layer_name] = weight
        return weights

    def get_optimizer(self) -> KerasOptimizer:
        optimizer_cls = KERAS_OPTIMIZERS[self.optimizer]
        if self.weight_decay:
            optimizer = keras_extend_with_decoupled_weight_decay(optimizer_cls)(weight_decay=self.weight_decay,
                                                                                lr=self.learning_rate,
                                                                                **self.optimizer_params)
        else:
            optimizer = optimizer_cls(lr=self.learning_rate, **self.optimizer_params)
        if self.optimizer_lookahead:
            optimizer = KerasLookahead(optimizer, **self.optimizer_lookahead_params)
        if self.enable_mixed_precision:
            optimizer = tf.train.experimental.enable_mixed_precision_graph_rewrite(optimizer)
        return optimizer

    def _get_callbacks(self, keras_model: KerasModel) -> List[KerasCallback]:
        tensorboard_callback = KerasTensorBoard(get_tensorboard_logdir(self.task_id))
        callbacks = [*self._get_extra_callbacks(keras_model, tensorboard_callback),
                     KerasModelCheckpoint(self.weights_path, save_best_only=True,
                                          monitor=self.monitor_metric, mode=self.monitor_mode, save_weights_only=True,
                                          verbose=1),
                     KerasEarlyStopping(patience=self.early_stopping_patience, min_delta=self.early_stopping_min_delta,
                                        monitor=self.monitor_metric, mode=self.monitor_mode, verbose=True),
                     KerasCSVLogger(get_history_path(self.output_path)),
                     tensorboard_callback]

        if self.fine_tuning_frozen_layers or self.fine_tuning_epochs:
            assert len(self.fine_tuning_frozen_layers) == len(self.fine_tuning_epochs)
            callbacks.append(KerasFineTuningCallback(list(zip(self.fine_tuning_frozen_layers, self.fine_tuning_epochs)),
                                                     self.strategy))

        if self.plot_training:
            callbacks.append(KerasPlotTraining(self.output_path))

        if self.swa_last_epochs > 0 and self.swa_last_epochs < self.epochs:
            callbacks.append(KerasSWA(filepath=self.swa_weights_path,
                                      swa_epoch=self.epochs - self.swa_last_epochs))

        warmup_batches = self.warmup_batches or \
                         (self.warmup_epochs * self.train_steps_per_epoch if self.warmup_epochs else None)
        lr_scheduler = KERAS_LR_SCHEDULERS[self.lr_scheduler](
            **self.lr_scheduler_params) if self.lr_scheduler != "none" else None
        if warmup_batches:
            callbacks.append(
                WarmUpLearningRateScheduler(warmup_batches, self.warmup_init_lr, self.learning_rate, lr_scheduler))
        elif lr_scheduler:
            callbacks.append(lr_scheduler)
        return callbacks

    def _get_extra_callbacks(self, keras_model: KerasModel, tensorboard_callback: KerasTensorBoard) -> List[
        KerasCallback]:
        return []

    def get_train_generator(self, shuffle: bool = True) -> tf.data.Dataset:
        return self.train_dataset.to_tf_dataset(self.batch_size, shuffle=shuffle, mixup=self.mixup, seed=self.seed)

    def get_val_generator(self) -> tf.data.Dataset:
        return self.val_dataset.to_tf_dataset(self.val_batch_size, shuffle=False, seed=self.seed)

    def get_test_generator(self) -> tf.data.Dataset:
        return self.test_dataset.to_tf_dataset(self.val_batch_size, shuffle=False, seed=self.seed)

    @property
    def train_generator(self):
        if not hasattr(self, "_train_generator"):
            self._train_generator = self.get_train_generator()
        return self._train_generator

    @property
    def val_generator(self):
        if not hasattr(self, "_val_generator"):
            self._val_generator = self.get_val_generator()
        return self._val_generator

    @property
    def test_generator(self):
        if not hasattr(self, "_test_generator"):
            self._test_generator = self.get_test_generator()
        return self._test_generator

    def get_trained_model(self) -> KerasModel:
        model = self.create_model()
        model.load_weights(self.weights_path, by_name=True)
        return model

    @property
    def tensorflow_device(self) -> str:
        if self.tpu_address:
            return "/job:worker"
        elif self.device == "cuda":
            return f"/device:GPU:{self.device_id}"
        else:
            return "/cpu:0"

    @property
    def strategy(self):  # -> tf.contrib.distribute.Strategy:
        if not hasattr(self, "_strategy"):
            if self.tpu_address:
                cluster_resolver = tf.contrib.cluster_resolver.TPUClusterResolver(
                    tpu=self.tpu_address)
                connect_to_remote_host(cluster_resolver.master())
                tf.contrib.distribute.initialize_tpu_system(cluster_resolver)
                self._strategy = tf.contrib.distribute.TPUStrategy(cluster_resolver)
            elif self.multi_gpu:
                self._strategy = tf.contrib.distribute.MirroredStrategy()
            else:
                self._strategy = tf.contrib.distribute.OneDeviceStrategy(self.tensorflow_device)
        return self._strategy


class KerasImageModelTraining(BaseKerasModelTraining, metaclass=abc.ABCMeta):
    backbone: str = luigi.ChoiceParameter(choices=KERAS_BASE_MODELS.keys())
    backbone_weights: str = luigi.Parameter(default="imagenet")
    dropout_between_backbone_layers: float = luigi.FloatParameter(default=0.0)

    # Each element must be a dict with "type" (layers) and "params" (kwargs for the given layer)
    custom_cnn_layers: List[dict] = luigi.ListParameter(default=[])

    base_model_last_layer: str = luigi.ChoiceParameter(choices=["global_avg_pooling", "global_max_pooling", "flatten"],
                                                       default="global_avg_pooling")

    def create_backbone_model(self) -> KerasModel:
        image_2d = self.project_config.input_type == InputType.IMAGE_2D \
                   or self.project_config.input_type == InputType.MULTI_VIEW_IMAGE_2D

        backbone_weights = self.backbone_weights if self.backbone_weights != "none" else None
        backbone: KerasModel = KERAS_BASE_MODELS[self.backbone](
            # input_shape=self.image_input_shape,
            input_tensor=self.create_input_tensor(),
            weights=backbone_weights, include_top=False)

        output = {
            "global_avg_pooling": KerasGlobalAveragePooling2D if image_2d else KerasGlobalAveragePooling3D,
            "global_max_pooling": KerasGlobalMaxPooling2D if image_2d else KerasGlobalMaxPooling3D,
            "flatten": KerasFlatten,
        }[self.base_model_last_layer]()(backbone.output)
        return KerasModel(backbone.input, output)

    def create_custom_cnn(self) -> KerasModel:
        input_ = super().create_input_tensor()

        output = input_
        for layer_def in self.custom_cnn_layers:
            layer_cls = getattr(keras_layers, layer_def["type"])
            if "kernel_initializer" in inspect.signature(layer_cls).parameters:
                layer = layer_cls(kernel_initializer=KERAS_WEIGHT_INIT[self.kernel_initializer](self.seed),
                                  **layer_def["params"])
            else:
                layer = layer_cls(**layer_def["params"])
            output = layer(output)

        image_2d = self.project_config.input_type == InputType.IMAGE_2D \
                   or self.project_config.input_type == InputType.MULTI_VIEW_IMAGE_2D

        output = {
            "global_avg_pooling": KerasGlobalAveragePooling2D if image_2d else KerasGlobalAveragePooling3D,
            "global_max_pooling": KerasGlobalMaxPooling2D if image_2d else KerasGlobalMaxPooling3D,
            "flatten": KerasFlatten,
        }[self.base_model_last_layer]()(output)

        return KerasModel(input_, output)

    @abc.abstractmethod
    def create_model_with(self, input: Union[tf.Tensor, List[tf.Tensor]],
                          output: Union[tf.Tensor, List[tf.Tensor]]) -> KerasModel:
        """Retornar um modelo utilizando o modelo pré-treinado passado como parâmetro"""
        pass

    def create_model(self) -> KerasModel:
        image_2d = self.project_config.input_type == InputType.IMAGE_2D \
                   or self.project_config.input_type == InputType.MULTI_VIEW_IMAGE_2D

        base_model = self.create_custom_cnn() if self.backbone == "custom" else self.create_backbone_model()

        if self.dropout_between_backbone_layers:
            dropout_class = KerasSpatialDropout2D if image_2d else KerasSpatialDropout3D
            base_model = insert_layer_nonseq(
                base_model, '.*activation.*',
                lambda: dropout_class(rate=self.dropout_between_backbone_layers, name='dropout')
            )

        self.keras_model = self.create_model_with(base_model.input, base_model.output)

        return self.keras_model


def load_torch_model_training_from_task_dir(model_cls: Type[BaseTorchModelTraining],
                                            task_dir: str) -> BaseTorchModelTraining:
    model_training = model_cls(**get_params(task_dir))
    model_training._output_path = task_dir
    return model_training


def load_torch_model_training_from_task_id(project: str, model_cls: Type[BaseTorchModelTraining],
                                           task_id: str) -> BaseTorchModelTraining:
    task_dir = get_task_dir(PROJECTS[project].base_dir, model_cls, task_id)

    return load_torch_model_training_from_task_dir(model_cls, task_dir)


def load_keras_model_training_from_task_dir(model_cls: Type[BaseKerasModelTraining],
                                            task_dir: str) -> BaseKerasModelTraining:
    model_training = model_cls(**get_params(task_dir))
    model_training._output_path = task_dir
    return model_training


def load_keras_model_training_from_task_id(project: str, model_cls: Type[BaseKerasModelTraining],
                                           task_id: str) -> BaseKerasModelTraining:
    task_dir = get_task_dir(PROJECTS[project].base_dir, model_cls, task_id)

    return load_keras_model_training_from_task_dir(model_cls, task_dir)
