import json
import os
from typing import Tuple, Dict, Union, List

import luigi
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
import tensorflow.keras.backend as K

from datalife.data import ImageIterator, ImageDataset, calculate_steps_per_epoch
from datalife.task.config import PROJECTS
from datalife.task.data_preparation.intracranial_hemorrhage_detection import TransformRSNA2019Stage1DataFrames, \
    ResizeRSNA2019Stage1Images, TransformRSNA2019Stage2DataFrames, ResizeRSNA2019Stage2Images
from datalife.task.evaluation import BaseEvaluationTask
from datalife.task.meta_config import TaskType


class GenerateFirstStageSubmissionFile(BaseEvaluationTask):
    project: str = luigi.Parameter(default="intracranial_hemorrhage_detection")
    test_time_augmentation: bool = luigi.BoolParameter(default=False)
    tta_steps: int = luigi.IntParameter(default=5)

    def requires(self) -> Tuple[TransformRSNA2019Stage1DataFrames,
                                ResizeRSNA2019Stage1Images]:
        return TransformRSNA2019Stage1DataFrames(), \
               ResizeRSNA2019Stage1Images(target_shape=self.model_training.input_shape,
                                          interpolation=self.model_training.interpolation,
                                          **self.model_training.resize_images_task_extra_params)

    @property
    def data_frame_path(self) -> str:
        return self.input()[0][1].path

    @property
    def images_path(self) -> str:
        return self.input()[1].path

    def output(self):
        if self.test_time_augmentation:
            return luigi.LocalTarget(super().output().path + "_tta_{}".format(self.tta_steps))
        else:
            return super().output()

    def evaluate(self, df: pd.DataFrame, dataset: ImageDataset, generator: tf.data.Dataset):
        try:
            self.model_training.set_keras_dtype()
            with self.model_training.strategy.scope():
                trained_model = self.model_training.get_trained_model()

            print("Executing the model...")

            steps = calculate_steps_per_epoch(dataset, self.model_training.val_batch_size)
            if self.test_time_augmentation:
                predictions = []
                dataset.set_augmenter(self.model_training.augmenter)
                generator = dataset.to_tf_dataset(self.model_training.val_batch_size, False)
                for i in range(self.tta_steps):
                    print("Test Time Augmentation {}/{}".format(i + 1, self.tta_steps))
                    model_preds = trained_model.predict(generator, steps=steps, verbose=1)[:len(dataset)]
                    predictions.append(model_preds)
                model_preds = np.mean(predictions, axis=0)
            else:
                model_preds = trained_model.predict(generator, steps=steps, verbose=1)[:len(dataset)]

            data_frames: List[pd.DataFrame] = []
            for i, task in enumerate(self.model_training.tasks):
                if task.type == TaskType.MULTILABEL_CLASSIFICATION:
                    preds: np.ndarray = np.array(model_preds if len(self.model_training.tasks) == 1 else model_preds[i])
                    columns = [column for column in task.mapping.keys()]
                    data_frames.append(pd.DataFrame(data=preds, columns=columns))
                elif task.type == TaskType.SOFTMAX_CLASSIFICATION and \
                        task.target_column in ("any", "epidural", "intraparenchymal", "intraventricular",
                                               "subarachnoid", "subdural"):
                    preds: np.ndarray = np.array(model_preds if len(self.model_training.tasks) == 1 else model_preds[i])
                    data_frames.append(pd.DataFrame(data=preds[:, 1], columns=[task.target_column]))
            preds_df = pd.concat(data_frames, axis=1)
            submission_df = df[["imageId"]].join(preds_df)
            submission_df: pd.DataFrame = submission_df.melt('imageId', var_name='type', value_name='Label')
            submission_df["ID"] = submission_df.apply(lambda row: "%s_%s" % (row["imageId"], row["type"]), axis=1)
            submission_df = submission_df[["ID", "Label"]]

            submission_df.to_csv(os.path.join(self.output().path, "submission.csv"), index=False)
        finally:
            K.clear_session()


class GenerateSecondStageSubmissionFile(GenerateFirstStageSubmissionFile):
    def requires(self) -> Tuple[TransformRSNA2019Stage1DataFrames,
                                ResizeRSNA2019Stage1Images]:
        return TransformRSNA2019Stage2DataFrames(), \
               ResizeRSNA2019Stage2Images(target_shape=self.model_training.input_shape,
                                          interpolation=self.model_training.interpolation,
                                          **self.model_training.resize_images_task_extra_params)


class EvaluateModelWithCompetitionScore(BaseEvaluationTask):
    project: str = luigi.Parameter(default="intracranial_hemorrhage_detection")

    _WEIGHTS: Dict[str, float] = {
        "any": 2,
        "epidural": 1,
        "intraparenchymal": 1,
        "intraventricular": 1,
        "subarachnoid": 1,
        "subdural": 1,
    }

    @staticmethod
    def _log_loss(preds: np.ndarray, targets: np.ndarray, eps=1e-15):
        preds = np.clip(preds, eps, 1 - eps)
        return np.mean(-(targets * np.log(preds) + (1 - targets) * np.log(1 - preds)))

    @staticmethod
    def _weighted_log_loss(preds: np.ndarray, targets: np.ndarray, weights: np.ndarray, eps=1e-15):
        preds = np.clip(preds, eps, 1 - eps)
        return np.mean(-(targets * np.log(preds) + (1 - targets) * np.log(1 - preds)) * weights)

    def evaluate(self, df: pd.DataFrame, dataset: ImageDataset, generator: ImageIterator):
        return "dummy"

    def run(self):
        os.makedirs(self.output().path, exist_ok=True)

        try:
            self.model_training.set_keras_dtype()
            with self.model_training.strategy.scope():
                trained_model = self.model_training.get_trained_model()
            i, task = [(i, task) for i, task in enumerate(self.model_training.tasks)
                       if task.type == TaskType.MULTILABEL_CLASSIFICATION][0]
            print("Executing the model...")
            train_generator = self.model_training.get_train_generator(shuffle=False)
            model_train_preds: Union[np.ndarray, List[np.ndarray]] = trained_model.predict(
                train_generator, steps=self.model_training.train_steps_per_epoch,
                verbose=1)[:len(self.model_training.train_dataset)]
            model_val_preds: Union[np.ndarray, List[np.ndarray]] = trained_model.predict(
                self.model_training.val_generator, steps=self.model_training.val_steps_per_epoch,
                verbose=1)[:len(self.model_training.val_dataset)]
            model_test_preds: Union[np.ndarray, List[np.ndarray]] = trained_model.predict_generator(
                self.model_training.test_generator, steps=self.model_training.test_steps_per_epoch,
                verbose=1)[:len(self.model_training.test_dataset)]
            train_preds: np.ndarray = np.array(model_train_preds if len(self.model_training.tasks) == 1
                                               else model_train_preds[i])
            val_preds: np.ndarray = np.array(model_val_preds if len(self.model_training.tasks) == 1
                                             else model_val_preds[i])
            test_preds: np.ndarray = np.array(model_test_preds if len(self.model_training.tasks) == 1
                                              else model_test_preds[i])
            weights = np.array([self._WEIGHTS[type] for type in task.mapping.keys()])
            weights = weights / weights.sum() * len(weights)
            metrics = {
                "train_log_loss": self._log_loss(train_preds,
                                                 self.model_training.train_dataset.targets[task.output_layer_name]),
                "train_weighted_log_loss": self._weighted_log_loss(train_preds,
                                                                   self.model_training.train_dataset.targets[
                                                                       task.output_layer_name], weights),
                "val_log_loss": self._log_loss(val_preds,
                                               self.model_training.val_dataset.targets[task.output_layer_name]),
                "val_weighted_log_loss": self._weighted_log_loss(val_preds,
                                                                 self.model_training.val_dataset.targets[
                                                                     task.output_layer_name], weights),
                "test_log_loss": self._log_loss(test_preds,
                                                self.model_training.test_dataset.targets[task.output_layer_name]),
                "test_weighted_log_loss": self._weighted_log_loss(test_preds, self.model_training.test_dataset.targets[
                    task.output_layer_name], weights),
            }

            with open(os.path.join(self.output().path, "metrics.json"), "w") as f:
                json.dump(metrics, f, indent=4)

        finally:
            K.clear_session()


class GenerateFirstStageSubmissionFileEnsemble(luigi.Task):
    project: str = luigi.Parameter(default="intracranial_hemorrhage_detection")
    test_time_augmentation: bool = luigi.BoolParameter(default=False)
    tta_steps: int = luigi.IntParameter(default=5)
    tasks_ids: dict = luigi.ListParameter(default=[])
    submission_cls: str = luigi.Parameter(default="GenerateFirstStageSubmissionFile")
    weights: List[float] = luigi.ListParameter(default=[])

    def __get_hash_id(self):
        return self.task_id.split('_')[-1]

    def output(self):
        return luigi.LocalTarget(
            os.path.join(PROJECTS[self.project].base_dir, "evaluation", self.__class__.__name__, "results",
                         self.__get_hash_id()))

    def run(self):
        os.makedirs(self.output().path, exist_ok=True)
        df = pd.DataFrame()
        composition = []
        for _id in self.tasks_ids:
            submission_nn = os.path.join(PROJECTS[self.project].base_dir, "evaluation", self.submission_cls, 'results',
                                         _id, 'submission.csv')
            print('Processing ... ', submission_nn)
            composition.append(dict(task_id=_id, source_path=submission_nn))

            if not os.path.exists(submission_nn):
                raise FileNotFoundError(
                    'Submission not found. Run the {} for this task id > {}'.format(self.submission_cls, _id))

            model_df = pd.read_csv(submission_nn)
            df['ID'] = model_df['ID']
            df[_id] = model_df['Label']

        self.__create_correlation(df)

        submission_df = pd.DataFrame()
        submission_df['ID'] = df['ID']
        if self.weights:
            df = df.drop(columns=["ID"])
            assert len(self.weights) == len(df.columns)
            weights = np.array(self.weights)
            submission_df['Label'] = np.sum(df.values * weights, axis=1) / np.sum(weights)
        else:
            submission_df['Label'] = df.mean(axis=1)
        submission_df.to_csv(os.path.join(self.output().path, "submission_{}.csv".format(self.__get_hash_id())),
                             index=False)

        with open(os.path.join(self.output().path, "composition.json"), 'w') as f:
            json.dump(composition, f, indent=4)

    def __create_correlation(self, df, figsize=(10, 7)):
        fig, ax = plt.subplots(figsize=figsize)
        sns.set(font_scale=1.4)
        sns.heatmap(df.corr(method='pearson'), annot=True, fmt='.4f',
                    cmap=plt.get_cmap('Blues'), cbar=False, ax=ax)
        ax.set_yticklabels(ax.get_yticklabels(), rotation="horizontal")
        fig.tight_layout()
        ax.set_ylim([len(df.columns) - 1, -.5])
        plt.savefig(os.path.join(self.output().path, 'correlation.png'))


class GenerateSecondStageSubmissionFileEnsemble(GenerateFirstStageSubmissionFileEnsemble):
    submission_cls: str = luigi.Parameter(default="GenerateSecondStageSubmissionFile")
