from collections import OrderedDict
from typing import Dict

from datalife.data import ImageDataset
from datalife.task.data_preparation import intracranial_hemorrhage_detection
# Importante: Em caso de classificação contendo classe negativa, ela deve ser a primeira classe da softmax correto
# funcionamento de thresholds
from datalife.task.meta_config import ProjectConfig, TargetMapping, InputType, TaskType, Task

PROJECTS: Dict[str, ProjectConfig] = {
    "intracranial_hemorrhage_detection": ProjectConfig(
        base_dir=intracranial_hemorrhage_detection.BASE_DIR,
        prepare_data_frames_task=intracranial_hemorrhage_detection.PrepareRSNA2019Stage2DataFrames,
        resize_images_task=intracranial_hemorrhage_detection.ResizeRSNA2019Stage2Images,
        tasks=[
            Task(
                target_column="hemorrhages",
                type=TaskType.MULTILABEL_CLASSIFICATION,
                mapping=OrderedDict([
                    ("any", TargetMapping(label="Hemorrhage")),
                    ("epidural", TargetMapping(label="Epidural Hemorrhage")),
                    ("intraparenchymal", TargetMapping(label="Intraparenchymal Hemorrhage")),
                    ("intraventricular", TargetMapping(label="Intraventricular Hemorrhage")),
                    ("subarachnoid", TargetMapping(label="Subarachnoid Hemorrhage")),
                    ("subdural", TargetMapping(label="Subdural Hemorrhage")),
                ]),
            ),
            Task(
                target_column="instanceNumberBinnedAt3",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (i, TargetMapping(label="%d" % i)) for i in range(3)
                ]),
            ),
            Task(
                target_column="instanceNumberBinnedAt4",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (i, TargetMapping(label="%d" % i)) for i in range(4)
                ]),
            ),
            Task(
                target_column="instanceNumberBinnedAt5",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (i, TargetMapping(label="%d" % i)) for i in range(5)
                ]),
            ),
            Task(
                target_column="instanceNumberBinnedAt6",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (i, TargetMapping(label="%d" % i)) for i in range(6)
                ]),
            ),
            Task(
                target_column="instanceNumberBinnedAt7",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (i, TargetMapping(label="%d" % i)) for i in range(7)
                ]),
            ),
            Task(
                target_column="instanceNumberBinnedAt8",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (i, TargetMapping(label="%d" % i)) for i in range(8)
                ]),
            ),
            Task(
                target_column="instanceNumberBinnedAt9",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (i, TargetMapping(label="%d" % i)) for i in range(9)
                ]),
            ),
            Task(
                target_column="instanceNumberBinnedAt10",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (i, TargetMapping(label="%d" % i)) for i in range(10)
                ]),
            ),
            Task(
                target_column="any",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (0, TargetMapping(label="No Hemorrhage")),
                    (1, TargetMapping(label="Hemorrhage")),
                ]),
                has_negative_target=True,
            ),
            Task(
                target_column="epidural",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (0, TargetMapping(label="No Epidural Hemorrhage")),
                    (1, TargetMapping(label="Epidural Hemorrhage")),
                ]),
                has_negative_target=True,
            ),
            Task(
                target_column="intraparenchymal",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (0, TargetMapping(label="No Intraparenchymal Hemorrhage")),
                    (1, TargetMapping(label="Intraparenchymal Hemorrhage")),
                ]),
                has_negative_target=True,
            ),
            Task(
                target_column="intraventricular",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (0, TargetMapping(label="No Intraventricular Hemorrhage")),
                    (1, TargetMapping(label="Intraventricular Hemorrhage")),
                ]),
                has_negative_target=True,
            ),
            Task(
                target_column="subarachnoid",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (0, TargetMapping(label="No Subarachnoid Hemorrhage")),
                    (1, TargetMapping(label="Subarachnoid Hemorrhage")),
                ]),
                has_negative_target=True,
            ),
            Task(
                target_column="subdural",
                type=TaskType.SOFTMAX_CLASSIFICATION,
                mapping=OrderedDict([
                    (0, TargetMapping(label="No Subdural Hemorrhage")),
                    (1, TargetMapping(label="Subdural Hemorrhage")),
                ]),
                has_negative_target=True,
            ),
        ],
        default_target_cols=["hemorrhages"],
        default_balance_fields=["any"],
        input_type=InputType.IMAGE_2D,
        dataset_class=ImageDataset,
    ),
}
