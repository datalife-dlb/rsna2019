from collections import OrderedDict
from enum import Enum, auto
from typing import Dict, List, Type, Union

import numpy as np
from torch.utils.data import Dataset

from datalife.task.data_preparation.base import BasePrepareDataFrames, BaseResizeImages


class InputType(Enum):
    IMAGE_2D = auto()
    MULTI_VIEW_IMAGE_2D = auto()
    IMAGE_3D = auto()


class TargetMapping(object):
    def __init__(self, label: str) -> None:
        self.label = label


class TaskType(Enum):
    SOFTMAX_CLASSIFICATION = auto()
    REGRESSION = auto()
    MULTILABEL_CLASSIFICATION = auto()
    SEGMENTATION = auto()


class Task(object):
    def __init__(self, target_column: str, type: TaskType, has_negative_target: bool = False,
                 mapping: Dict[Union[str, int], TargetMapping] = None,
                 output_layer_name: str = "output") -> None:
        self.target_column = target_column
        self.type = type
        self.has_negative_target = has_negative_target
        self.output_layer_name = output_layer_name
        self.mapping = mapping
        assert mapping is None or isinstance(mapping, OrderedDict)

    @property
    def labels(self):
        return [mapping.label for mapping in self.mapping.values()]


class ProjectConfig(object):
    def __init__(self, base_dir: str, prepare_data_frames_task: Type[BasePrepareDataFrames],
                 resize_images_task: Type[BaseResizeImages], tasks: List[Task],
                 default_target_cols: List[str], default_balance_fields: List[str],
                 input_type: InputType, dataset_class: Type[Dataset],
                 default_number_of_slices: int = None,
                 data_frames_preparation_extra_params: dict = {}) -> None:
        self.base_dir = base_dir
        self.prepare_data_frames_task = prepare_data_frames_task
        self.resize_images_task = resize_images_task
        self.tasks = tasks
        self.default_target_cols = default_target_cols
        self.default_balance_fields = default_balance_fields
        self.input_type = input_type
        self.dataset_class = dataset_class
        self.default_number_of_slices = default_number_of_slices
        self.data_frames_preparation_extra_params = data_frames_preparation_extra_params