from typing import List, Union

import luigi
import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Activation, Dropout, Dense, BatchNormalization
from tensorflow.keras.losses import mean_absolute_error

from datalife.keras.metrics import root_mean_squared_error
from datalife.task.base import KERAS_ACTIVATION_FUNCTIONS, KERAS_WEIGHT_INIT, \
    KerasImageModelTraining, KERAS_LOSS_FUNCTIONS


# - Para múltiplas GPUs:
## ./cuda_luigid --background --pidfile /tmp/luigid.pid --logdir /tmp/luigid_log
## PYTHONPATH="." ./cuda_luigi --module datalife.task.model.image_regressor ImageRegressor --project bone_age --learning-rate 0.01 --dense-layers '[1024, 512]'
# - Para uma única GPU:
## PYTHONPATH="." luigi --module datalife.task.model.image_regressor ImageRegressor --project bone_age --backbone vgg16 --learning-rate 0.01 --dense-layers '[1024, 512]' --local-scheduler
from datalife.task.meta_config import TaskType
from datalife.task.model.multi_view import MultiViewMixin
from datalife.task.model.window_optimizer import WindowOptimizerMixin


class ImageRegressor(KerasImageModelTraining):
    sampling_strategy: str = luigi.ChoiceParameter(choices=["none"], default="none")
    loss_functions: List[str] = luigi.ListParameter(default=["mean_squared_error"])

    pooling: str = luigi.ChoiceParameter(choices=["avg", "max", "none"], default="avg")
    dense_layers: List[int] = luigi.ListParameter(default=[])
    dropout_between_dense_layers: float = luigi.FloatParameter(default=0.0)
    dropout_after_base_model: float = luigi.FloatParameter(default=0.0)
    activation_function: str = luigi.ChoiceParameter(choices=KERAS_ACTIVATION_FUNCTIONS.keys(), default="relu")
    kernel_initializer: str = luigi.ChoiceParameter(choices=KERAS_WEIGHT_INIT.keys(), default="glorot_uniform")
    bn_after_base_model: bool = luigi.BoolParameter(default=False)
    bn_between_dense_layers: bool = luigi.BoolParameter(default=False)

    def get_metrics(self):
        return [mean_absolute_error, root_mean_squared_error]

    def create_model_with(self, input: Union[tf.Tensor, List[tf.Tensor]],
                          output: Union[tf.Tensor, List[tf.Tensor]]) -> Model:
        assert len(self.tasks) == 1
        assert self.tasks[0].type == TaskType.REGRESSION

        if self.dropout_after_base_model:
            output = Dropout(self.dropout_after_base_model, name="dropout_after_base_model")(output)

        for i, dense_neurons in enumerate(self.dense_layers):
            output = Dense(dense_neurons, kernel_initializer=KERAS_WEIGHT_INIT[self.kernel_initializer](self.seed),
                           name="dense_%d" % i)(output)

            if self.bn_between_dense_layers:
                output = BatchNormalization(name="bn_%d" % i)(output)

            output = Activation(self.activation_function, name="activation_%d" % i)(output)

            if self.dropout_between_dense_layers:
                output = Dropout(self.dropout_between_dense_layers, name="dropout_%d" % i)(output)

        output = Dense(1, activation='linear', kernel_initializer=KERAS_WEIGHT_INIT[self.kernel_initializer](self.seed),
                       name="output")(output)

        return Model(input, output)


class ImageRegressorWithWindowOptimizer(WindowOptimizerMixin, ImageRegressor):
    pass


class MultiViewImageRegressor(MultiViewMixin, ImageRegressor):
    def create_model_with(self, input: Union[tf.Tensor, List[tf.Tensor]],
                          output: Union[tf.Tensor, List[tf.Tensor]]) -> Model:
        input, output = self.create_multi_view_model(input, output)
        return super().create_model_with(input, output)


class MultiViewImageRegressorWithWindowOptimizer(WindowOptimizerMixin, MultiViewImageRegressor):
    pass