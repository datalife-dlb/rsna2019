from typing import Tuple, List, Union

import luigi
import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Input, concatenate, maximum,  minimum, average, add


class MultiViewMixin(object):
    base_model_last_layer: str = luigi.ChoiceParameter(choices=["global_avg_pooling", "global_max_pooling"],
                                                       default="global_avg_pooling")

    merge_features_method: str = luigi.ChoiceParameter(choices=["concatenate", "maximum", "minimum", "average", "add"],
                                                       default="concatenate")

    def create_multi_view_model(self, input: Union[tf.Tensor, List[tf.Tensor]],
                          output: Union[tf.Tensor, List[tf.Tensor]]) -> Tuple[List[Input], tf.Tensor]:
        base_model = Model(input, output)

        inputs: List[Input] = [Input(shape=self.image_input_shape, name="multi_view_input_%d" % i)
                               for i in range(self.number_of_slices)]
        features: List[tf.Tensor] = [base_model(input_) for input_ in inputs]

        output = dict(concatenate=concatenate, maximum=maximum, minimum=minimum,
                      average=average, add=add)[self.merge_features_method](features)

        return inputs, output