from typing import List, Tuple, Union

import luigi
import tensorflow as tf
from tensorflow.keras import Model
import tensorflow_core.python.keras.layers as layers

from datalife.task.model.image_classifier import ImageClassifier
from datalife.task.model.multi_view import MultiViewMixin
from datalife.task.model.window_optimizer import WindowOptimizerMixin


class HighResImageClassifier(ImageClassifier):
    backbone_weights: str = luigi.Parameter(default="none")
    input_shape: Tuple[int, int] = luigi.TupleParameter(default=(1024, 1024))

    # Each element must be a dict with "type" (keras.layers) and "params" (kwargs for the given layer)
    layers_before_base_model: List[dict] = luigi.ListParameter(default=[])

    def create_input_tensor(self) -> tf.Tensor:
        input_ = super().create_input_tensor()

        x = input_
        for layer_def in self.layers_before_base_model:
            layer = getattr(layers, layer_def["type"])(**layer_def["params"])
            x = layer(x)

        return x


class HighResImageClassifierWithWindowOptimizer(WindowOptimizerMixin, HighResImageClassifier):
    pass


class MultiViewHighResImageClassifier(MultiViewMixin, HighResImageClassifier):
    def create_model_with(self, input: Union[tf.Tensor, List[tf.Tensor]],
                          output: Union[tf.Tensor, List[tf.Tensor]]) -> Model:
        input, output = self.create_multi_view_model(input, output)
        return super().create_model_with(input, output)


class MultiViewHighResImageClassifierWithWindowOptimizer(WindowOptimizerMixin, MultiViewHighResImageClassifier):
    pass