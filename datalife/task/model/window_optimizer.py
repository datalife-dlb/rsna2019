import json
import os
from typing import List

import luigi
import tensorflow as tf
from tensorflow.keras import Model

from datalife.keras.window_optimizer.layers import WindowOptimizerConv2D, WindowOptimizerActivation
from datalife.keras.window_optimizer.utils import initialize_windows, get_windows
from datalife.preprocessing import IMAGE_PREPROCESSING_FUNCTIONS


class WindowOptimizerMixin(object):
    image_color_mode: str = luigi.ChoiceParameter(choices=["rgb", "grayscale"], default="grayscale")
    image_preprocessing: str = luigi.ChoiceParameter(choices=IMAGE_PREPROCESSING_FUNCTIONS.keys(), default="none")

    window_optimizer_channels: int = luigi.IntParameter(default=3)
    window_optimizer_activation: str = luigi.ChoiceParameter(choices=["relu", "sigmoid", "regular_sigmoid"],
                                                             default="sigmoid")
    window_optimizer_upbound_window: float = luigi.FloatParameter(default=255.0)
    window_optimizer_initial_widths: List[int] = luigi.ListParameter(default=[])
    window_optimizer_initial_centers: List[int] = luigi.ListParameter(default=[])

    def create_input_tensor(self) -> tf.Tensor:
        input_ = super().create_input_tensor()
        input_ = WindowOptimizerConv2D(self.window_optimizer_channels)(input_)
        input_ = WindowOptimizerActivation(self.window_optimizer_activation,
                                           self.window_optimizer_upbound_window)(input_)
        return input_

    def create_model(self) -> Model:
        model = super().create_model()

        if self.window_optimizer_initial_widths or self.window_optimizer_initial_centers:
            assert len(self.window_optimizer_initial_widths) == len(self.window_optimizer_initial_centers)
            assert len(self.window_optimizer_initial_widths) <= self.window_optimizer_channels
            initialize_windows(model, self.window_optimizer_activation,
                               list(zip(self.window_optimizer_initial_centers, self.window_optimizer_initial_widths)),
                               self.window_optimizer_upbound_window)

        return model

    def after_fit(self, model: Model = None):
        model = self.get_trained_model()
        super().after_fit(model)

        windows = get_windows(model, self.window_optimizer_activation, self.window_optimizer_upbound_window)

        windows_dict = {
            "window_centers": [float(wl) for wl, _ in windows],
            "window_widths": [float(ww) for _, ww in windows],
        }

        with open(os.path.join(self.output_path, "final_windows.json"), "w") as windows_file:
            json.dump(windows_dict, windows_file)
