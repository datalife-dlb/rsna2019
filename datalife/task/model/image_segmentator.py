from typing import Dict, List

import luigi
from tensorflow.keras import Model
from segmentation_models import Unet, FPN, Linknet, PSPNet

from datalife.task.base import KERAS_BASE_MODELS, BaseKerasModelTraining, \
    KERAS_LOSS_FUNCTIONS
from datalife.task.meta_config import TaskType
from datalife.task.model.window_optimizer import WindowOptimizerMixin

KERAS_SEGMENTATION_MODELS: Dict[str, Model] = dict(
    unet=Unet,
    fpn=FPN,
    linknet=Linknet,
    pspnet=PSPNet,
)


class BinaryImageSegmentator(BaseKerasModelTraining):
    loss_function: str = luigi.ChoiceParameter(choices=KERAS_LOSS_FUNCTIONS.keys(), default="dice")
    segmentation_model: str = luigi.ChoiceParameter(choices=KERAS_SEGMENTATION_MODELS.keys())
    backbone: str = luigi.ChoiceParameter(choices=KERAS_BASE_MODELS.keys())
    encoder_weights: str = luigi.Parameter(default="imagenet")
    encoder_freeze: bool = luigi.BoolParameter(default=False)
    segmentation_model_extra_params: dict = luigi.DictParameter(default={})

    def create_model(self) -> Model:
        assert self.project_config.project_type == TaskType.SEGMENTATION

        channels = 3 if self.image_color_mode == "rgb" else 1

        model: Model = KERAS_SEGMENTATION_MODELS[self.segmentation_model](
            input_shape=(self.input_shape[0], self.input_shape[1], channels), backbone_name=self.backbone,
            encoder_weights=self.encoder_weights, encoder_freeze=self.encoder_freeze, classes=1, activation='sigmoid',
            **self.segmentation_model_extra_params, )
        return model


class BinaryImageSegmentatorWithWindowOptimizer(WindowOptimizerMixin, BinaryImageSegmentator):
    pass
