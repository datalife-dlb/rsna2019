from typing import List, Union

import luigi
import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Activation, Dropout, Dense, BatchNormalization

from datalife.task.base import KERAS_ACTIVATION_FUNCTIONS, \
    KERAS_WEIGHT_INIT, KerasImageModelTraining
# - Para múltiplas GPUs:
## ./cuda_luigid --background --pidfile /tmp/luigid.pid --logdir /tmp/luigid_log
## PYTHONPATH="." ./cuda_luigi --module datalife.task.model.image_classifier ImageClassifier --project pneumonia --backbone vgg16 --learning-rate 0.01 --dense-layers '[1024, 512]'
# - Para uma única GPU:
## PYTHONPATH="." luigi --module datalife.task.model.image_classifier ImageClassifier --project pneumonia --backbone vgg16 --learning-rate 0.01 --dense-layers '[1024, 512]' --local-scheduler
from datalife.task.meta_config import TaskType
from datalife.task.model.multi_view import MultiViewMixin
from datalife.task.model.window_optimizer import WindowOptimizerMixin


class ImageMultiTask(KerasImageModelTraining):
    dense_layers: List[int] = luigi.ListParameter(default=[])
    dropout_between_dense_layers: float = luigi.FloatParameter(default=0.0)
    dropout_after_base_model: float = luigi.FloatParameter(default=0.0)
    activation_function: str = luigi.ChoiceParameter(choices=KERAS_ACTIVATION_FUNCTIONS.keys(), default="relu")
    kernel_initializer: str = luigi.ChoiceParameter(choices=KERAS_WEIGHT_INIT.keys(), default="glorot_uniform")
    bn_between_dense_layers: bool = luigi.BoolParameter(default=False)

    def create_model_with(self, input: Union[tf.Tensor, List[tf.Tensor]],
                          output: Union[tf.Tensor, List[tf.Tensor]]) -> Model:
        if self.dropout_after_base_model:
            output = Dropout(self.dropout_after_base_model, name="dropout_after_base_model")(output)

        for i, dense_neurons in enumerate(self.dense_layers):
            output = Dense(dense_neurons, kernel_initializer=KERAS_WEIGHT_INIT[self.kernel_initializer](self.seed),
                           name="dense_%d" % i)(output)

            if self.bn_between_dense_layers:
                output = BatchNormalization(name="dense_bn_%d" % i)(output)

            output = Activation(self.activation_function, name="dense_activation_%d" % i)(output)

            if self.dropout_between_dense_layers:
                output = Dropout(self.dropout_between_dense_layers, name="dense_dropout_%d" % i)(output)

        outputs = []
        for task in self.tasks:
            number_of_classes = len(task.labels)

            if task.type == TaskType.SOFTMAX_CLASSIFICATION or task.type == TaskType.MULTILABEL_CLASSIFICATION:
                outputs.append(Dense(number_of_classes,
                                     activation="softmax" if task.type == TaskType.SOFTMAX_CLASSIFICATION else "sigmoid",
                                     kernel_initializer=KERAS_WEIGHT_INIT[self.kernel_initializer](self.seed),
                                     name=task.output_layer_name)(output))
            elif task.type == TaskType.REGRESSION:
                outputs.append(Dense(1, activation='linear',
                                     kernel_initializer=KERAS_WEIGHT_INIT[self.kernel_initializer](self.seed),
                                     name=task.output_layer_name)(output))
            else:
                raise NotImplementedError("Task type not implemented")

        return Model(input, outputs)


class ImageMultiTaskWithWindowOptimizer(WindowOptimizerMixin, ImageMultiTask):
    pass


class MultiViewImageMultiTask(MultiViewMixin, ImageMultiTask):
    def create_model_with(self, input: Union[tf.Tensor, List[tf.Tensor]],
                          output: Union[tf.Tensor, List[tf.Tensor]]) -> Model:
        input, output = self.create_multi_view_model(input, output)
        return super().create_model_with(input, output)


class MultiViewImageMultiTaskWithWindowOptimizer(WindowOptimizerMixin, MultiViewImageMultiTask):
    pass
