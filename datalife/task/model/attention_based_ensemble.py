import os
from typing import List, Union

import luigi
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras import Model
from tensorflow.keras.layers import Input, Activation, Dropout, Dense, Reshape, BatchNormalization, Concatenate, Dot, \
    Softmax

from datalife.data import ImageIterator, ImageDataset
from datalife.keras.eval import pred_probas_for_classifier
from datalife.plot import plot_attention_heat_map
from datalife.task.base import KERAS_ACTIVATION_FUNCTIONS, \
    KERAS_WEIGHT_INIT, load_keras_model_training_from_task_id, KERAS_REGULARIZERS, \
    KerasImageModelTraining
from datalife.task.model.image_classifier import ImageClassifier


# - Para múltiplas GPUs:
## ./cuda_luigid --background --pidfile /tmp/luigid.pid --logdir /tmp/luigid_log
## PYTHONPATH="." ./cuda_luigi --module datalife.task.model.attention_based_ensemble AttentionBasedImageEnsembleClassifier --project pneumonia --backbone vgg16 --model-training-task-ids '["id1", "id2"]' --learning-rate 0.01
# - Para uma única GPU:
## PYTHONPATH="." luigi --module datalife.task.model.attention_based_ensemble AttentionBasedImageEnsembleClassifier --project pneumonia --backbone vgg16 --model-training-task-ids '["id1", "id2"]' --learning-rate 0.01 --local-scheduler

# TODO: Update
from datalife.task.model.window_optimizer import WindowOptimizerMixin


class AttentionBasedImageEnsembleClassifier(KerasImageModelTraining):
    monitor_metric: str = luigi.Parameter(default="val_acc")
    monitor_mode: str = luigi.ChoiceParameter(choices=["min", "max", "auto"], default="max")

    model_training_task_ids: List[str] = luigi.ListParameter()

    include_ensemble_output_in_attention: bool = luigi.BoolParameter(default=False)
    attention_layer_activity_regularizer: str = luigi.ChoiceParameter(choices=KERAS_REGULARIZERS.keys(), default="none")
    attention_layer_activity_regularizer_value: float = luigi.FloatParameter(default=0.01)

    pooling: str = luigi.ChoiceParameter(choices=["avg", "max", "none"], default="avg")
    dropout_after_base_model: float = luigi.FloatParameter(default=0.0)
    dense_layers_after_base_model: List[int] = luigi.ListParameter(default=[])
    dropout_between_dense_layers_after_base_model: float = luigi.FloatParameter(default=0.0)
    bn_between_dense_layers_after_base_model: bool = luigi.BoolParameter(default=False)
    bn_between_attention_dense_layers: bool = luigi.BoolParameter(default=False)
    attention_dense_layers: List[int] = luigi.ListParameter(default=[])
    dropout_between_attention_dense_layers: float = luigi.FloatParameter(default=0.0)
    activation_function: str = luigi.ChoiceParameter(choices=KERAS_ACTIVATION_FUNCTIONS.keys(), default="relu")
    kernel_initializer: str = luigi.ChoiceParameter(choices=KERAS_WEIGHT_INIT.keys(), default="glorot_uniform")

    @property
    def model_trainings(self) -> List[ImageClassifier]:
        if not hasattr(self, "_model_trainings"):
            self._model_trainings = [load_keras_model_training_from_task_id(self.project, ImageClassifier, model)
                                     for model in self.model_training_task_ids]

            for model_training in self._model_trainings:
                assert model_training.target_labels == self.target_labels
                # Necessário para garantir a mesma quantidade de exemplos no dataset de treinamento
                model_training.dataset_split_method = self.dataset_split_method
                model_training.n_splits = self.n_splits
                model_training.split_index = self.split_index
                model_training.data_frames_preparation_extra_params = self.data_frames_preparation_extra_params
                model_training.sampling_strategy = self.sampling_strategy
                model_training.sampling_proportions = self.sampling_proportions
                model_training.balance_fields = self.balance_fields
                model_training.seed = self.seed
                model_training.use_sampling_in_validation = self.use_sampling_in_validation
                # Faz com que sejam obtidas predições para as imagens reais
                model_training.data_augmented = False
                # Desliga cache por ser desnecessário
                model_training.cache_images = False

        return self._model_trainings

    def requires(self):
        return [required_task for model_training in self.model_trainings for required_task in model_training.requires()]

    def obtain_models_train_and_val_probas(self):
        self.train_probas = []
        self.val_probas = []

        for i, model_training in enumerate(self.model_trainings):
            print("Running model %d of %d" % (i + 1, len(self.model_trainings)))

            with self.strategy.scope():
                trained_model = model_training.get_trained_model()
            self.train_probas.append(pred_probas_for_classifier(
                trained_model, model_training.train_generator,
                model_training.train_steps_per_epoch)[:len(self.train_dataset)])
            self.val_probas.append(pred_probas_for_classifier(
                trained_model, model_training.val_generator,
                model_training.val_steps_per_epoch)[:len(self.val_dataset)])

            K.clear_session()

        return self.train_probas, self.val_probas

    def before_train(self):
        self.obtain_models_train_and_val_probas()
        super().before_train()

    def after_fit(self, model: Model = None):
        model = self.get_trained_model()
        super().after_fit(model)

        attention_model = Model(model.input, model.get_layer("softmax_attention_last_dense").output)
        train_attention_probas = pred_probas_for_classifier(attention_model, self.train_generator,
                                                            self.train_steps_per_epoch)[:len(self.train_dataset)]
        val_attention_probas = pred_probas_for_classifier(attention_model, self.val_generator,
                                                          self.val_steps_per_epoch)[:len(self.val_dataset)]
        train_attention_heat_map = plot_attention_heat_map(train_attention_probas)
        val_attention_heat_map = plot_attention_heat_map(val_attention_probas)
        train_attention_heat_map.savefig(os.path.join(self.output_path, "train_attention_heat_map.png"))
        val_attention_heat_map.savefig(os.path.join(self.output_path, "val_attention_heat_map.png"))
        plt.close(train_attention_heat_map)
        plt.close(val_attention_heat_map)

    def get_train_generator(self) -> ImageIterator:
        generator = super().get_train_generator()
        return EnsembleImageIterator(self.train_probas, generator.dataset, generator.batch_size, generator.shuffle,
                                     generator.seed)

    def get_val_generator(self) -> ImageIterator:
        generator = super().get_val_generator()
        return EnsembleImageIterator(self.val_probas, generator.dataset, generator.batch_size, generator.shuffle,
                                     generator.seed)

    def create_model_with(self, input: Union[tf.Tensor, List[tf.Tensor]],
                          base_model_output: Union[tf.Tensor, List[tf.Tensor]]) -> Model:
        n_targets = len(self.target_labels)
        n_models = len(self.model_trainings)

        if self.dropout_after_base_model:
            base_model_output = Dropout(self.dropout_after_base_model,
                                        name="dropout_after_base_model")(base_model_output)
        for i, dense_neurons in enumerate(self.dense_layers_after_base_model):
            base_model_output = Dense(dense_neurons,
                                      kernel_initializer=KERAS_WEIGHT_INIT[self.kernel_initializer](self.seed),
                                      name="dense_after_base_model_%d" % i)(base_model_output)

            if self.bn_between_dense_layers_after_base_model:
                base_model_output = BatchNormalization(name="bn_dense_after_base_model_%d" % i)(base_model_output)

            base_model_output = Activation(self.activation_function,
                                           name="activation_dense_after_base_model_%d" % i)(base_model_output)

            if self.dropout_between_dense_layers_after_base_model:
                base_model_output = Dropout(self.dropout_between_attention_dense_layers,
                                            name="dropout_dense_after_base_model_%d" % i)(base_model_output)

        ensemble_inputs = [Input(shape=(n_targets,), name="ensemble_input_%d" % i) for i in range(n_models)]
        if self.include_ensemble_output_in_attention:
            attention_output = Concatenate(name="attention_input")([base_model_output] + ensemble_inputs)
        else:
            attention_output = base_model_output

        for i, dense_neurons in enumerate(self.attention_dense_layers):
            attention_output = Dense(dense_neurons,
                                     kernel_initializer=KERAS_WEIGHT_INIT[self.kernel_initializer](self.seed),
                                     name="attention_dense_%d" % i)(attention_output)

            if self.bn_between_attention_dense_layers:
                attention_output = BatchNormalization(name="bn_attention_dense_%d" % i)(attention_output)

            attention_output = Activation(self.activation_function,
                                          name="activation_attention_dense_%d" % i)(attention_output)

            if self.dropout_between_attention_dense_layers:
                attention_output = Dropout(self.dropout_between_attention_dense_layers,
                                           name="dropout_attention_dense_%d" % i)(attention_output)

        attention_output = Dense(n_models,
                                 activity_regularizer=self._get_attention_layer_activity_regularizer(),
                                 name="attention_last_dense")(attention_output)
        attention_output = Softmax(name="softmax_attention_last_dense")(attention_output)

        stacked_ensemble_probas_inputs = Reshape((n_models, n_targets))(Concatenate(axis=-1)(ensemble_inputs))
        output = Dot(axes=1)([attention_output, stacked_ensemble_probas_inputs])

        return Model([input] + ensemble_inputs, output)

    def _get_attention_layer_activity_regularizer(self):
        attention_layer_activity_regularizer = None
        if self.attention_layer_activity_regularizer != "none":
            attention_layer_activity_regularizer = KERAS_REGULARIZERS[self.attention_layer_activity_regularizer](
                self.attention_layer_activity_regularizer_value)
        return attention_layer_activity_regularizer


class EnsembleImageIterator(ImageIterator):
    def __init__(self, ensemble_probas, dataset: ImageDataset, batch_size: int, shuffle: bool = True, seed: int = None):
        super().__init__(dataset, batch_size, shuffle, seed)
        self._ensemble_probas = ensemble_probas

    def _get_batches_of_transformed_samples(self, index_array):
        batch = super()._get_batches_of_transformed_samples(index_array)
        return [batch[0]] + [probas[index_array] for probas in self._ensemble_probas], batch[1]


class AttentionBasedImageEnsembleClassifierWithWindowOptimizer(WindowOptimizerMixin, AttentionBasedImageEnsembleClassifier):
    pass
