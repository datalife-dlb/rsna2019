import luigi
import cv2
import os
import glob
import pandas as pd
import numpy as np
import json

from typing import Tuple
from PIL import Image
from matplotlib import pyplot as plt
from tqdm import tqdm

from datalife.task import config
from datalife.data import ImageIterator, ImageDataset
from datalife.task.base import BaseModelTraining


# - Para múltiplas GPUs:
## ./cuda_luigid --background --pidfile /tmp/luigid.pid --logdir /tmp/luigid_log
## PYTHONPATH="." ./cuda_luigi --module datalife.task.debug GenerateAugumenterImages --project pneumonia --debug-samples 30 --show-histogram --histogram-type rgb

# - Para uma única GPU:
## PYTHONPATH="." luigi --module datalife.task.debug GenerateAugumenterImages --project pneumonia --debug-samples 30 --show-histogram --histogram-type rgb --local-scheduler

class GenerateAugumenterImages(BaseModelTraining):
    debug_samples: int = luigi.IntParameter(default=10)  # 0 (zero) = All
    show_histogram: bool = luigi.BoolParameter(default=False)
    histogram_type: str = luigi.ChoiceParameter(choices=["basic", "rgb"], default="rgb")
    run_children: bool = luigi.BoolParameter(default=False)

    image_data_format: str = luigi.ChoiceParameter(choices=["channels_first", "channels_last"], default="channels_last")

    def requires(self):
        return config.PROJECTS[self.project].prepare_data_frames_task(), \
               config.PROJECTS[self.project].resize_images_task()

    def create_model(self):
        pass

    def fit(self):
        pass

    def lr_find(self):
        pass

    def run(self):
        __augmenter_dir: str = os.path.join(os.path.join(config.PROJECTS[self.project].base_dir, "debug"), "augmenter")

        print('[INFO] Gerando os arquivos em:', __augmenter_dir)

        # Cria o diretório de destino
        os.makedirs(__augmenter_dir, exist_ok=True)

        # remove os arquivos anteriores
        files = glob.glob('{}/*'.format(__augmenter_dir))
        for f in files:
            os.remove(f)

        # recupera todos os augmenters
        augmenters = [self.augmenter] + self.augmenter.get_all_children(flat=True) \
            if self.run_children else [self.augmenter]

        # Processa cada um
        for i, augmenter in enumerate(augmenters):

            # Recupera o nome do augmenter
            __augmenter_name = "%s[%d]" % (augmenter.name.replace('Unnamed', ''), i)

            print('[INFO] Processando o Augmenter:', __augmenter_name)

            # Gerando o Generator
            train_df = pd.read_csv(self.train_data_frame_path)
            train_dataset = ImageDataset(self.project_config, self.target_columns, train_df, self.images_path,
                                         self.input_shape,
                                         self.interpolation,
                                         image_preprocessing=self.image_preprocessing,
                                         image_preprocessing_extra_params=self.image_preprocessing_extra_params,
                                         image_color_mode=self.image_color_mode,
                                         image_data_format=self.image_data_format,
                                         augmenter=augmenter)

            images = ImageIterator(train_dataset, batch_size=self.batch_size, shuffle=True, seed=self.seed)

            # Recuperando uma amostra de cada augmenter
            for i, image in tqdm(enumerate(images), total=self.debug_samples or len(images)):
                array_img = np.array(image[0][0])
                if self.rescale_input:
                    array_img = array_img * 255

                file_name = os.path.join(__augmenter_dir, "{}_{}.png".format(__augmenter_name, str(i)))

                if self.show_histogram:
                    histogram = Histogram(filename=file_name)
                    if self.histogram_type in 'rgb':
                        histogram.execute_rgb_type(np.uint8(array_img))
                    else:
                        histogram.execute_basic_type(np.uint8(array_img))
                else:
                    cv2.imwrite(file_name, array_img)

                if int(i + 1) >= int(self.debug_samples or len(images)):
                    break


class Histogram(object):

    def __init__(self, filename: str):
        self.__filename: str = filename

    def execute_rgb_type(self, array_img: np.ndarray):
        image: Image = Image.fromarray(array_img)
        indice: int = 221

        shape: Tuple[int, int, int] = np.shape(array_img)

        plt.figure(figsize=(10, 10))
        plt.subplot(indice)
        plt.axis('off')
        plt.title('Original Image {}, {}, {}'.format(shape[0], shape[1], shape[2]))
        plt.grid(False)
        plt.imshow(image)

        color: Tuple[str, str, str] = ('b', 'g', 'r')
        titles: dict = {'b': 'Histogram blue', 'g': 'Histogram green', 'r': 'Histogram red'}
        indice: int = 222

        for i, col in enumerate(color):
            histr = cv2.calcHist([array_img], [i], None, [256], [0, 256])
            histr = cv2.normalize(histr, None)
            plt.subplot(indice + i)
            plt.grid(False)
            plt.xlabel('Value')
            plt.ylabel('frequency')
            plt.title(titles[col])
            plt.plot(histr, color=col)
            plt.xlim([0, 256])

        plt.savefig(self.__filename)

    def execute_basic_type(self, array_img: np.ndarray):
        image: Image = Image.fromarray(array_img)
        shape: Tuple[int, int, int] = np.shape(array_img)

        plt.figure(figsize=(10, 5))
        plt.subplot(121)
        plt.axis('off')
        plt.title('Original Image {}, {}, {}.'.format(shape[0], shape[1], shape[2]))
        plt.grid(False)
        plt.imshow(image)

        plt.subplot(122)
        plt.hist(array_img.ravel(), 256, [0, 256], color='black', density=True, histtype='step');
        plt.grid(False)
        plt.xlabel('Value')
        plt.ylabel('frequency')
        plt.title('Histogram')
        plt.xlim([0, 256])
        plt.savefig(self.__filename)


# - Para múltiplas GPUs:
## ./cuda_luigid --background --pidfile /tmp/luigid.pid --logdir /tmp/luigid_log
## PYTHONPATH="." ./cuda_luigi --module datalife.task.debug GetMeanDataset --project pneumonia --image-data-format "channels_first"

# - Para uma única GPU:
## PYTHONPATH="." luigi --module datalife.task.debug GetMeanDataset --project pneumonia --image-data-format "channels_first" --local-scheduler
class GetMeanDataset(BaseModelTraining):
    image_data_format: str = luigi.ChoiceParameter(choices=["channels_first", "channels_last"],
                                                   default="channels_first")
    dataset_absolute_path: str = luigi.Parameter(default=None)

    def requires(self):
        return config.PROJECTS[self.project].prepare_data_frames_task(), \
               config.PROJECTS[self.project].resize_images_task()

    def create_model(self):
        pass

    def fit(self):
        pass

    def lr_find(self):
        pass

    def run(self):
        __mean_dir: str = os.path.join(os.path.join(config.PROJECTS[self.project].base_dir, "debug"), "mean")

        os.makedirs(__mean_dir, exist_ok=True)

        if self.dataset_absolute_path is None:
            dataset_path = self.images_path
        else:
            dataset_path = self.dataset_absolute_path

        file_name = os.path.join(__mean_dir, "{}.txt".format(dataset_path.split(os.sep)[-1]))

        R = []
        G = []
        B = []
        for i, img in tqdm(enumerate(os.listdir(dataset_path)), total=len(os.listdir(dataset_path))):
            image = cv2.imread(os.path.join(dataset_path, img))

            (b, g, r) = cv2.mean(image)[:3]
            R.append(r)
            G.append(g)
            B.append(b)

        dict = {"red_mean": np.mean(R), "green_mean": np.mean(G), "blue_mean": np.mean(B)}

        txt = json.dumps(dict)
        f = open(file_name, "w")
        f.write(txt)
        f.close()
