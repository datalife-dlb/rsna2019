import abc
import importlib
import os
from collections import OrderedDict
from typing import Tuple, Dict

import luigi
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow.keras.backend as K
from torch.utils.data import Dataset
from tqdm import tqdm

from datalife.data import ImageIterator, ImageDataset, calculate_steps_per_epoch
from datalife.files import symlink_misclassified_inputs
from datalife.gradcamutils import grad_cam_plus
from datalife.keras.eval import evaluate_classifier, pred_probas_for_classifier, evaluate_roc_curve
from datalife.plot import plot_confusion_matrix, plot_roc_curve, plot_cam
from datalife.task.base import load_keras_model_training_from_task_id, BaseKerasModelTraining
from datalife.task.config import PROJECTS
from datalife.task.data_preparation.base import BasePrepareDataFrames, BaseResizeImages
from datalife.task.meta_config import TaskType
from datalife.task.model.attention_based_ensemble import AttentionBasedImageEnsembleClassifier, EnsembleImageIterator

LAST_CONV_LAYER_PER_BACKBONE: Dict[str, str] = dict(
    vgg16="block5_activation3",
    vgg19="block5_activation4",
    resnet50="activation_49",
    inception_v3="mixed10",
    inception_resnet_v2="conv_7b",
    xception="block14_sepconv2",
    mobilenet="conv_pw_13",
    mobilenet_v2="Conv_1",
    densenet121="conv5_block16_2_conv",
    densenet169="conv5_block32_2_conv",
    densenet201="conv5_block32_2_conv",
    nasnet_mobile="normal_concat_18",
    nasnet_large="normal_concat_18",
)


class BaseEvaluationTask(luigi.Task, metaclass=abc.ABCMeta):
    project: str = luigi.Parameter()
    model_module: str = luigi.Parameter(default="datalife.task.model.image_classifier")
    model_cls: str = luigi.Parameter(default="ImageClassifier")
    model_task_id: str = luigi.Parameter()

    data_frames_preparation_extra_params: dict = luigi.DictParameter(default={})

    use_model_training_dataset_and_generator: bool = luigi.BoolParameter(default=False)

    def requires(self) -> Tuple[BasePrepareDataFrames, BaseResizeImages]:
        return self.model_training.requires()

    def output(self):
        return luigi.LocalTarget(
            os.path.join(PROJECTS[self.project].base_dir, "evaluation", self.__class__.__name__, "results",
                         self.model_task_id))

    @property
    def model_training(self) -> BaseKerasModelTraining:
        if not hasattr(self, "_model_training"):
            module = importlib.import_module(self.model_module)
            class_ = getattr(module, self.model_cls)

            self._model_training = load_keras_model_training_from_task_id(self.project, class_, self.model_task_id)
            if self.data_frames_preparation_extra_params:
                self._model_training.data_frames_preparation_extra_params = self.data_frames_preparation_extra_params

        return self._model_training

    @property
    def data_frame_path(self) -> str:
        return self.input()[0][2].path

    @property
    def images_path(self) -> str:
        return self.input()[1].path

    def _get_dataset_and_generator(self, df: pd.DataFrame, images_path: str,
                                   model_training: BaseKerasModelTraining) -> Tuple[
        Dataset, tf.data.Dataset]:
        dataset = self.model_training.project_config.dataset_class(model_training.project_config,
                                                                   model_training.tasks, df,
                                                                   images_path,
                                                                   model_training.input_shape,
                                                                   model_training.interpolation,
                                                                   image_preprocessing=model_training.image_preprocessing,
                                                                   image_preprocessing_extra_params=model_training.image_preprocessing_extra_params,
                                                                   image_color_mode=model_training.image_color_mode,
                                                                   image_data_format=model_training.image_data_format,
                                                                   dtype=model_training.dtype)
        return dataset, dataset.to_tf_dataset(batch_size=model_training.val_batch_size, shuffle=False)

    def transform_data_frame(self, df: pd.DataFrame) -> pd.DataFrame:
        return df

    def run(self):
        os.makedirs(self.output().path, exist_ok=True)

        df = pd.read_csv(self.data_frame_path)
        df = self.transform_data_frame(df)

        if type(self.model_training) is AttentionBasedImageEnsembleClassifier:
            probas = []
            for dependent_model_training in self.model_training.model_trainings:  # type: BaseKerasModelTraining
                dataset, dependent_generator = self._get_dataset_and_generator(df, self.images_path,
                                                                               # TODO: Suportar dimensões diferentes
                                                                               dependent_model_training)
                probas.append(pred_probas_for_classifier(
                    dependent_model_training.get_trained_model(),
                    dependent_generator,
                    calculate_steps_per_epoch(dataset, dependent_model_training.val_batch_size)
                )[:len(dataset)])

            dataset, _ = self._get_dataset_and_generator(df, self.images_path, self.model_training)
            generator = EnsembleImageIterator(probas, dataset, self.model_training.batch_size, shuffle=False)
        elif self.use_model_training_dataset_and_generator:
            dataset, generator = self.model_training.test_dataset, self.model_training.test_generator
        else:
            dataset, generator = self._get_dataset_and_generator(df, self.images_path, self.model_training)

        self.evaluate(df, dataset, generator)

    @abc.abstractmethod
    def evaluate(self, df: pd.DataFrame, dataset: ImageDataset, generator: tf.data.Dataset):
        pass


class KerasTestEvaluationForImageClassification(BaseEvaluationTask):
    def evaluate(self, df: pd.DataFrame, dataset: ImageDataset, generator: tf.data.Dataset):
        print("Running best model on dataset...")
        try:
            self.model_training.set_keras_dtype()
            with self.model_training.strategy.scope():
                trained_model = self.model_training.get_trained_model()
            model_probas = pred_probas_for_classifier(
                trained_model, generator,
                calculate_steps_per_epoch(dataset, self.model_training.val_batch_size)
            )[:len(dataset)]
        finally:
            K.clear_session()

        print("Evaluating...")
        for i, task in enumerate(self.model_training.tasks):
            probas = model_probas if len(self.model_training.tasks) == 1 else model_probas[i]
            df["score_%d" % i] = probas.tolist()
            targets = dataset.targets[task.output_layer_name]
            output_path = os.path.join(self.output().path, task.output_layer_name)

            if task.type == TaskType.SOFTMAX_CLASSIFICATION:
                thresholds = self.model_training.thresholds_to_eval \
                    if task.has_negative_target else [None]

                results = []
                for threshold in tqdm(thresholds, total=len(thresholds)):
                    report = evaluate_classifier(probas, targets, dataset.im_list, threshold)

                    misclassified_images_for_threshold_path = os.path.join(output_path,
                                                                           "misclassified_images_%.2f" % threshold
                                                                           if threshold else "misclassified_images")
                    os.makedirs(misclassified_images_for_threshold_path, exist_ok=True)
                    symlink_misclassified_inputs(report.misclassified_inputs, misclassified_images_for_threshold_path)

                    results.append(
                        OrderedDict(threshold=threshold, acc=report.acc, precision=report.precision,
                                    recall=report.recall, f1_score=report.f1_score))

                    confusion_matrix_fig = plot_confusion_matrix(report.confusion_matrix,
                                                                 task.labels)
                    confusion_matrix_fig \
                        .savefig(os.path.join(self.output().path,
                                              "confusion_matrix_%.2f.png" % threshold
                                              if threshold else "confusion_matrix.png"))
                    plt.close(confusion_matrix_fig)

                results_df = pd.DataFrame(results)
                results_df.to_csv(os.path.join(output_path, "report.csv"), index=False)

            if task.type == TaskType.SOFTMAX_CLASSIFICATION or task.type == TaskType.MULTILABEL_CLASSIFICATION:
                print("Plotting ROC curves...")
                for i, label in enumerate(tqdm(task.labels, total=len(task.labels))):
                    fpr, tpr, _, roc_auc = evaluate_roc_curve(i, probas, targets)
                    roc_curve_fig = plot_roc_curve(fpr, tpr, roc_auc)
                    roc_curve_fig.savefig(os.path.join(output_path, "roc_curve_[%s].png" % label))
                    plt.close(roc_curve_fig)

        df.to_csv(os.path.join(self.output().path, "inference_output.csv"), index=False)


class GenerateImagesWithVisualExplanation(BaseEvaluationTask):
    samples: int = luigi.IntParameter(default=10)  # 0 (zero) = All
    last_conv_layer_name: str = luigi.Parameter(default="")

    def evaluate(self, df: pd.DataFrame, dataset: ImageDataset, generator: ImageIterator):
        try:
            samples = self.samples or len(dataset)
            if samples > len(dataset):
                samples = len(dataset)
            last_conv_layer_name = self.last_conv_layer_name or LAST_CONV_LAYER_PER_BACKBONE[
                self.model_training.backbone]

            self.model_training.set_keras_dtype()
            with self.model_training.strategy.scope():
                model = self.model_training.get_trained_model()

            indices = list(range(len(dataset)))
            np.random.shuffle(indices)

            for i, _ in tqdm(zip(indices, range(samples)), total=samples):
                img, target = dataset[i]

                cam, prediction = grad_cam_plus(model, np.expand_dims(img, axis=0),
                                                last_conv_layer_name,
                                                self.model_training.input_shape[0],
                                                self.model_training.input_shape[1])
                fig = plot_cam(dataset.load_raw_image(i), cam)
                fig.savefig(os.path.join(self.output().path,
                                         "%d_[expected:%s]_[result:%s].png" % (i, str(target), str(prediction))))
                plt.close(fig)
        finally:
            K.clear_session()
