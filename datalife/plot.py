from typing import List, Union

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
import numpy as np
import pandas as pd
import seaborn as sns
sns.set()

def plot_history(history_df: pd.DataFrame) -> Figure:
    metrics = [column for column in history_df.columns if column != "epoch" and "val_" not in column]

    fig = plt.figure(figsize=(8*len(metrics), 5))

    for i, metric in enumerate(metrics):
        ax = fig.add_subplot(1, len(metrics), i+1)
        if metric == "loss":
            ax.set_yscale('log')
        ax.plot(history_df[metric], label="train")
        ax.plot(history_df[f"val_{metric}"], label="validation")
        ax.set_title(metric)
        ax.set_xlabel('epoch')
        ax.set_ylabel(metric)
        ax.legend()

    fig.tight_layout()

    return fig


def plot_loss_per_lr(learning_rates: List[float], loss_values: List[float]) -> Figure:
    fig = plt.figure(figsize=(15, 10))

    ax = fig.add_subplot(1, 1, 1)
    ax.set_xscale('log')
    ax.plot(learning_rates, loss_values)
    ax.set_xlabel('learning rate (log scale)')
    ax.set_ylabel('loss')

    fig.tight_layout()

    return fig


def plot_loss_derivatives_per_lr(learning_rates: List[float], loss_derivatives: List[float]) -> Figure:
    fig = plt.figure(figsize=(15, 10))

    ax = fig.add_subplot(1, 1, 1)
    ax.set_xscale('log')
    ax.plot(learning_rates, loss_derivatives)
    ax.set_xlabel('learning rate (log scale)')
    ax.set_ylabel('d/loss')

    fig.tight_layout()

    return fig


def plot_confusion_matrix(confusion_matrix: Union[np.ndarray, List[List[float]]], class_names, figsize=(10, 7),
                          fontsize=12) -> Figure:
    df_cm = pd.DataFrame(
        confusion_matrix, index=class_names, columns=class_names,
    )
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)
    heatmap = sns.heatmap(df_cm, annot=True, ax=ax, fmt="d", cmap="Blues")
    heatmap.yaxis.set_ticklabels(heatmap.yaxis.get_ticklabels(), rotation=0, ha='right', fontsize=fontsize)
    heatmap.xaxis.set_ticklabels(heatmap.xaxis.get_ticklabels(), rotation=0, ha='center', fontsize=fontsize)
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    return fig


def plot_roc_curve(fpr: np.ndarray, tpr: np.ndarray, roc_auc: float, figsize=(10, 7)) -> Figure:
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)

    ax.set_title('Receiver Operating Characteristic')
    ax.plot(fpr, tpr, 'b', label='AUC = %0.6f' % roc_auc)
    ax.legend(loc='lower right')
    ax.plot([0, 1], [0, 1], 'r--')
    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    ax.set_ylabel('True Positive Rate')
    ax.set_xlabel('False Positive Rate')

    return fig


def plot_cam(img: np.ndarray, cam: np.ndarray) -> Figure:
    fig = plt.figure()

    ax1 = fig.add_subplot(121)
    ax1.imshow(img)
    ax1.set_axis_off()

    ax2 = fig.add_subplot(122)
    ax2.imshow(img)
    ax2.imshow(cam, alpha=0.3, cmap="jet")
    ax2.set_axis_off()

    return fig


def plot_masks(img: np.ndarray, masks: List[np.ndarray]) -> Figure:
    fig = plt.figure()

    ax1 = fig.add_subplot(1, len(masks) + 1, 1)
    ax1.imshow(img)
    ax1.set_axis_off()

    for i, mask in enumerate(masks):
        ax = fig.add_subplot(1, len(masks) + 1, i + 2)
        ax.imshow(img)
        ax.imshow(mask, alpha=0.3, cmap="jet")
        ax.set_axis_off()

    return fig


def plot_attention_heat_map(attention: np.ndarray, figsize=(7, 12)) -> Figure:
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111)
    sns.heatmap(attention, ax=ax, cmap="Blues")
    plt.ylabel('Samples')
    plt.xlabel('Models')
    return fig