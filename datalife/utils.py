from datetime import datetime
from typing import List

import numpy as np
import pandas as pd
import pydicom
from PIL.Image import Image
from sklearn.preprocessing import LabelBinarizer


def no_params_wrapper(loss_function):
    def wrapper(*args):
        return loss_function

    return wrapper


def img_to_array(img: Image, data_format: str) -> np.ndarray:
    """Converts a PIL Image instance to a Numpy array.

    # Arguments
        img: PIL Image instance.
        data_format: Image data format,
            either "channels_first" or "channels_last".

    # Returns
        A 3D Numpy array.

    # Raises
        ValueError: if invalid `img` or `data_format` is passed.
    """

    if data_format not in {'channels_first', 'channels_last'}:
        raise ValueError('Unknown data_format: ', data_format)
    # Numpy array x has format (height, width, channel)
    # or (channel, height, width)
    # but original PIL image has format (width, height, channel)
    x = np.asarray(img)
    if len(x.shape) == 3:
        if data_format == 'channels_first':
            x = x.transpose(2, 0, 1)
    elif len(x.shape) == 2:
        if data_format == 'channels_first':
            x = x.reshape((1, x.shape[0], x.shape[1]))
        else:
            x = x.reshape((x.shape[0], x.shape[1], 1))
    else:
        raise ValueError('Unsupported image shape: ', x.shape)
    return x


class SoftmaxLabelBinarizer(LabelBinarizer):
    def __init__(self, classes: list):
        super().__init__()
        self.classes_ = np.array(classes)
        self.y_type_ = "binary" if len(classes) == 2 else "multiclass"

    def transform(self, y):
        Y = super().transform(y)
        if self.y_type_ == "binary":
            return np.hstack((1 - Y, Y))
        else:
            return Y

    def inverse_transform(self, Y, threshold=None):
        if self.y_type_ == "binary":
            return super().inverse_transform(Y[:, 0], threshold)
        else:
            return super().inverse_transform(Y, threshold)


def row_with_one_hot_to_list(row: pd.Series, columns: List[str]) -> List[str]:
    classes = []
    for column in columns:
        if row[column]:
            classes.append(column)
    return classes


def get_first_of_dicom_field_as_int(x) -> int:
    if type(x) == pydicom.multival.MultiValue:
        return int(x[0])
    else:
        return int(x)


def get_dicom_field_as_int_list(x) -> List[int]:
    if type(x) == pydicom.multival.MultiValue:
        return [int(element) for element in x]
    else:
        return [int(x)]


def convert_date_and_time_from_dicom_to_datetime(date: str, time: str) -> datetime:
    return datetime.strptime(date + " " + time,
                             "%Y%m%d %H%M%S" if len(time) == 6 else "%Y%m%d %H%M%S.%f")
