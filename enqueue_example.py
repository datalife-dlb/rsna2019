import luigi
import numpy as np


# - Para múltiplas GPUs:
## ./cuda_luigid --background --pidfile /tmp/luigid.pid --logdir /tmp/luigid_log
## PYTHONPATH="." ./cuda_luigi --module enqueue_example EnqueueExample --seed 42 --workers 4
# - Para uma única GPU:
## PYTHONPATH="." luigi --module enqueue_example EnqueueExample --seed 42 --local-scheduler
from datalife.task.model.image_classifier import ImageClassifier


class EnqueueExample(luigi.WrapperTask):
    seed: int = luigi.IntParameter(default=42)

    def requires(self):
        random_state = np.random.RandomState(self.seed)

        neurons_options = [512, 1024, 2048]
        number_of_layers_options = [1, 2, 3, 4]
        batch_size_options = [20, 50, 100]

        for i in range(16):
            number_of_layers = int(random_state.choice(number_of_layers_options))
            dense_layers = [int(random_state.choice(neurons_options)) for _ in range(number_of_layers)]
            yield ImageClassifier(project="pneumonia", backbone="vgg16", dense_layers=dense_layers,
                                  learning_rate=random_state.uniform(1e-4, 1e-2),
                                  batch_size=int(random_state.choice(batch_size_options)))